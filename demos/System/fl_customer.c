/***************************************************************************
 *
 *            SIMCOM Application Foreground Layer
 *
 *              Copyright (c) 2010 SIMCOM Ltd.
 *
 ***************************************************************************
 *
 *   $Revision: #1 $
 *   $DateTime: 2010/07/22 09:00:00 $
 *
 ***************************************************************************
 *
 *  Designed by    :   CY
 *  Coded by       :   CY
 *  Tested by      :
 *
 ***************************************************************************
 *
 * File Description
 * ----------------
 *   system functions
 *   sample1:  execute AT+CRWP=1,1024(cannot exceed 8192bytes) means allocate memory(length:1024)
 *                   from memory pool, if success, free it. 
 *   sample2:  execute AT+CRWP=2 to reset the target.
 *   sample3:  execute AT+CRWP=3 to power down. 
 *   sample4:  execute AT+CRWP=4 to clean the watch dog.
 *
 ***************************************************************************
 *
 ***************************************************************************/
#include "fl_stdlib.h"
#include "fl_typ.h"
#include "fl_appinit.h"
#include "fl_interface.h"
#include "fl_error.h"
#include "fl_fcm.h"
#include "fl_timer.h"
#include "fl_trace.h"
FlEventBuffer	__align__ flEventBuffer;
FlEventBuffer	__align__ flEventBufferM1;
FlEventBuffer	__align__ flEventBufferM2;
FlEventBuffer	__align__ flEventBufferM3;
FlEventBuffer	__align__ flEventBufferM4;
FlEventBuffer	__align__ flEventBufferM5;
extern void waitForSysInt(void);
extern void fl_MultiTaskPrio1(void);
extern void fl_MultiTaskPrio2(void);
extern void fl_MultiTaskPrio3(void);
extern void fl_MultiTaskPrio4(void);
extern void fl_MultiTaskPrio5(void);
/** Init function */
fl_task_entry fl_init = waitForSysInt;

/** Entry functions */
fl_task_entry fl_entries[] = { fl_MultiTaskPrio1, fl_MultiTaskPrio2, fl_MultiTaskPrio3, fl_MultiTaskPrio4, fl_MultiTaskPrio5, NULL };
/***************************************************************************
*  Function:       fl_entry                                                 
*  Object:         Customer application                        
*  Variable Name     |IN |OUT|GLB|  Utilisation                           
***************************************************************************/
void fl_entry()
{
    bool  keepGoing = TRUE;
    u32 para1,para2,i;
    u16 *ptr;
    u8 result;
		
    /*Sends the output data from core to embedded application*/	
    ebdat9_03SetModemdataToFL(TRUE);   
      
    while (keepGoing == TRUE)
    {
        memset((u8*)&flEventBuffer,0x00,sizeof(flEventBuffer));
        eat1_02GetEvent(&flEventBuffer);

        switch(flEventBuffer.eventTyp)
        {
            case EVENT_MODEMDATA:
            {
                ebdat7_01DebugTrace("length:%d",flEventBuffer.eventData.modemdata_evt.length);
                ebdat7_01DebugTrace("value:%s",flEventBuffer.eventData.modemdata_evt.data);
                if(flEventBuffer.eventData.modemdata_evt.type == MODEM_CRWP)
                {
                    flEventBuffer.eventData.modemdata_evt.data[flEventBuffer.eventData.modemdata_evt.length] = 0;
										
                    /*get the parameters*/
                    i = sscanf((const char *)flEventBuffer.eventData.modemdata_evt.data, "AT+CRWP=%d,%d",&para1, &para2);
                    ebdat7_01DebugTrace("\n\rGet %d para: %d, %d, %d\r\n", i, para1, para2);

					   /*sample 1*/
                    if( 1 == para1 )
                    {
                        /*para2 is the length of memory to be get */
                        ptr=ebdat4_01GetMemory(para2);
                        if( NULL != ptr )
                        {
                            ebdat7_01DebugTrace("***get memory OK***");

                            /*free it*/
                            result=ebdat4_02FreeMemory(ptr);

                            if( TRUE == result )
                            {
                                ebdat7_01DebugTrace("***free memory OK***");
                            }
                            else
                            {
                                ebdat7_01DebugTrace("***free memory ERROR***");
                            }
                         }
                         else
                        {
                            ebdat7_01DebugTrace("***get memory ERROR***");
                        }
                    }
										
                    /*sample 2, that will takes about 17 seconds*/
                    else if(para1 == 2)
                    {
                        ebdat7_01DebugTrace("***RESET***");
                        ebdat4_03Reset();
                    }
											
                    /*sample 3, test pown down function*/
                    else if(para1 == 3)
                    {
                        ebdat7_01DebugTrace("***POWER DOWN***");
                        ebdat4_05PowerDown();
                    }
										
                    /*sample 4,  test wdtkick function. the target can not reset.*/
                    else if(para1 == 4)
                    {
                        /*clean the watch dog*/
                        ebdat4_04Wdtkick();
                    }
                }       
            }
            break;
            case EVENT_UARTDATA:

            break;
            case EVENT_INTR:

            break;
            case EVENT_SERAILSTATUS:

            break;
            case EVENT_TIMER:

            break;
            default:
            break;
        }
    }
}

void fl_MultiTaskPrio1(void)
{
	while (TRUE)
	{
		eat1_02GetEvent(&flEventBufferM1);
		ebdat7_01DebugTrace((const char *)"fl_MultiTaskPrio1");
	}
}

void fl_MultiTaskPrio2(void)
{
	while (TRUE)
	{
		eat1_02GetEvent(&flEventBufferM2);
		ebdat7_01DebugTrace((const char *)"fl_MultiTaskPrio2");
	}
}

void fl_MultiTaskPrio3(void)
{
	while (TRUE)
	{
		eat1_02GetEvent(&flEventBufferM3);
		ebdat7_01DebugTrace((const char *)"fl_MultiTaskPrio3");
	}
}

void fl_MultiTaskPrio4(void)
{
	while (TRUE)
	{
		eat1_02GetEvent(&flEventBufferM4);
		ebdat7_01DebugTrace((const char *)"fl_MultiTaskPrio4");
	}
}

void fl_MultiTaskPrio5(void)
{
	while (TRUE)
	{
		ebdat7_01DebugTrace((const char *)"fl_MultiTaskPrio5");
		eat1_02GetEvent(&flEventBufferM5);
	}
}

void waitForSysInt(void)
{
	bool v_uartInit = FALSE;
	bool v_fileSystem = FALSE;
	while((v_uartInit == FALSE) ||
		(v_fileSystem == FALSE))
	{
		eat1_02GetEvent(&flEventBuffer);
		switch(flEventBuffer.eventTyp)
		{
			case EVENT_FLASH_READY:
				v_fileSystem = TRUE;
				break;
			case EVENT_UART_READY:
				v_uartInit = TRUE;
				break;
			default:
				break;
		}
	}

}
