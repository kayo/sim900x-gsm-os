/***************************************************************************
 *
 *            SIMCOM Application Foreground Layer
 *
 *              Copyright (c) 2010 SIMCOM Ltd.
 *
 ***************************************************************************
 *
 *   $Revision: #1 $
 *   $DateTime: 2010/08/30 09:00:00 $
 *
 ***************************************************************************
 *
 *  Designed by    :   HJ
 *  Coded by       :   HJ
 *  Tested by      :
 *
 ***************************************************************************
 *
 * File Description
 * ----------------
 *   Update embedapp.cla via FTP
 *
 *
 ***************************************************************************
 *
 ***************************************************************************/
 
#include "fl_stdlib.h" 
#include "fl_typ.h"
#include "fl_appinit.h"
#include "fl_interface.h"
#include "fl_error.h"
#include "fl_timer.h"
#include "fl_fcm.h"
#include "fl_flash.h"
#include "fl_trace.h"

#define STR_CME_EER		      "+CME ERROR:"
#define STR_ERR			          "ERROR"
#define STR_OK			             "OK"

#define FL_END 100

#define CHECKTIME 3   
#define MAX_AT_LEN 100
#define MAX_DATALEN 200

typedef enum ATCmdTypTag
{
     AT_NULL=0,
	  AT_TEST,           
     AT_READ,          
     AT_WRITE,       
     AT_EXECUT,
     AT_ELSE           
}ATCmdTyp;

typedef enum ResTag
{
    RES_OK,              // AT command response is OK 
    RES_ERROR,
    RES_ELSE
}t_res;

typedef struct CurcmdTag
{
    ascii cmd_string[MAX_AT_LEN];
    u8   cmd_type;
} t_curcmd;

typedef s8(*fl_cmdhandle)(u8 *data,t_curcmd *curcmd);

typedef struct attab
{
    u8* cmd;
    fl_cmdhandle	hd;
}t_attab; 

void  clearcmd(t_curcmd *cmd);
static s8 updatecmd(u8 *cmddata, t_curcmd *cmd, ATCmdTyp cmdtype);
FlEventBuffer	__align__ flEventBuffer;
FlEventBuffer	__align__ flEventBufferM1;
FlEventBuffer	__align__ flEventBufferM2;
FlEventBuffer	__align__ flEventBufferM3;
FlEventBuffer	__align__ flEventBufferM4;
FlEventBuffer	__align__ flEventBufferM5;
extern void waitForSysInt(void);
extern void fl_MultiTaskPrio1(void);
extern void fl_MultiTaskPrio2(void);
extern void fl_MultiTaskPrio3(void);
extern void fl_MultiTaskPrio4(void);
extern void fl_MultiTaskPrio5(void);
/** Init function */
fl_task_entry fl_init = waitForSysInt;

/** Entry functions */
fl_task_entry fl_entries[] = { fl_MultiTaskPrio1, fl_MultiTaskPrio2, fl_MultiTaskPrio3, fl_MultiTaskPrio4, fl_MultiTaskPrio5, NULL };

/*******************************************************************************
 * Function:  CHECK_OK
 
 * Description: check the return value of AT command. 
 
 *******************************************************************************/
static s8 CHECK_OK(u8 *data, t_curcmd *curcmd)
{
	s8 *p1=NULL;
	s8 *p2=NULL;	
	p1 = (s8 *)strstr((const char *)data,STR_OK);
	p2 = (s8 *)strstr((const char *)data,STR_ERR);
	//return OK
	if(NULL!=p1)
   {	
		return RES_OK;  
	}
	//return ERROR
	if(NULL!=p2)
   {
		return RES_ERROR; 
	}		
	return RES_ELSE;   
}

/*******************************************************************************
 * Function:  CHECK_SAPBR_1
 
 * Description: check the return value of AT+SAPBR=3,1,"Contype","GPRS" command  from modem
 
 *******************************************************************************/
		
static s8 CHECK_SAPBR_1(u8 *data, t_curcmd *curcmd)
{
    s8 updateflag=FL_ERROR;
    s8 checkok=RES_ERROR;
    checkok=CHECK_OK(data,curcmd);	 
    switch (checkok)
    {
        case RES_OK:
        {
            updateflag=updatecmd((u8*)"AT+SAPBR_2\r", curcmd,AT_WRITE);
            if(FL_ERROR == updateflag)
            {
                ebdat7_01DebugTrace("FLOW1 ERROR!");
                return FL_ERROR;
            }			            
            ebdat9_01SendToModem((u8*)"AT+SAPBR=3,1,\"APN\",\"CMNET\"\r", sizeof("AT+SAPBR=3,1,\"APN\",\"CMNET\"\r"));
            return RES_OK;
        }								
        case RES_ERROR:
        {
            return FL_ERROR;
        }
        /*do nothing*/
        case RES_ELSE:		
        return RES_ELSE;		
    } 
    return FL_ERROR;	 
}

/*******************************************************************************
 * Function:  CHECK_SAPBR_2
 
 * Description: check the return value of AT+SAPBR=3,1,"APN","CMNET" command  from modem
 
 *******************************************************************************/
static s8 CHECK_SAPBR_2(u8 *data, t_curcmd *curcmd)
{
    s8 updateflag=FL_ERROR;
    s8 checkok=RES_ERROR;
    checkok=CHECK_OK(data,curcmd);	 
			 
    switch (checkok)
    {
        case RES_OK:
        {
            updateflag=updatecmd((u8*)"AT+SAPBR_3\r", curcmd,AT_WRITE);
            if(FL_ERROR == updateflag)
            {
                ebdat7_01DebugTrace("FLOW3 ERROR!");
                return FL_ERROR;
            }			            
            ebdat9_01SendToModem((u8*)"AT+SAPBR=1,1\r", sizeof("AT+SAPBR=1,1\r"));
            return RES_OK;
        }							
        case RES_ERROR:
        {
            ebdat7_01DebugTrace("FLOW4 ERROR!");
            return FL_ERROR;
        }
        /*do nothing*/
        case RES_ELSE:		
        return RES_ELSE;		
    } 
    return FL_ERROR;	 
}
/*******************************************************************************
 * Function:  CHECK_SAPBR_3
 
 * Description: check the return value of AT+SAPBR=1,1 command  from modem
 
 *******************************************************************************/
static s8 CHECK_SAPBR_3(u8 *data, t_curcmd *curcmd)
{
    s8 updateflag=FL_ERROR;
    s8 checkok=RES_ERROR;
    checkok=CHECK_OK(data,curcmd);	 
			 
    switch (checkok)
    {
        case RES_OK:
        {
            updateflag=updatecmd((u8*)"AT+FTPSERV\r", curcmd,AT_EXECUT);
            if(FL_ERROR == updateflag)
            {
                ebdat7_01DebugTrace("FLOW5 ERROR!");
                return FL_ERROR;
            }			            
            ebdat9_01SendToModem((u8*)"AT+FTPSERV=\"116.221.229.52\"\r", sizeof("AT+FTPSERV=\"116.221.229.52\"\r"));
            return RES_OK;
        }							
        case RES_ERROR:
        {
            return FL_ERROR;
        }
        /*do nothing*/
        case RES_ELSE:		
        return RES_ELSE;		
    } 
    return FL_ERROR;	 
}
/*******************************************************************************
 * Function:  CHECK_FTPServer
 
 * Description: check the return value of AT+FTPSERV command  from modem
 
 *******************************************************************************/
static s8 CHECK_FTPServer(u8 *data,t_curcmd *curcmd)
{
    s8 updateflag=FL_ERROR;
    s8 checkok=RES_ERROR;
    checkok=CHECK_OK(data,curcmd);	 
			 
    switch (checkok)
    {
        case RES_OK:
        {
            updateflag=updatecmd((u8*)"AT+FTPUN\r", curcmd,AT_WRITE);
            if(FL_ERROR == updateflag)
            {
                ebdat7_01DebugTrace("FLOW7 ERROR!");
                return FL_ERROR;
            }			            
                ebdat9_01SendToModem((u8*)"AT+FTPUN=\"mkr\"\r", sizeof("AT+FTPUN=\"mkr\"\r"));
                return RES_OK;
        }						
        case RES_ERROR:
        {
            return FL_ERROR;
        }
        /*do nothing*/
        case RES_ELSE:		
        return RES_ELSE;
    } 
    return FL_ERROR;	 
}

/*******************************************************************************
 * Function:  CHECK_FTPUN
 
 * Description: check the return value of AT+FTPUN command  from modem
 
 *******************************************************************************/
static s8 CHECK_FTPUN(u8 *data,t_curcmd *curcmd)
{
    s8 updateflag=FL_ERROR;
    s8 checkok=RES_ERROR;
    checkok=CHECK_OK(data,curcmd);	 			 
    switch (checkok)
    {
        case RES_OK:
        {
            updateflag=updatecmd((u8*)"AT+FTPPW\r", curcmd,AT_WRITE);
            if(FL_ERROR == updateflag)
            {
                ebdat7_01DebugTrace("FLOW9 ERROR!");
                return FL_ERROR;
            }			            
            ebdat9_01SendToModem((u8*)"AT+FTPPW=\"135\"\r", sizeof("AT+FTPPW=\"135\"\r"));
            return RES_OK;
        }							
        case RES_ERROR:
        {
            return FL_ERROR;
        }
        /*do nothing*/
        case RES_ELSE:			
        return RES_ELSE;			
    } 
    return FL_ERROR;
}

/*******************************************************************************
 * Function:  CHECK_FTPPW
 
 * Description: check the return value of AT+FTPPW command  from modem
 
 *******************************************************************************/
static s8 CHECK_FTPPW(u8 *data,t_curcmd *curcmd)
{
    s8 updateflag=FL_ERROR;
    s8 checkok=RES_ERROR;
    checkok=CHECK_OK(data,curcmd);	 			 
    switch (checkok)
    {
        case RES_OK:
        {
            updateflag=updatecmd((u8*)"AT+FTPGETNAME\r", curcmd,AT_WRITE);
            if(FL_ERROR == updateflag)
            {
                ebdat7_01DebugTrace("FLOW9 ERROR!");
                return FL_ERROR;
            }			            
            ebdat9_01SendToModem((u8*)"AT+FTPGETNAME=\"new_embedapp.cla\"\r", sizeof("AT+FTPGETNAME=\"new_embedapp.cla\"\r"));
            return RES_OK;
        }							
        case RES_ERROR:
        {
            return FL_ERROR;
        }
        /*do nothing*/
        case RES_ELSE:			
        return RES_ELSE;			
    } 
    return FL_ERROR;
}


/*******************************************************************************
 * Function:  CHECK_FTPGETNAME
 
 * Description: check the return value of AT+FTPGETNAME command  from modem
 
 *******************************************************************************/
static s8 CHECK_FTPGETNAME(u8 *data,t_curcmd *curcmd)
{
    s8 updateflag=FL_ERROR;
    s8 checkok=RES_ERROR;
    checkok=CHECK_OK(data,curcmd);	 			 
    switch (checkok)
    {
        case RES_OK:
        {
            updateflag=updatecmd((u8*)"AT+FTPGETPATH\r", curcmd,AT_WRITE);
            if(FL_ERROR == updateflag)
            {
                ebdat7_01DebugTrace("FLOW9 ERROR!");
                return FL_ERROR;
            }			            
            ebdat9_01SendToModem((u8*)"AT+FTPGETPATH=\"/\"\r", sizeof("AT+FTPGETPATH=\"/\"\r"));
            return RES_OK;
        }							
        case RES_ERROR:
        {
            return FL_ERROR;
        }
        /*do nothing*/
        case RES_ELSE:			
        return RES_ELSE;			
    } 
    return FL_ERROR;
}
/*******************************************************************************
 * Function:  CHECK_FTPGETPATH
 
 * Description: check the return value of AT+FTPGETPATH command  from modem
 
 *******************************************************************************/
static s8 CHECK_FTPGETPATH(u8 *data,t_curcmd *curcmd)
{
    s8 updateflag=FL_ERROR;
    s8 checkok=RES_ERROR;
    checkok=CHECK_OK(data,curcmd);	 
			 
    switch (checkok)
    {
        case RES_OK:
        {
            updateflag=updatecmd((u8*)"AT+FTPGET_1\r", curcmd,AT_WRITE);
            if(FL_ERROR == updateflag)
            {
                ebdat7_01DebugTrace("FLOW11 ERROR!");
                return FL_ERROR;
            }			            
                ebdat9_01SendToModem((u8*)"AT+FTPGET=1\r", sizeof("AT+FTPGET=1\r"));
                return RES_OK;					
        }							
        case RES_ERROR:
        {
            return FL_ERROR;
        }

        /*do nothing*/
        case RES_ELSE:	
        return  RES_ELSE;					
    } 
    return FL_ERROR;
}


/*******************************************************************************
 * Function:  CHECK_HTTPACTION
 
 * Description: check the return value of AT+HTTPACTION=0 command  from modem
 
 *******************************************************************************/
static s8 CHECK_FTPGET1(u8 *data,t_curcmd *curcmd)
{
    s8 checkok=RES_ERROR;

    checkok=CHECK_OK(data,curcmd);	 
	
    switch (checkok)
    {
        case RES_OK:
        clearcmd(curcmd);	
        return RES_OK;
        
        case RES_ERROR:
        {
            return FL_ERROR;
        }
				
     

    } 
    return FL_ERROR;
}

static s8 CHECK_FTPGET2(u8 *data,t_curcmd *curcmd)
{
    s8 checkok=RES_ERROR;
    checkok=CHECK_OK(data,curcmd);	 
	
    switch (checkok)
    {
        case RES_OK:
        return FL_OK;
        case RES_ERROR:
        {
            return FL_ERROR;
        }
        /*do nothing*/
      
    } 
    return RES_ELSE;
}

//init tab of AT command
t_attab Attab[] = {
    {(u8*)"AT+SAPBR_1",CHECK_SAPBR_1},
    {(u8*)"AT+SAPBR_2",CHECK_SAPBR_2},  
    {(u8*)"AT+SAPBR_3",CHECK_SAPBR_3},
    {(u8*)"AT+FTPSERV",CHECK_FTPServer},
    {(u8*)"AT+FTPUN",CHECK_FTPUN},
    {(u8*)"AT+FTPPW",CHECK_FTPPW},
    {(u8*)"AT+FTPGETNAME",CHECK_FTPGETNAME},
    {(u8*)"AT+FTPGETPATH",CHECK_FTPGETPATH},
    {(u8*)"AT+FTPGET_1",CHECK_FTPGET1},
    {(u8*)"AT+FTPGET_2",CHECK_FTPGET2},
};


/*******************************************************************************
 * Function:  clearcmd
 
 * Description: clear the flcurcmd table.
 
 *******************************************************************************/
void  clearcmd(t_curcmd *cmd)
{
	/*set 0*/
	memset(cmd->cmd_string,0,MAX_AT_LEN);
	cmd->cmd_type=AT_NULL;
}

/*******************************************************************************
 * Function:  updatecmd
 
 * Description: update the flcurcmd table, make sure it contains the latest command to be exauted.
 
 *******************************************************************************/
static s8 updatecmd(u8 *cmddata, t_curcmd *cmd, ATCmdTyp cmdtype)
{
    char *p1 = NULL;
    char *p2 = NULL;		
	
	 /*check the input parameter*/
	if(NULL==cmd||NULL==cmddata)
	{
		return FL_ERROR;
	}		
	//clean the flcurcmd content
    clearcmd(cmd);
	
    p1 = (char*)cmddata;
    p2 = strchr((const char *)cmddata,0x0D);		
    memcpy(cmd->cmd_string,p1,p2-p1);
    cmd->cmd_type=cmdtype;
	return FL_OK;
}


/***************************************************************************
*  Function:       fl_entry                                                                                                 
*  Object:         Customer application                                                                              
*  Variable Name     |IN |OUT|GLB|  Utilisation                                                               
*                                                                                                                                 
*  Sample :    execute AT+CRWP  after the target find the network and then it will          
*                    trigger HTTP function automatically.                                                  
****************************************************************************/
gu8 ebtData[20480];
gu16 ebtLen = 0;
void fl_entry()
{
    bool  keepGoing = TRUE;
    u16 i;
    s8 result=-2; 
    s8 updateflag=FL_ERROR;
    s8 AtcmdNum=0;
    t_emb_Timer timer;   
    t_curcmd *flcurcmd;	 
    u16 curentlen = 0;
    flcurcmd = ebdat4_01GetMemory(sizeof(t_curcmd ));
    timer.timeoutPeriod = ebdat8_04SecondToTicks(CHECKTIME);
    timer.timerId =1;	 
    clearcmd(flcurcmd);	 
    ebdat9_01SendToModem((u8*)"ATE0\r",strlen("ATE0\r"));
	AtcmdNum = (sizeof(Attab)/sizeof(t_attab));
    //ebdat7_00EnterDebugMode();

    while (keepGoing == TRUE)
    {
        memset((u8*)&flEventBuffer,0x00,sizeof(flEventBuffer));
        eat1_02GetEvent(&flEventBuffer);
				
        switch(flEventBuffer.eventTyp)
        {		

            case EVENT_MODEMDATA:        
            {
                 /*execute AT+CRWP  to trigger this function.*/
	             if(flEventBuffer.eventData.modemdata_evt.type == MODEM_CRWP)
                {                     					                     
                    /*set the destination of return values form  core to the embeddedat application*/

                    ebdat9_03SetModemdataToFL(TRUE);			
						updateflag=updatecmd((u8*)"AT+SAPBR_1\r", flcurcmd,AT_WRITE);
						if(FL_ERROR == updateflag)
						{
						    ebdat7_01DebugTrace("UPDATECMD ERROR");
                         break;
						}
						/*send the cmd to modem*/
                     ebdat9_01SendToModem((u8*)"AT+SAPBR=3,1,\"Contype\",\"GPRS\"\r", sizeof("AT+SAPBR=3,1,\"Contype\",\"GPRS\"\r"));	
                }                 

                if(flEventBuffer.eventData.modemdata_evt.type == MODEM_CMD)
                {            
                   if((flcurcmd->cmd_type)!= AT_NULL)
                   {
                       for(i = 0;i<AtcmdNum;i++)
                       {
                           /*compare the current command to attab*/
                           if(0 == strcmp((const char*)flcurcmd->cmd_string,(const char*)Attab[i].cmd) )
                           {   								 	 
                               result = Attab[i].hd((u8*)flEventBuffer.eventData.modemdata_evt.data,flcurcmd);	
       																 
                               if(RES_OK == result)  
                               {                                         
                                   ebdat7_01DebugTrace("OK\r\n");		
                                   break;
                               }
       										
                               if(RES_ELSE == result)
                               {     
                                   ebdat7_01DebugTrace("UNKNOW RES ");	
                                   ebdat9_02SendToSerialPort((char * )flEventBuffer.eventData.modemdata_evt.data,flEventBuffer.eventData.modemdata_evt.length);
                                   break;
                               }
												 
                               /*ERROR occurs in the flow*/
                               if(FL_ERROR == result)  									 	
                               {                 
                                   ebdat7_01DebugTrace("%s flow ERROR !!! ",flcurcmd->cmd_string );	
                                   clearcmd(flcurcmd);	
                                   break;
                               }		
       									 
                              
                           }		                      								
                       }
                   }
                   
                    {
                        u8 *q1=NULL;
                        u8 *q2=NULL;
                        q1 = (u8*)strstr((const char *)flEventBuffer.eventData.modemdata_evt.data,"+FTPGET:1,");
                        if(q1!=NULL)
                        {
                              u16 ret = 0;
                              q1+= strlen("+FTPGET:1,");
                              ret = atoi((const char *)q1);
                              if(ret == 1)//get data
                              {
                                  ebdat8_01StartTimer(timer);
                                  ebtLen = 0;

                                  updateflag=updatecmd((u8*)"AT+FTPGET_2\r", flcurcmd,AT_WRITE);
                                  if(FL_ERROR == updateflag)
                                  {
                                      ebdat7_01DebugTrace("FLOW11 ERROR!");
                                      break;
                                  }			            
                                  ebdat9_01SendToModem((u8*)"AT+FTPGET=2,1024\r", sizeof("AT+FTPGET=2,1024\r"));
                              }
                              else if(ret == 0)
                              {
                                  ebdat7_01DebugTrace("FTP OVER !!! ");	
                                  ebdat8_02StopTimer(timer);
                                  clearcmd(flcurcmd);
                                  ebdat7_01DebugTrace("Update DATA %d !!! ",ebtLen);	
                                  
                                 /*****************************************************/
                                  ebdat3_03FlashWriteData(800,ebtLen,ebtData);
                                  eat1_09UpdateEmbeddedAp(800,1,ebtLen);
                                  ebtLen = 0;
                                  return;//return EmbeddedAp to Update
                                  /*****************************************************/
                                  
 
                              }
                              else
                              {
                                   ebdat7_01DebugTrace("FTP FAIL %d!!! ",ret);	
                                   ebdat8_02StopTimer(timer);
                                   clearcmd(flcurcmd);	
                                   ebtLen = 0;
                              }
                        }

                        q2 = (u8*)strstr((const char *)flEventBuffer.eventData.modemdata_evt.data,"+FTPGET:2,");
                        if(q2!=NULL)
                        {
                            q2+= strlen("+FTPGET:2,");
                            curentlen = atoi((const char *)q2);
                            ebdat7_01DebugTrace("FTP DATA  %d !!! ",curentlen);	
                        }
                    }                                       					
                }
                else if(flEventBuffer.eventData.modemdata_evt.type == MODEM_DATA)
                {
                    //FTPGET DATA 
                    curentlen -= flEventBuffer.eventData.modemdata_evt.length;
                    memcpy(&ebtData[ebtLen],flEventBuffer.eventData.modemdata_evt.data,flEventBuffer.eventData.modemdata_evt.length);
                    ebtLen+=flEventBuffer.eventData.modemdata_evt.length;
                    ebdat7_01DebugTrace("FTPGET DATA !!!!");       
                }
            }
            break;

            case EVENT_UARTDATA:

            break;
	
            case EVENT_INTR:// print the interrupt's pinname and status

            break;

            case EVENT_SERAILSTATUS:

            break;

            case EVENT_TIMER:
            {
                ebdat7_01DebugTrace("time out!");
                if(curentlen == 0)
                {
                    updateflag=updatecmd((u8*)"AT+FTPGET_2\r", flcurcmd,AT_WRITE);
                    if(FL_ERROR == updateflag)
                    {
                        ebdat7_01DebugTrace("FLOW11 ERROR!");
                        break;
                    }			            
                    ebdat9_01SendToModem((u8*)"AT+FTPGET=2,1024\r", sizeof("AT+FTPGET=2,1024\r"));
                }
                ebdat8_01StartTimer( timer);
            }
				
            break;

            default:
            break;
        }
    }
}

void fl_MultiTaskPrio1(void)
{
	while (TRUE)
	{
		eat1_02GetEvent(&flEventBufferM1);
		ebdat7_01DebugTrace((const char *)"fl_MultiTaskPrio1");
	}
}

void fl_MultiTaskPrio2(void)
{
	while (TRUE)
	{
		eat1_02GetEvent(&flEventBufferM2);
		ebdat7_01DebugTrace((const char *)"fl_MultiTaskPrio2");
	}
}

void fl_MultiTaskPrio3(void)
{
	while (TRUE)
	{
		eat1_02GetEvent(&flEventBufferM3);
		ebdat7_01DebugTrace((const char *)"fl_MultiTaskPrio3");
	}
}

void fl_MultiTaskPrio4(void)
{
	while (TRUE)
	{
		eat1_02GetEvent(&flEventBufferM4);
		ebdat7_01DebugTrace((const char *)"fl_MultiTaskPrio4");
	}
}

void fl_MultiTaskPrio5(void)
{
	while (TRUE)
	{
		ebdat7_01DebugTrace((const char *)"fl_MultiTaskPrio5");
		eat1_02GetEvent(&flEventBufferM5);
	}
}

void waitForSysInt(void)
{
	bool v_uartInit = FALSE;
	bool v_fileSystem = FALSE;
	while((v_uartInit == FALSE) ||
		(v_fileSystem == FALSE))
	{
		eat1_02GetEvent(&flEventBuffer);
		switch(flEventBuffer.eventTyp)
		{
			case EVENT_FLASH_READY:
				v_fileSystem = TRUE;
				break;
			case EVENT_UART_READY:
				v_uartInit = TRUE;
				break;
			default:
				break;
		}
	}

}
