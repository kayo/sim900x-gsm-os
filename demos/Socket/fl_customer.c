/***************************************************************************
 *
 *            SIMCOM Application Foreground Layer
 *
 *              Copyright (c) 2010 SIMCOM Ltd.
 *
 ***************************************************************************
 *
 *   $Id: fl_customer.c#1 $
 *   $Revision: #1 $
 *   $DateTime: 2011/12/29 09:00:00 $
 *
 ***************************************************************************
 *
 *  Designed by    :   HJ
 *  Coded by       :   HJ
 *  Tested by      :
 *
 ***************************************************************************
 *
 * File Description
 * ----------------
 *   Socket processing fonctions
 *
 *
 ***************************************************************************
 *
 ***************************************************************************/


#include "fl_typ.h"
#include "fl_appinit.h"
#include "fl_interface.h"
#include "fl_error.h"
#include "fl_fcm.h"
#include "fl_timer.h"
#include "fl_trace.h"
#include "fl_bsd.h"
#include <stdarg.h>
#include <stdio.h>
#include "string.h"

void ipaddr32to8(u32 ipaddr, u8 *str)
{
   u8 buf[4];
  	buf[0]=(u8)((ipaddr&0xff000000)>>24);
   buf[1]=(u8)((ipaddr&0x00ff0000)>>16);
	buf[2]=(u8)((ipaddr&0x0000ff00)>>8);
	buf[3]=(u8)(ipaddr&0x000000ff);    
   sprintf((char *)str, "%d.%d.%d.%d",buf[0],buf[1],buf[2],buf[3]);
}
FlEventBuffer	__align__ flEventBuffer;
FlEventBuffer	__align__ flEventBufferM1;
FlEventBuffer	__align__ flEventBufferM2;
FlEventBuffer	__align__ flEventBufferM3;
FlEventBuffer	__align__ flEventBufferM4;
FlEventBuffer	__align__ flEventBufferM5;
extern void waitForSysInt(void);
extern void fl_MultiTaskPrio1(void);
extern void fl_MultiTaskPrio2(void);
extern void fl_MultiTaskPrio3(void);
extern void fl_MultiTaskPrio4(void);
extern void fl_MultiTaskPrio5(void);
/** Init function */
fl_task_entry fl_init = waitForSysInt;

/** Entry functions */
fl_task_entry fl_entries[] = { fl_MultiTaskPrio1, fl_MultiTaskPrio2, fl_MultiTaskPrio3, fl_MultiTaskPrio4, fl_MultiTaskPrio5, NULL };
/***************************************************************************/
/*  Function:       fl_entry                                                  */
/*  Object:         Customer application                        */
/*  Variable Name     |IN |OUT|GLB|  Utilisation                           */
/***************************************************************************/
void fl_entry()
{
    bool          keepGoing = TRUE;
    s8			returnValue;
    u32       para1 = 0;
    u32      socket1 = 0xffffffff;
    u32      socket2 = 0xffffffff;
    u8 buf[100];

    while (keepGoing == TRUE)
    {
        memset((u8*)&flEventBuffer,0x00,sizeof(flEventBuffer));
        eat1_02GetEvent(&flEventBuffer); 
      //  ebdat7_01DebugTrace("hj--bsd demo eventTyp  %d ",flEventBuffer.eventTyp);
        
        switch(flEventBuffer.eventTyp)
        {
            case EVENT_MODEMDATA:
              if(flEventBuffer.eventData.modemdata_evt.type == MODEM_CRWP)
              {
                  flEventBuffer.eventData.modemdata_evt.data[flEventBuffer.eventData.modemdata_evt.length] = 0;   	
                  /*get the parameters*/
                  sscanf((const char *)flEventBuffer.eventData.modemdata_evt.data, "AT+CRWP=%d",&para1);

                  /*sample 1*/
                  if(0 == para1)
                  {
                        returnValue = ebdat11_15GprsDeactive();
                        ebdat7_01DebugTrace("ebdat11_15GprsDeactive returnValue  %d ",returnValue);
                  }
                  else if(1 == para1 )
                  {
                        returnValue = ebdat11_10GprsActive((u8*)"CMNET",(u8*)NULL,(u8*)NULL);
                        ebdat7_01DebugTrace("ebdat11_10GprsActive returnValue  %d ",returnValue);
                  }
                  else if( 2 == para1 )
                  {
                      socket1 = ebdat11_20SocketConnect(EBDAT_TCP_CONNECT,(u8*)"116.236.221.75",5555);
                      ebdat7_01DebugTrace("ebdat11_20SocketConnect 1 returnValue  %x",socket1);
                      socket2 = ebdat11_20SocketConnect(EBDAT_UDP_CONNECT,(u8*)"116.236.221.75",6666);
                      ebdat7_01DebugTrace("ebdat11_20SocketConnect 2 returnValue  %x",socket2);
                   }    
                  else if( 3 == para1 )
                  {
                        returnValue = ebdat11_30SocketSend(socket1,"hello world 1",13);
                        ebdat7_01DebugTrace("ebdat11_30SocketSend 1 returnValue  %d ",returnValue);
                        returnValue = ebdat11_30SocketSend(socket2,"hello world 2",13);
                        ebdat7_01DebugTrace("ebdat11_30SocketSend 2 returnValue  %d ",returnValue);
                  }
                  else if( 4 == para1 )
                  {
                        returnValue = ebdat11_25SocketClose(socket1,1);
                        ebdat7_01DebugTrace("ebdat11_25SocketClose 1 socket1 %d returnValue  %d ",socket1,returnValue);
                        returnValue = ebdat11_25SocketClose(socket2,1);
                        ebdat7_01DebugTrace("ebdat11_25SocketClose 2 socket2 %d returnValue  %d ",socket2,returnValue);
                  }
                  else if(5 == para1)
                  {
                        returnValue = ebdat11_45SocketTcpServerSet(1,1234);
                        ebdat7_01DebugTrace("ebdat11_45SocketTcpServerSet 1 returnValue  %d ",returnValue);
                  }
                  else if(6 == para1)
                  {
                        returnValue = ebdat11_45SocketTcpServerSet(0,0);
                        ebdat7_01DebugTrace("ebdat11_45SocketTcpServerSet 0 returnValue  %d ",returnValue);
                  }
                  else if(7 == para1)
                  {
                        u8  ipaddr[50];
                        u32 ip = ebdat11_50GetLocalIpAddr();
                        memset(ipaddr,0,50);
                        ipaddr32to8(ip,ipaddr);
                        ebdat7_01DebugTrace("ebdat11_50GetLocalIpAddr returnValue  %s",ipaddr);
                  }
                  else if(8 == para1)
                  {
                        u16 remain = 0;
                        u16 ret = 0;
                        
                        ret = ebdat11_35SocketRecv(socket1,buf,100,&remain);
                        buf[ret] = 0;
                        ebdat7_01DebugTrace("ebdat11_35SocketRecv socket1 %d,%d,%d,%s",socket1,ret,remain,buf);

                        ret = ebdat11_35SocketRecv(socket2,buf,100,&remain);
                        buf[ret] = 0;
                        ebdat7_01DebugTrace("ebdat11_35SocketRecv socket2 %d,%d,%d,%s",socket1,ret,remain,buf);
                  }
                  
              }
              break;

              case EVENT_SOCKET:
              {
                    switch(flEventBuffer.eventData.socket_evt.type)
                    {
                        case FL_SOCKET_GPRS_ACTIVE:
                          ebdat7_01DebugTrace("gprs active %d, id %x",flEventBuffer.eventData.socket_evt.bsdResult,flEventBuffer.eventData.socket_evt.socketId);
                        break;

                        case FL_SOCKET_GPRS_DEACTIVE:
                          ebdat7_01DebugTrace("gprs deactive %d, id %x",flEventBuffer.eventData.socket_evt.bsdResult,flEventBuffer.eventData.socket_evt.socketId);
                        break;

                        case FL_SOCKET_CONNECT:
                          ebdat7_01DebugTrace("bsd connect %d, id %x",flEventBuffer.eventData.socket_evt.bsdResult,flEventBuffer.eventData.socket_evt.socketId);
                          if(flEventBuffer.eventData.socket_evt.bsdResult == 1)
                          {
                              returnValue = ebdat11_30SocketSend(flEventBuffer.eventData.socket_evt.socketId,"hello world",11);
                              ebdat7_01DebugTrace("ebdat11_30SocketSend 0 1 returnValue  %d ",returnValue);
                              returnValue = ebdat11_30SocketSend(flEventBuffer.eventData.socket_evt.socketId,"hello world",11);
                              ebdat7_01DebugTrace("ebdat11_30SocketSend 0 2 returnValue  %d ",returnValue);
                          }
                          else
                          {
                              if(flEventBuffer.eventData.socket_evt.socketId == socket1)
                              {
                                  socket1 = 0xFFFFFFFF;
                              }
                              else if(flEventBuffer.eventData.socket_evt.socketId == socket2)
                              {
                                  socket2 = 0xFFFFFFFF;
                              }
                          }
                        break;

                        case FL_SOCKET_SEND:
                          ebdat7_01DebugTrace("bsd send len %d, id %x",flEventBuffer.eventData.socket_evt.bsdResult,flEventBuffer.eventData.socket_evt.socketId);
                        break;

                        case FL_SOCKET_RECV:
                          ebdat7_01DebugTrace("bsd recv len %d, id %x",flEventBuffer.eventData.socket_evt.bsdResult,flEventBuffer.eventData.socket_evt.socketId);
                        break;


                        case FL_SOCKET_REMOTE_CLOSE:
                          ebdat7_01DebugTrace("bsd remote close %d,id %x",flEventBuffer.eventData.socket_evt.bsdResult,flEventBuffer.eventData.socket_evt.socketId);
                           if(flEventBuffer.eventData.socket_evt.socketId == socket1)
                          {
                              socket1 = 0xFFFFFFFF;
                          }
                          else if(flEventBuffer.eventData.socket_evt.socketId == socket2)
                          {
                              socket2 = 0xFFFFFFFF;
                          }
                        break;

                        case FL_SOCKET_CLOSE:
                          ebdat7_01DebugTrace("bsd close %d, id %x",flEventBuffer.eventData.socket_evt.bsdResult,flEventBuffer.eventData.socket_evt.socketId);
                          if(flEventBuffer.eventData.socket_evt.socketId == socket1)
                          {
                              socket1 = 0xFFFFFFFF;
                          }
                          else if(flEventBuffer.eventData.socket_evt.socketId == socket2)
                          {
                              socket2 = 0xFFFFFFFF;
                          }
                         
                          if((socket1 == 0xFFFFFFFF) && (socket2 == 0xFFFFFFFF))
                          {
                              ebdat7_01DebugTrace("bsd close all %x,%x",socket1,socket2);
                              ebdat11_15GprsDeactive();
                          }
                        break;

                        case FL_SOCKET_TCP_SERVER_START:
                          ebdat7_01DebugTrace("bsd server start %d, id %x",flEventBuffer.eventData.socket_evt.bsdResult,flEventBuffer.eventData.socket_evt.socketId);
                        break;

                        case FL_SOCKET_TCP_SERVER_STOP:
                          ebdat7_01DebugTrace("bsd server stop %d, id %x",flEventBuffer.eventData.socket_evt.bsdResult,flEventBuffer.eventData.socket_evt.socketId);
                        break;

                        case FL_SOCKET_TCP_SERVER_CONNECT:
                          ebdat7_01DebugTrace("bsd server connect %d, id %x",flEventBuffer.eventData.socket_evt.bsdResult,flEventBuffer.eventData.socket_evt.socketId);
                          if(flEventBuffer.eventData.socket_evt.bsdResult == 1)
                          {
                              if(socket1 == 0xFFFFFFFF)
                              {
                                  socket1 = flEventBuffer.eventData.socket_evt.socketId;
                              }
                              else if(socket2 == 0xFFFFFFFF)
                              {
                                  socket2 = flEventBuffer.eventData.socket_evt.socketId;
                              }
                          }
                        break;
                        default:
                        break;
                        
                    }
                }
                break;
          

          default:
            break;
        }

    }

}


void fl_MultiTaskPrio1(void)
{
	while (TRUE)
	{
		eat1_02GetEvent(&flEventBufferM1);
		ebdat7_01DebugTrace((const char *)"fl_MultiTaskPrio1");
	}
}

void fl_MultiTaskPrio2(void)
{
	while (TRUE)
	{
		eat1_02GetEvent(&flEventBufferM2);
		ebdat7_01DebugTrace((const char *)"fl_MultiTaskPrio2");
	}
}

void fl_MultiTaskPrio3(void)
{
	while (TRUE)
	{
		eat1_02GetEvent(&flEventBufferM3);
		ebdat7_01DebugTrace((const char *)"fl_MultiTaskPrio3");
	}
}

void fl_MultiTaskPrio4(void)
{
	while (TRUE)
	{
		eat1_02GetEvent(&flEventBufferM4);
		ebdat7_01DebugTrace((const char *)"fl_MultiTaskPrio4");
	}
}

void fl_MultiTaskPrio5(void)
{
	while (TRUE)
	{
		ebdat7_01DebugTrace((const char *)"fl_MultiTaskPrio5");
		eat1_02GetEvent(&flEventBufferM5);
	}
}

void waitForSysInt(void)
{
	bool v_uartInit = FALSE;
	bool v_fileSystem = FALSE;
	while((v_uartInit == FALSE) ||
		(v_fileSystem == FALSE))
	{
		eat1_02GetEvent(&flEventBuffer);
		switch(flEventBuffer.eventTyp)
		{
			case EVENT_FLASH_READY:
				v_fileSystem = TRUE;
				break;
			case EVENT_UART_READY:
				v_uartInit = TRUE;
				break;
			default:
				break;
		}
	}

}




