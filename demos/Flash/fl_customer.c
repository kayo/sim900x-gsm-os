/***************************************************************************
 *
 *            SIMCOM Application Foreground Layer
 *
 *              Copyright (c) 2010 SIMCOM Ltd.
 *
 ***************************************************************************
 *
 *   $Id: fl_flash.h#1 $
 *   $Revision: #1 $
 *   $DateTime: 2010/07/22 09:00:00 $
 *
 ***************************************************************************
 *
 *  Designed by    :   MXN
 *  Coded by       :   MXN
 *  Tested by      :
 *
 ***************************************************************************
 *
 * File Description
 * ----------------
 *   Flash processing fonctions
 *
 *
 ***************************************************************************
 *
 ***************************************************************************/

//#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include "fl_typ.h"
#include "fl_appinit.h"
#include "fl_interface.h"
#include "fl_error.h"
#include "fl_timer.h"
#include "fl_fcm.h"
#include "fl_flash.h"
#include "fl_trace.h"
#include "fl_pinforsim900.h"
#include "fl_periphery.h"
#include "fl_stdlib.h"

#define MEMORY_PID		0x00
#define MEMORY_PLEN	0x2000
#define MEMORY_PIDMAX	0x80

gascii ByteBuf[MEMORY_PLEN];/*if you want to define a char or unsigned char variable, you should use gu8 to define a variable.*/
FlEventBuffer	__align__ flEventBuffer;
FlEventBuffer	__align__ flEventBufferM1;
FlEventBuffer	__align__ flEventBufferM2;
FlEventBuffer	__align__ flEventBufferM3;
FlEventBuffer	__align__ flEventBufferM4;
FlEventBuffer	__align__ flEventBufferM5;
u32  SysTick;
s32 p1, p2, p3, p4, p5, p6;/*if you want to define a char or unsigned char variable, you should use gu8 to define a variable.*/
extern void waitForSysInt(void);
extern void fl_MultiTaskPrio1(void);
extern void fl_MultiTaskPrio2(void);
extern void fl_MultiTaskPrio3(void);
extern void fl_MultiTaskPrio4(void);
extern void fl_MultiTaskPrio5(void);
/** Init function */
fl_task_entry fl_init = waitForSysInt;

/** Entry functions */
fl_task_entry fl_entries[] = { fl_MultiTaskPrio1, fl_MultiTaskPrio2, fl_MultiTaskPrio3, fl_MultiTaskPrio4, fl_MultiTaskPrio5, NULL };
/***************************************************************************/
/*  Function:       fl_entry                                                  */
/*  Object:         Customer application                        */
/*  Variable Name     |IN |OUT|GLB|  Utilisation                           */
/***************************************************************************/
void fl_entry()
{
	u16 i;
	u16 dataLength = 0;	
	u32 freeSize = 0;
	ebdat7_01DebugTrace((const char *)"\r\n<<<Entry Customer Task Work>>>\r\n");	

	ebdat9_03SetModemdataToFL(TRUE);

	ebdat9_01SendToModem((u8*)"AT+ICF=3,3;+IPR=115200\r",strlen("AT+ICF=3,3;+IPR=115200\r"));

	p1=9;
	p2=3;
	p3=4;
	p4=5;
	fl_memset(ByteBuf, 0x00, 20);
	i = sprintf(ByteBuf, "AT+CRWP=%d,%d,%d",p1, p2, p3);
	ebdat7_01DebugTrace((const char *)"Sprintf %d : %d, %d, %d\r\n", p1, p2, p3);
	
//	SysTick = ebdat8_08GetSystemTickCounter( );
//	SysTick += 20;
//	while(ebdat8_08GetSystemTickCounter( )<=SysTick);
	
	i = sscanf(ByteBuf, "AT+CRWP=%d,%d,%d",&p1, &p2, &p3);
	ebdat7_01DebugTrace((const char *)"sscanf %d para: %d, %d, %d\r\n", i, p1, p2, p3);
	
	
	ebdat7_01DebugTrace((const char *)"<<<<<<<EAT Task Work Loop begin>>>>>>>");
	while (TRUE)
	{
		ebdat7_01DebugTrace("eat1_02GetEvent in");
		eat1_02GetEvent(&flEventBuffer);
		ebdat7_01DebugTrace((const char *)"eat1_02GetEvent OUT");
		switch(flEventBuffer.eventTyp)
		{
			case EVENT_TIMER:
				ebdat7_01DebugTrace((const char *)"EVENT_TIMER");
				break;
			case EVENT_MODEMDATA:
				ebdat7_01DebugTrace((const char *)"EVENT_MODEMDATA");
				ebdat7_01DebugTrace((const char *)"OUTPUTDATA TYPE: %x", flEventBuffer.eventData.modemdata_evt.type);
				if(flEventBuffer.eventData.modemdata_evt.type == MODEM_CMD)
				{
					if(ebdat9_05GetSerialPortTxStatus())
					{
						ebdat9_02SendToSerialPort((char *)flEventBuffer.eventData.modemdata_evt.data, flEventBuffer.eventData.modemdata_evt.length);
					}
				}
				else if(flEventBuffer.eventData.modemdata_evt.type == MODEM_CRWP)
				{
					flEventBuffer.eventData.modemdata_evt.data[flEventBuffer.eventData.modemdata_evt.length] = 0;
					ebdat7_01DebugTrace((const char *)flEventBuffer.eventData.modemdata_evt.data);
					sscanf((const char *)flEventBuffer.eventData.modemdata_evt.data, "AT+CRWP=%d,%d,%d,%d,%d,%d",&p1, &p2, &p3, &p4, &p5, &p6);
					ebdat7_01DebugTrace((const char *)"Get %d para: %d, %d, %d", i, p1, p2);
					switch(p1)/*FLASH API sample*/
					{
						case 1:
						{
							switch (p2)
							{/*write data to flash*/
								case 0:
								{
									fl_memset(ByteBuf, 0xAA, sizeof(ByteBuf));
									/*The ID should be less than 60000, and the length of data should be less than 65535*/
									if (ebdat3_03FlashWriteData(0, sizeof(ByteBuf), (u8*)ByteBuf) == FL_OK)/*write successfully*/
									{
										if(ebdat9_05GetSerialPortTxStatus())
										{
											ebdat9_02SendToSerialPort((char *)"WRITE SUCCESSFULLY", sizeof("WRITE SUCCESSFULLY"));
										}
									}
									else
									{
										/*write fail*/
										if(ebdat9_05GetSerialPortTxStatus())
										{
											ebdat9_02SendToSerialPort((char *)"\r\nWRITE FAIL\r\n", sizeof("WRITE FAIL"));
										}
									}
								}
								break;
								case 1:/*read data from flash*/
								{
									fl_memset(ByteBuf, 0x00, sizeof(ByteBuf));
									/*The ID should be less than 60000, and the length of data should be less than 65535*/
									if (ebdat3_04FlashReadData(0, sizeof(ByteBuf), (u8*)ByteBuf) == FL_OK)/*read successfully*/
									{
										if(ebdat9_05GetSerialPortTxStatus())
										{
											ebdat9_02SendToSerialPort((char *)ByteBuf, EVENT_MAX_DATA);
											while(!ebdat9_05GetSerialPortTxStatus());
											ebdat9_02SendToSerialPort((char *)"\r\nREAD SUCCESSFULLY\r\n", sizeof("\r\nREAD SUCCESSFULLY\r\n"));
										}
									}
									else/*read fail*/
									{
										if(ebdat9_05GetSerialPortTxStatus())
										{
											ebdat9_02SendToSerialPort((char *)"\r\nREAD FAIL\r\n", sizeof("\r\nREAD FAIL\r\n"));
										}
									}
								}
								break;
								case 2:/*delete data in flash*/
								{
									/*The ID should be less than 60000*/
									if (ebdat3_06FlashDelete(0) == FL_OK)/*delete data in flash successfully*/
									{
										if(ebdat9_05GetSerialPortTxStatus())
										{
											ebdat9_02SendToSerialPort((char *)"\r\nDELETE SUCCESSFULLY\r\n", sizeof("\r\nDELETE SUCCESSFULLY\r\n"));
										}
									}
									else
									{
										if(ebdat9_05GetSerialPortTxStatus())
										{
											ebdat9_02SendToSerialPort((char *)"\r\nDELETE FAIL\r\n", sizeof("\r\nDELETE FAIL\r\n"));
										}
									}
								}
								break;
								case 3:/*get the length of the data in flash*/
								{
									fl_memset(ByteBuf, 0x00, sizeof(ByteBuf));
									/*The ID should be less than 60000*/
									if (ebdat3_05FlashGetLen(0, &dataLength) == FL_OK)/*get the length of the data in flash successfully*/
									{
										fl_sprintf(ByteBuf, "\r\ndataLength:%d\r\n", dataLength);
										while(!ebdat9_05GetSerialPortTxStatus());
										{
											ebdat9_02SendToSerialPort((char *)ByteBuf, fl_strlen(ByteBuf));
										}
										while(!ebdat9_05GetSerialPortTxStatus());
										{
											ebdat9_02SendToSerialPort((char *)"\r\nGET DATA LENGTH SUCCESSFULLY\r\n", sizeof("\r\nGET DATA LENGTH SUCCESSFULLY\r\n"));
										}
									}
									else
									{/*get the length of the data in flash failed*/
										if(ebdat9_05GetSerialPortTxStatus())
										{
											ebdat9_02SendToSerialPort((char *)"\r\nGET DATA LENGTH FAIL\r\n", sizeof("\r\nGET DATA LENGTH FAIL\r\n"));
										}
									}
								}
								break;
								case 4:/*get the free size in flash*/
								{
									fl_memset(ByteBuf, 0x00, sizeof(ByteBuf));
									if (ebdat3_07FlashGetFreeSize(&freeSize) == FL_OK)/*get the free size in flash successfully*/
									{
										fl_sprintf(ByteBuf, "\r\nfreeSize:%d\r\n", freeSize);
										if(ebdat9_05GetSerialPortTxStatus())
										{
											ebdat9_02SendToSerialPort((char *)ByteBuf, fl_strlen(ByteBuf));
										}
										while(!ebdat9_05GetSerialPortTxStatus());
										{
											ebdat9_02SendToSerialPort((char *)"\r\nGET FREE SIZE SUCCESSFULLY\r\n", sizeof("\r\nGET DATA LENGTH SUCCESSFULLY\r\n"));
										}
									}
									else
									{/*get the free size in flash failed*/
										if(ebdat9_05GetSerialPortTxStatus())
										{
											ebdat9_02SendToSerialPort((char *)"\r\nGET FREE SIZE FAIL\r\n", sizeof("\r\nGET DATA LENGTH FAIL\r\n"));
										}
									}
								}
								break;
								case 5:/*write data to test.a file*/
								{
									fl_memset(ByteBuf, 0xCC, sizeof(ByteBuf));
									/*The ID should be less than 60000, and the length of data should be less than 65535*/
									if (ebdat3_09FlashFileWrite(sizeof(ByteBuf), (u8*)ByteBuf, (u8*)"test.a", FL_FILE_FROM_BEGINNING) == FL_OK)/*write successfully*/
									{
										if(ebdat9_05GetSerialPortTxStatus())
										{
											ebdat9_02SendToSerialPort((char *)"WRITE test.a SUCCESSFULLY", sizeof("WRITE test.a SUCCESSFULLY"));
										}
									}
									else
									{
										/*write fail*/
										if(ebdat9_05GetSerialPortTxStatus())
										{
											ebdat9_02SendToSerialPort((char *)"\r\nWRITE test.a FAIL\r\n", sizeof("\r\nWRITE test.a FAIL\r\n"));
										}
									}
								}
								break;
								case 6:/*read data from test.a file*/
								{
									fl_memset(ByteBuf, 0x00, sizeof(ByteBuf));
									/*The ID should be less than 60000, and the length of data should be less than 65535*/
									if (ebdat3_08FlashFileRead(sizeof(ByteBuf), (u8*)ByteBuf, (u8*)"test.a", 0) == FL_OK)/*read successfully*/
									{
										if(ebdat9_05GetSerialPortTxStatus())
										{
											ebdat9_02SendToSerialPort((char *)ByteBuf, EVENT_MAX_DATA);
											while(!ebdat9_05GetSerialPortTxStatus());
											ebdat9_02SendToSerialPort((char *)"\r\nREAD test.a SUCCESSFULLY\r\n", sizeof("\r\nREAD test.a SUCCESSFULLY\r\n"));
										}
									}
									else/*read fail*/
									{
										if(ebdat9_05GetSerialPortTxStatus())
										{
											ebdat9_02SendToSerialPort((char *)"\r\nREAD test.a FAIL\r\n", sizeof("\r\nREAD test.a FAIL\r\n"));
										}
									}
								}
								break;
								case 7:/*delete test.a file*/
								{
									/*The ID should be less than 60000*/
									if (ebdat3_10FlashFileDelete((u8*)"test.a") == FL_OK)/*delete data in flash successfully*/
									{
										if(ebdat9_05GetSerialPortTxStatus())
										{
											ebdat9_02SendToSerialPort((char *)"\r\nDELETE test.a SUCCESSFULLY\r\n", sizeof("\r\nDELETE test.a SUCCESSFULLY\r\n"));
										}
									}
									else
									{
										if(ebdat9_05GetSerialPortTxStatus())
										{
											ebdat9_02SendToSerialPort((char *)"\r\nDELETE test.a FAIL\r\n", sizeof("\r\nDELETE test.a FAIL\r\n"));
										}
									}
								}
								break;
								case 8:/*get the length of test.a file*/
								{
									fl_memset(ByteBuf, 0x00, sizeof(ByteBuf));
									/*The ID should be less than 60000*/
									if (ebdat3_11FlashFileGetLen((u8*)"test.a", &dataLength) == FL_OK)/*get the length of the data in flash successfully*/
									{
										fl_sprintf(ByteBuf, "\r\ndataLength:%d\r\n", dataLength);
										while(!ebdat9_05GetSerialPortTxStatus());
										{
											ebdat9_02SendToSerialPort((char *)ByteBuf, fl_strlen(ByteBuf));
										}
										while(!ebdat9_05GetSerialPortTxStatus());
										{
											ebdat9_02SendToSerialPort((char *)"\r\nGET test.a LENGTH SUCCESSFULLY\r\n", sizeof("\r\nGET test.a LENGTH SUCCESSFULLY\r\n"));
										}
									}
									else
									{/*get the length of the data in flash failed*/
										if(ebdat9_05GetSerialPortTxStatus())
										{
											ebdat9_02SendToSerialPort((char *)"\r\nGET test.a LENGTH FAIL\r\n", sizeof("\r\nGET test.a LENGTH FAIL\r\n"));
										}
									}
								}
								break;
							}
						}
						break;
					}
				}
				break;
			case EVENT_UARTDATA :
				{
				}
				break;
			case EVENT_KEY:/*When you enable the keypad and a key is pressed or released, the EVENT_KEY will be received*/
				{
				}
				break;
			case EVENT_INTR:
				{
				}
				break;
			case EVENT_SERAILSTATUS:
				{
				}
				break;
			default:
				ebdat7_01DebugTrace((const char *)"defualt Event: %d\r\n", flEventBuffer.eventTyp);
				break;
		}
	}
}


void fl_MultiTaskPrio1(void)
{
	while (TRUE)
	{
		eat1_02GetEvent(&flEventBufferM1);
		ebdat7_01DebugTrace((const char *)"fl_MultiTaskPrio1");
	}
}

void fl_MultiTaskPrio2(void)
{
	while (TRUE)
	{
		eat1_02GetEvent(&flEventBufferM2);
		ebdat7_01DebugTrace((const char *)"fl_MultiTaskPrio2");
	}
}

void fl_MultiTaskPrio3(void)
{
	while (TRUE)
	{
		eat1_02GetEvent(&flEventBufferM3);
		ebdat7_01DebugTrace((const char *)"fl_MultiTaskPrio3");
	}
}

void fl_MultiTaskPrio4(void)
{
	while (TRUE)
	{
		eat1_02GetEvent(&flEventBufferM4);
		ebdat7_01DebugTrace((const char *)"fl_MultiTaskPrio4");
	}
}

void fl_MultiTaskPrio5(void)
{
	while (TRUE)
	{
		ebdat7_01DebugTrace((const char *)"fl_MultiTaskPrio5");
		eat1_02GetEvent(&flEventBufferM5);
	}
}

void waitForSysInt(void)
{
	bool v_uartInit = FALSE;
	bool v_fileSystem = FALSE;
	while((v_uartInit == FALSE) ||
		(v_fileSystem == FALSE))
	{
		eat1_02GetEvent(&flEventBuffer);
		switch(flEventBuffer.eventTyp)
		{
			case EVENT_FLASH_READY:
				v_fileSystem = TRUE;
				break;
			case EVENT_UART_READY:
				v_uartInit = TRUE;
				break;
			default:
				break;
		}
	}

}



