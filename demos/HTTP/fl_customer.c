/***************************************************************************
 *
 *            SIMCOM Application Foreground Layer
 *
 *              Copyright (c) 2010 SIMCOM Ltd.
 *
 ***************************************************************************
 *
 *   $Revision: #1 $
 *   $DateTime: 2010/08/30 09:00:00 $
 *
 ***************************************************************************
 *
 *  Designed by    :   CY
 *  Coded by       :   CY
 *  Tested by      :
 *
 ***************************************************************************
 *
 * File Description
 * ----------------
 *   HTTP processing fonctions
 *
 *
 ***************************************************************************
 *
 ***************************************************************************/
 
#include "fl_stdlib.h" 
#include "fl_typ.h"
#include "fl_appinit.h"
#include "fl_interface.h"
#include "fl_error.h"
#include "fl_timer.h"
#include "fl_fcm.h"
#include "fl_flash.h"
#include "fl_trace.h"

#define STR_CME_EER		      "+CME ERROR:"
#define STR_ERR			          "ERROR"
#define STR_OK			             "OK"

#define FL_END 100

#define MAXTIME 60    // max connection time (s)
#define MAX_AT_LEN 100
#define MAX_DATALEN 200

typedef enum ATCmdTypTag
{
     AT_NULL=0,
	  AT_TEST,           
     AT_READ,          
     AT_WRITE,       
     AT_EXECUT,
     AT_ELSE           
}ATCmdTyp;

typedef enum ResTag
{
    RES_OK,              // AT command response is OK 
    RES_ERROR,
    RES_ELSE
}t_res;

typedef struct CurcmdTag
{
    ascii cmd_string[MAX_AT_LEN];
    u8   cmd_type;
} t_curcmd;

typedef s8(*fl_cmdhandle)(u8 *data,t_curcmd *curcmd);

typedef struct attab
{
    u8* cmd;
    fl_cmdhandle	hd;
}t_attab; 
FlEventBuffer	__align__ flEventBuffer;
FlEventBuffer	__align__ flEventBufferM1;
FlEventBuffer	__align__ flEventBufferM2;
FlEventBuffer	__align__ flEventBufferM3;
FlEventBuffer	__align__ flEventBufferM4;
FlEventBuffer	__align__ flEventBufferM5;
extern void waitForSysInt(void);
void  clearcmd(t_curcmd *cmd);
static s8 updatecmd(u8 *cmddata, t_curcmd *cmd, ATCmdTyp cmdtype);
extern void fl_MultiTaskPrio1(void);
extern void fl_MultiTaskPrio2(void);
extern void fl_MultiTaskPrio3(void);
extern void fl_MultiTaskPrio4(void);
extern void fl_MultiTaskPrio5(void);
/** Init function */
fl_task_entry fl_init = waitForSysInt;

/** Entry functions */
fl_task_entry fl_entries[] = { fl_MultiTaskPrio1, fl_MultiTaskPrio2, fl_MultiTaskPrio3, fl_MultiTaskPrio4, fl_MultiTaskPrio5, NULL };

/*******************************************************************************
 * Function:  CHECK_OK
 
 * Description: check the return value of AT command. 
 
 *******************************************************************************/
static s8 CHECK_OK(u8 *data, t_curcmd *curcmd)
{
	s8 *p1=NULL;
	s8 *p2=NULL;	
	p1 = (s8 *)strstr((const char *)data,STR_OK);
	p2 = (s8 *)strstr((const char *)data,STR_ERR);
	//return OK
	if(NULL!=p1)
   {	
		return RES_OK;  
	}
	//return ERROR
	if(NULL!=p2)
   {
		return RES_ERROR; 
	}		
	return RES_ELSE;   
}

/*******************************************************************************
 * Function:  CHECK_SAPBR_1
 
 * Description: check the return value of AT+SAPBR=3,1,"Contype","GPRS" command  from modem
 
 *******************************************************************************/
		
static s8 CHECK_SAPBR_1(u8 *data, t_curcmd *curcmd)
{
    s8 updateflag=FL_ERROR;
    s8 checkok=RES_ERROR;
    checkok=CHECK_OK(data,curcmd);	 
    switch (checkok)
    {
        case RES_OK:
        {
            updateflag=updatecmd((u8*)"AT+SAPBR_2\r", curcmd,AT_WRITE);
            if(FL_ERROR == updateflag)
            {
                ebdat7_01DebugTrace("FLOW1 ERROR!");
                return FL_ERROR;
            }			            
            ebdat9_01SendToModem((u8*)"AT+SAPBR=3,1,\"APN\",\"CMNET\"\r", sizeof("AT+SAPBR=3,1,\"APN\",\"CMNET\"\r"));
            return RES_OK;
        }								
        case RES_ERROR:
        {
            return FL_ERROR;
        }
        /*do nothing*/
        case RES_ELSE:		
        return RES_ELSE;		
    } 
    return FL_ERROR;	 
}

/*******************************************************************************
 * Function:  CHECK_SAPBR_2
 
 * Description: check the return value of AT+SAPBR=3,1,"APN","CMNET" command  from modem
 
 *******************************************************************************/
static s8 CHECK_SAPBR_2(u8 *data, t_curcmd *curcmd)
{
    s8 updateflag=FL_ERROR;
    s8 checkok=RES_ERROR;
    checkok=CHECK_OK(data,curcmd);	 
			 
    switch (checkok)
    {
        case RES_OK:
        {
            updateflag=updatecmd((u8*)"AT+SAPBR_3\r", curcmd,AT_WRITE);
            if(FL_ERROR == updateflag)
            {
                ebdat7_01DebugTrace("FLOW3 ERROR!");
                return FL_ERROR;
            }			            
            ebdat9_01SendToModem((u8*)"AT+SAPBR=1,1\r", sizeof("AT+SAPBR=1,1\r"));
            return RES_OK;
        }							
        case RES_ERROR:
        {
            ebdat7_01DebugTrace("FLOW4 ERROR!");
            return FL_ERROR;
        }
        /*do nothing*/
        case RES_ELSE:		
        return RES_ELSE;		
    } 
    return FL_ERROR;	 
}
/*******************************************************************************
 * Function:  CHECK_SAPBR_3
 
 * Description: check the return value of AT+SAPBR=1,1 command  from modem
 
 *******************************************************************************/
static s8 CHECK_SAPBR_3(u8 *data, t_curcmd *curcmd)
{
    s8 updateflag=FL_ERROR;
    s8 checkok=RES_ERROR;
    checkok=CHECK_OK(data,curcmd);	 
			 
    switch (checkok)
    {
        case RES_OK:
        {
            updateflag=updatecmd((u8*)"AT+HTTPINIT\r", curcmd,AT_EXECUT);
            if(FL_ERROR == updateflag)
            {
                ebdat7_01DebugTrace("FLOW5 ERROR!");
                return FL_ERROR;
            }			            
            ebdat9_01SendToModem((u8*)"AT+HTTPINIT\r", sizeof("AT+HTTPINIT\r"));
            return RES_OK;
        }							
        case RES_ERROR:
        {
            return FL_ERROR;
        }
        /*do nothing*/
        case RES_ELSE:		
        return RES_ELSE;		
    } 
    return FL_ERROR;	 
}
/*******************************************************************************
 * Function:  CHECK_HTTPINIT
 
 * Description: check the return value of AT+HTTPINIT command  from modem
 
 *******************************************************************************/
static s8 CHECK_HTTPINIT(u8 *data,t_curcmd *curcmd)
{
    s8 updateflag=FL_ERROR;
    s8 checkok=RES_ERROR;
    checkok=CHECK_OK(data,curcmd);	 
			 
    switch (checkok)
    {
        case RES_OK:
        {
            updateflag=updatecmd((u8*)"AT+HTTPPARA_1\r", curcmd,AT_WRITE);
            if(FL_ERROR == updateflag)
            {
                ebdat7_01DebugTrace("FLOW7 ERROR!");
                return FL_ERROR;
            }			            
                ebdat9_01SendToModem((u8*)"AT+HTTPPARA=\"CID\",1\r", sizeof("AT+HTTPPARA=\"CID\",1\r"));
                return RES_OK;
        }						
        case RES_ERROR:
        {
            return FL_ERROR;
        }
        /*do nothing*/
        case RES_ELSE:		
        return RES_ELSE;
    } 
    return FL_ERROR;	 
}

/*******************************************************************************
 * Function:  CHECK_HTTPPARA_1
 
 * Description: check the return value of AT+HTTPPARA="CID",1 command  from modem
 
 *******************************************************************************/
static s8 CHECK_HTTPPARA_1(u8 *data,t_curcmd *curcmd)
{
    s8 updateflag=FL_ERROR;
    s8 checkok=RES_ERROR;
    checkok=CHECK_OK(data,curcmd);	 			 
    switch (checkok)
    {
        case RES_OK:
        {
            updateflag=updatecmd((u8*)"AT+HTTPPARA_2\r", curcmd,AT_WRITE);
            if(FL_ERROR == updateflag)
            {
                ebdat7_01DebugTrace("FLOW9 ERROR!");
                return FL_ERROR;
            }			            
            ebdat9_01SendToModem((u8*)"AT+HTTPPARA=\"URL\",\"www.baidu.com\"\r", sizeof("AT+HTTPPARA=\"URL\",\"www.baidu.com\"\r"));
            return RES_OK;
        }							
        case RES_ERROR:
        {
            return FL_ERROR;
        }
        /*do nothing*/
        case RES_ELSE:			
        return RES_ELSE;			
    } 
    return FL_ERROR;
}


/*******************************************************************************
 * Function:  CHECK_HTTPPARA_2
 
 * Description: check the return value of AT+HTTPPARA="URL","www.baidu.com" command  from modem
 
 *******************************************************************************/
static s8 CHECK_HTTPPARA_2(u8 *data,t_curcmd *curcmd)
{
    s8 updateflag=FL_ERROR;
    s8 checkok=RES_ERROR;
    checkok=CHECK_OK(data,curcmd);	 
			 
    switch (checkok)
    {
        case RES_OK:
        {
            updateflag=updatecmd((u8*)"AT+HTTPACTION\r", curcmd,AT_WRITE);
            if(FL_ERROR == updateflag)
            {
                ebdat7_01DebugTrace("FLOW11 ERROR!");
                return FL_ERROR;
            }			            
                ebdat9_01SendToModem((u8*)"AT+HTTPACTION=0\r", sizeof("AT+HTTPACTION=0\r"));
                return RES_OK;					
        }							
        case RES_ERROR:
        {
            return FL_ERROR;
        }

        /*do nothing*/
        case RES_ELSE:	
        return  RES_ELSE;					
    } 
    return FL_ERROR;
}


/*******************************************************************************
 * Function:  CHECK_HTTPACTION
 
 * Description: check the return value of AT+HTTPACTION=0 command  from modem
 
 *******************************************************************************/
static s8 CHECK_HTTPACTION(u8 *data,t_curcmd *curcmd)
{
    s8 checkok=RES_ERROR;
    s8 *p1=NULL;
    p1 = (s8 *)strstr((const char *)data,"+HTTPACTION:0,200,");

    checkok=CHECK_OK(data,curcmd);	 
	
    switch (checkok)
    {
        case RES_OK:
        return RES_OK;
        case RES_ERROR:
        {
            return FL_ERROR;
        }
				
        /*do nothing*/
        case RES_ELSE:		
        {
            /*if  the return value is  HTTPACTION":0,200,***/ 					
            if(NULL!=p1)
            {
                ebdat7_01DebugTrace("----connect ok!!!!------");
                return FL_END;
            }          
        }	
    } 
    return FL_ERROR;
}

//init tab of AT command
t_attab attab[] = {
  {(u8*)"AT+SAPBR_1",CHECK_SAPBR_1},
  {(u8*)"AT+SAPBR_2",CHECK_SAPBR_2},  
  {(u8*)"AT+SAPBR_3",CHECK_SAPBR_3},
  {(u8*)"AT+HTTPINIT",CHECK_HTTPINIT},
  {(u8*)"AT+HTTPPARA_1",CHECK_HTTPPARA_1},
  {(u8*)"AT+HTTPPARA_2",CHECK_HTTPPARA_2},
  {(u8*)"AT+HTTPACTION",CHECK_HTTPACTION},
};


/*******************************************************************************
 * Function:  clearcmd
 
 * Description: clear the flcurcmd table.
 
 *******************************************************************************/
void  clearcmd(t_curcmd *cmd)
{
	/*set 0*/
	memset(cmd->cmd_string,0,MAX_AT_LEN);
	cmd->cmd_type=AT_NULL;
}

/*******************************************************************************
 * Function:  updatecmd
 
 * Description: update the flcurcmd table, make sure it contains the latest command to be exauted.
 
 *******************************************************************************/
static s8 updatecmd(u8 *cmddata, t_curcmd *cmd, ATCmdTyp cmdtype)
{
    char *p1 = NULL;
    char *p2 = NULL;		
	
	 /*check the input parameter*/
	if(NULL==cmd||NULL==cmddata)
	{
		return FL_ERROR;
	}		
	//clean the flcurcmd content
    clearcmd(cmd);
	
    p1 = (char*)cmddata;
    p2 = strchr((const char *)cmddata,0x0D);		
    memcpy(cmd->cmd_string,p1,p2-p1);
    cmd->cmd_type=cmdtype;
	return FL_OK;
}

/***************************************************************************
*  Function:       fl_entry                                                                                                 
*  Object:         Customer application                                                                              
*  Variable Name     |IN |OUT|GLB|  Utilisation                                                               
*                                                                                                                                 
*  Sample :    execute AT+CRWP  after the target find the network and then it will          
*                    trigger HTTP function automatically.                                                  
****************************************************************************/
void fl_entry()
{
    bool  keepGoing = TRUE;
    u16 i;
    s8 result=-2; 
    s8 updateflag=FL_ERROR;
    s8 AtcmdNum=0;
    t_emb_Timer timer;   
    t_curcmd *flcurcmd;	 
    flcurcmd = ebdat4_01GetMemory(sizeof(t_curcmd ));
    timer.timeoutPeriod = ebdat8_04SecondToTicks(MAXTIME);
    timer.timerId =1;	 
    clearcmd(flcurcmd);	 
    ebdat9_01SendToModem((u8*)"ATE0\r",strlen("ATE0\r"));
	AtcmdNum = (sizeof(attab)/sizeof(t_attab));
    //ebdat7_00EnterDebugMode();

    while (keepGoing == TRUE)
    {
        memset((u8*)&flEventBuffer,0x00,sizeof(flEventBuffer));
        eat1_02GetEvent(&flEventBuffer);
				
        switch(flEventBuffer.eventTyp)
        {		

            case EVENT_MODEMDATA:        
            {
                 /*execute AT+CRWP  to trigger this function.*/
	             if(flEventBuffer.eventData.modemdata_evt.type == MODEM_CRWP)
                {                     					                     
                     /*set the destination of return values form  core to the embeddedat application*/
                     ebdat9_03SetModemdataToFL(TRUE);			
						ebdat8_01StartTimer( timer);
						updateflag=updatecmd((u8*)"AT+SAPBR_1\r", flcurcmd,AT_WRITE);
						if(FL_ERROR == updateflag)
						{
						    ebdat7_01DebugTrace("UPDATECMD ERROR");
                         break;
						}
						/*send the cmd to modem*/
                     ebdat9_01SendToModem((u8*)"AT+SAPBR=3,1,\"Contype\",\"GPRS\"\r", sizeof("AT+SAPBR=3,1,\"Contype\",\"GPRS\"\r"));	
                }                 

                if(flEventBuffer.eventData.modemdata_evt.type == MODEM_CMD)
                {
                   if((flcurcmd->cmd_type)!= AT_NULL)
                   {
                       for(i = 0;i<AtcmdNum;i++)
                       {
                           /*compare the current command to attab*/
                           if(0 == strcmp((const char*)flcurcmd->cmd_string,(const char*)attab[i].cmd) )
                           {   								 	 
                               result = attab[i].hd((u8*)flEventBuffer.eventData.modemdata_evt.data,flcurcmd);	
       																 
                               if(RES_OK == result)  
                               {                                         
                                   ebdat7_01DebugTrace("OK\r\n");		
                                   break;
                               }
       										
                               if(RES_ELSE == result)
                               {     
                                   ebdat7_01DebugTrace("UNKNOW RES ");	
                                   ebdat9_02SendToSerialPort((char * )flEventBuffer.eventData.modemdata_evt.data,flEventBuffer.eventData.modemdata_evt.length);
                                   break;
                               }
												 
                               /*ERROR occurs in the flow*/
                               if(FL_ERROR == result)  									 	
                               {                 
                                   ebdat7_01DebugTrace("%s flow ERROR !!! ",flcurcmd->cmd_string );	
                                   clearcmd(flcurcmd);		
                                   ebdat9_03SetModemdataToFL(FALSE);
                                   break;
                               }		
       									 
                               if(FL_END == result)
                               {		
                                   clearcmd(flcurcmd);
                                   ebdat9_03SetModemdataToFL(FALSE);
                                   ebdat9_01SendToModem((u8*)"AT+HTTPREAD\r", sizeof("AT+HTTPREAD\r"));
                                   break;   																		
                               }	
                           }		                      								
                       }
                   }
                   		
                   else  
                   {        				
                       ebdat9_02SendToSerialPort((char * )flEventBuffer.eventData.modemdata_evt.data,flEventBuffer.eventData.modemdata_evt.length);
                       break;
                   }								
                }
            }
            break;

            case EVENT_UARTDATA:

            break;
	
            case EVENT_INTR:// print the interrupt's pinname and status

            break;

            case EVENT_SERAILSTATUS:

            break;

            case EVENT_TIMER:
            {
                ebdat7_01DebugTrace("time out!");
							 
                /*close bearer*/
                ebdat9_01SendToModem((u8*)"AT+SAPBR=0,1\r",sizeof("AT+SAPBR=0,1\r"));
            }
				
            break;

            default:
            break;
        }
    }
}

void fl_MultiTaskPrio1(void)
{
	while (TRUE)
	{
		eat1_02GetEvent(&flEventBufferM1);
		ebdat7_01DebugTrace((const char *)"fl_MultiTaskPrio1");
	}
}

void fl_MultiTaskPrio2(void)
{
	while (TRUE)
	{
		eat1_02GetEvent(&flEventBufferM2);
		ebdat7_01DebugTrace((const char *)"fl_MultiTaskPrio2");
	}
}

void fl_MultiTaskPrio3(void)
{
	while (TRUE)
	{
		eat1_02GetEvent(&flEventBufferM3);
		ebdat7_01DebugTrace((const char *)"fl_MultiTaskPrio3");
	}
}

void fl_MultiTaskPrio4(void)
{
	while (TRUE)
	{
		eat1_02GetEvent(&flEventBufferM4);
		ebdat7_01DebugTrace((const char *)"fl_MultiTaskPrio4");
	}
}

void fl_MultiTaskPrio5(void)
{
	while (TRUE)
	{
		ebdat7_01DebugTrace((const char *)"fl_MultiTaskPrio5");
		eat1_02GetEvent(&flEventBufferM5);
	}
}

void waitForSysInt(void)
{
	bool v_uartInit = FALSE;
	bool v_fileSystem = FALSE;
	while((v_uartInit == FALSE) ||
		(v_fileSystem == FALSE))
	{
		eat1_02GetEvent(&flEventBuffer);
		switch(flEventBuffer.eventTyp)
		{
			case EVENT_FLASH_READY:
				v_fileSystem = TRUE;
				break;
			case EVENT_UART_READY:
				v_uartInit = TRUE;
				break;
			default:
				break;
		}
	}

}
