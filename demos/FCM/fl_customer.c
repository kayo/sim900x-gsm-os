/***************************************************************************
 *
 *            SIMCOM Application Foreground Layer
 *
 *              Copyright (c) 2010 SIMCOM Ltd.
 *
 ***************************************************************************
 *
 *   $Revision: #1 $
 *   $DateTime: 2010/07/22 09:00:00 $
 *
 ***************************************************************************
 *
 *  Designed by    :   CY
 *  Coded by        :   CY
 *  Tested by       :
 *
 ***************************************************************************
 *
 * File Description
 * ----------------
 *   Flash processing fonctions
 *   When you execute an AT command, the command will be sent to embedded application,
 *   and the return values of command will be sent to serial port.
 *
 *   The data flow :
 *   input AT command (from serial port) -> embedded application -> modem
 *   output data (from modem) -> embedded application -> serial port
 ***************************************************************************
 *
 ***************************************************************************/
#include "fl_stdlib.h"
#include "fl_typ.h"
#include "fl_appinit.h"
#include "fl_interface.h"
#include "fl_error.h"
#include "fl_fcm.h"
#include "fl_trace.h"
FlEventBuffer	__align__ flEventBuffer;
FlEventBuffer	__align__ flEventBufferM1;
FlEventBuffer	__align__ flEventBufferM2;
FlEventBuffer	__align__ flEventBufferM3;
FlEventBuffer	__align__ flEventBufferM4;
FlEventBuffer	__align__ flEventBufferM5;
extern void waitForSysInt(void);
extern void fl_MultiTaskPrio1(void);
extern void fl_MultiTaskPrio2(void);
extern void fl_MultiTaskPrio3(void);
extern void fl_MultiTaskPrio4(void);
extern void fl_MultiTaskPrio5(void);
/** Init function */
fl_task_entry fl_init = waitForSysInt;

/** Entry functions */
fl_task_entry fl_entries[] = { fl_MultiTaskPrio1, fl_MultiTaskPrio2, fl_MultiTaskPrio3, fl_MultiTaskPrio4, fl_MultiTaskPrio5, NULL };
/***************************************************************************
*  Function:       fl_entry                                                  
*  Object:         Customer application                        
*  Variable Name     |IN |OUT|GLB|  Utilisation                           
***************************************************************************/
void fl_entry()
{
    bool  keepGoing = TRUE;
    u8  len;
    bool  Txstatus;
    /*The input data from serial port is sent to embedded application*/
    ebdat9_04SetUartdataToFL(TRUE); 

    /*Sends the output data from core to embedded application*/
    ebdat9_03SetModemdataToFL(TRUE);
	
    while (keepGoing == TRUE)
    {
        memset((u8*)&flEventBuffer,0x00,sizeof(flEventBuffer));
        eat1_02GetEvent(&flEventBuffer);
       
        switch(flEventBuffer.eventTyp)
        {
            case EVENT_MODEMDATA:
            {
				ebdat7_01DebugTrace("EVENT_MODEMDATA");
                ebdat7_01DebugTrace("length:%d",flEventBuffer.eventData.modemdata_evt.length);
                ebdat7_01DebugTrace("value:%s",flEventBuffer.eventData.modemdata_evt.data);
								
                /*Gets data length*/
                len=flEventBuffer.eventData.modemdata_evt.length;
								
                /*Gets thetransmit buffer's status of the serial port.*/
                Txstatus=ebdat9_05GetSerialPortTxStatus();		
								
                /*There are data in the transmit buffer*/
                if(Txstatus == FALSE)
                {
                    ebdat7_01DebugTrace("\r\nthere are data in the transmit buffer\r\n");
                }
                else
                {
                    ebdat7_01DebugTrace("\r\nthe transmit buffer is null\r\n");
                    /*send data to serial port*/
                    ebdat9_02SendToSerialPort((char *)flEventBuffer.eventData.modemdata_evt.data,len);
                }
                
            }
            break;

			  /* data from serial port */			
            case EVENT_UARTDATA:
            {
				ebdat7_01DebugTrace("EVENT_UARTDATA");
                ebdat7_01DebugTrace("length:%d",flEventBuffer.eventData.uartdata_evt.length);
                ebdat7_01DebugTrace("value:%s",flEventBuffer.eventData.uartdata_evt.data);
                
                len=flEventBuffer.eventData.uartdata_evt.length;
                /*sends data to core buffer*/
                ebdat9_01SendToModem(flEventBuffer.eventData.uartdata_evt.data,len);
            }
            break;
            case EVENT_INTR:

            break;
            case EVENT_SERAILSTATUS:

            break;
            case EVENT_TIMER:

            break;
            default:
            break;
        }
    }
}

void fl_MultiTaskPrio1(void)
{
	while (TRUE)
	{
		eat1_02GetEvent(&flEventBufferM1);
		ebdat7_01DebugTrace((const char *)"fl_MultiTaskPrio1");
	}
}

void fl_MultiTaskPrio2(void)
{
	while (TRUE)
	{
		eat1_02GetEvent(&flEventBufferM2);
		ebdat7_01DebugTrace((const char *)"fl_MultiTaskPrio2");
	}
}

void fl_MultiTaskPrio3(void)
{
	while (TRUE)
	{
		eat1_02GetEvent(&flEventBufferM3);
		ebdat7_01DebugTrace((const char *)"fl_MultiTaskPrio3");
	}
}

void fl_MultiTaskPrio4(void)
{
	while (TRUE)
	{
		eat1_02GetEvent(&flEventBufferM4);
		ebdat7_01DebugTrace((const char *)"fl_MultiTaskPrio4");
	}
}

void fl_MultiTaskPrio5(void)
{
	while (TRUE)
	{
		ebdat7_01DebugTrace((const char *)"fl_MultiTaskPrio5");
		eat1_02GetEvent(&flEventBufferM5);
	}
}

void waitForSysInt(void)
{
	bool v_uartInit = FALSE;
	bool v_fileSystem = FALSE;
	while((v_uartInit == FALSE) ||
		(v_fileSystem == FALSE))
	{
		eat1_02GetEvent(&flEventBuffer);
		switch(flEventBuffer.eventTyp)
		{
			case EVENT_FLASH_READY:
				v_fileSystem = TRUE;
				break;
			case EVENT_UART_READY:
				v_uartInit = TRUE;
				break;
			default:
				break;
		}
	}

}


