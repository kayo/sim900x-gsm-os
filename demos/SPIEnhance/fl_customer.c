/***************************************************************************
 *
 *            SIMCOM Application Foreground Layer
 *
 *              Copyright (c) 2010 SIMCOM Ltd.
 *
 ***************************************************************************
 *
 *   $Id: fl_flash.h#1 $
 *   $Revision: #1 $
 *   $DateTime: 2010/07/22 09:00:00 $
 *
 ***************************************************************************
 *
 *  Designed by    :   HJY
 *  Coded by       :   HJY
 *  Tested by      :
 *
 ***************************************************************************
 * 
 * File Description
 * ----------------
 *   Flash processing fonctions
 *
 *
 ***************************************************************************
 *
 ***************************************************************************/


#include <fl_typ.h>
#include <fl_pinforsim900.h>/*added by mengxiangning_simcom for embeddedAT -s*/
#include <fl_appinit.h>
#include <fl_interface.h>
#include <fl_error.h>
#include <fl_fcm.h>
#include <fl_timer.h>
#include <fl_periphery.h>

u8 g_counter=0;

FlEventBuffer	__align__ flEventBuffer;
FlEventBuffer	__align__ flEventBufferM1;
FlEventBuffer	__align__ flEventBufferM2;
FlEventBuffer	__align__ flEventBufferM3;
FlEventBuffer	__align__ flEventBufferM4;
FlEventBuffer	__align__ flEventBufferM5;
extern void waitForSysInt(void);
extern void fl_MultiTaskPrio1(void);
extern void fl_MultiTaskPrio2(void);
extern void fl_MultiTaskPrio3(void);
extern void fl_MultiTaskPrio4(void);
extern void fl_MultiTaskPrio5(void);
/** Init function */
fl_task_entry fl_init = waitForSysInt;

/** Entry functions */
fl_task_entry fl_entries[] = { fl_MultiTaskPrio1, fl_MultiTaskPrio2, fl_MultiTaskPrio3, fl_MultiTaskPrio4, fl_MultiTaskPrio5, NULL };
/***************************************************************************/
/*  Function:       fl_entry                                                  */
/*  Object:         Customer application                        */
/*  Variable Name     |IN |OUT|GLB|  Utilisation                           */
/***************************************************************************/
void fl_entry()
{
    bool             keepGoing = TRUE;
    static	     u8 pinFlash = 0;
    t_emb_Timer      timer;
    u8               spiReadByte=0;
    char             uart_log[40];

    unsigned char data ; /*  data */
    unsigned char datas1[16] ="0123456789ABCDEF"; /*  datas */
    unsigned char datas2[16] ="abcdefghijklmnop"; /*  datas */



    
    timer.timerId = 1;
    timer.timeoutPeriod = ebdat8_05MillisecondToTicks(7000);
    
    ebdat8_01StartTimer(timer);

    while (keepGoing == TRUE)
    {
        memset((u8*)&flEventBuffer,0x00,sizeof(flEventBuffer));
    	eat1_02GetEvent(&flEventBuffer);

    	switch(flEventBuffer.eventTyp)
    	{
    	    case EVENT_INTR:/* print the interrupt's pinname and status*/
    		break;
    	    case EVENT_SERAILSTATUS:
    		break;
    	    case EVENT_TIMER:
            {
                
                /*judge the time_id, if it is timer1,restart timer1 1 second*/
                if (1 == flEventBuffer.eventData.timer_evt.timer_id)
                {
                    timer.timeoutPeriod = ebdat8_05MillisecondToTicks(1000);
        	    	ebdat8_02StopTimer(timer);
                    
                    
                    switch(g_counter)
                    {
                        
                        /* (1)CS0(GPIO51) */
                        
                        /* (1.1) SPI config:CS0(GPIO51) */
                        case 0:
                        {  
                            
                            /*  set as 4 wire SPI mode CS0(GPIO51) */
                            ebdat5_21EnhanceSpiConfigure( 
                                                   SSI_SLAVE0,
                                                   SSI_3WIRE, 
                                                   SSI_SYSTEM_DIV_64,
                                                   SSI_FALLING_EDGE,
                                                   SSI_MSBFIRST); 
                        }break;
                         
                        /* (1.2) SPI send one byte with CS0(GPIO51) */
                        case 1:
                        {  
                            ebdat5_22EnhanceSpiWriteByte( SSI_SLAVE0, 0x30);
                            
                        }break;
                          
                        /* (2)CS1(GPIO52) */
                        
                        /* (2.1) SPI config:CS1(GPIO52) */
                        case 2:
                        {   
                            /*  set as 4 wire SPI mode CS1(GPIO52) */
                            ebdat5_21EnhanceSpiConfigure( 
                                                   SSI_SLAVE1,
                                                   SSI_3WIRE, 
                                                   SSI_SYSTEM_DIV_64,
                                                   SSI_FALLING_EDGE,
                                                   SSI_MSBFIRST); 
                            
                        }break;                
                        
                        /* (2.2) SPI send one byte with CS1(GPIO52) */
                        case 3:
                        {  
                            ebdat5_22EnhanceSpiWriteByte( SSI_SLAVE1, 0x31 );
                        }break;
                        
                        /* (3)CS2(GPIO53) */
                        
                        /* (3.1) SPI config:CS2(GPIO53) */
                        case 4:
                        {   
                            /*  set as 4 wire SPI mode CS2(GPIO52) */
                            ebdat5_21EnhanceSpiConfigure( 
                                                   SSI_SLAVE2,
                                                   SSI_3WIRE, 
                                                   SSI_SYSTEM_DIV_64,
                                                   SSI_FALLING_EDGE,
                                                   SSI_MSBFIRST); 
                        }break;
                        
                        /* (3.2) SPI send one byte with CS2(GPIO53) */
                        case 5:
                        {  
                            ebdat5_22EnhanceSpiWriteByte(SSI_SLAVE2, 0x32  );
                        }break;
                        
                        /* (3.3) SPI send bytes with CS2(GPIO53) */
                        case 6:
                        {  
                            ebdat5_24EnhanceSpiWriteBytes(SSI_SLAVE2,  datas1,16);
                            ebdat5_24EnhanceSpiWriteBytes(SSI_SLAVE2,  datas2,16);
                        }break;
                        
                        default:break;
                        
                    }
                    g_counter++;   
                    g_counter%=7;
                    ebdat8_01StartTimer(timer);
                }
                
            }	
                
    	    break;
            
    	    default:
    	        break;
    	}
    }
}




void fl_MultiTaskPrio1(void)
{
	while (TRUE)
	{
		eat1_02GetEvent(&flEventBufferM1);
		ebdat7_01DebugTrace((const char *)"fl_MultiTaskPrio1");
	}
}

void fl_MultiTaskPrio2(void)
{
	while (TRUE)
	{
		eat1_02GetEvent(&flEventBufferM2);
		ebdat7_01DebugTrace((const char *)"fl_MultiTaskPrio2");
	}
}

void fl_MultiTaskPrio3(void)
{
	while (TRUE)
	{
		eat1_02GetEvent(&flEventBufferM3);
		ebdat7_01DebugTrace((const char *)"fl_MultiTaskPrio3");
	}
}

void fl_MultiTaskPrio4(void)
{
	while (TRUE)
	{
		eat1_02GetEvent(&flEventBufferM4);
		ebdat7_01DebugTrace((const char *)"fl_MultiTaskPrio4");
	}
}

void fl_MultiTaskPrio5(void)
{
	while (TRUE)
	{
		ebdat7_01DebugTrace((const char *)"fl_MultiTaskPrio5");
		eat1_02GetEvent(&flEventBufferM5);
	}
}

void waitForSysInt(void)
{
	bool v_uartInit = FALSE;
	bool v_fileSystem = FALSE;
	while((v_uartInit == FALSE) ||
		(v_fileSystem == FALSE))
	{
		eat1_02GetEvent(&flEventBuffer);
		switch(flEventBuffer.eventTyp)
		{
			case EVENT_FLASH_READY:
				v_fileSystem = TRUE;
				break;
			case EVENT_UART_READY:
				v_uartInit = TRUE;
				break;
		}
	}

}

