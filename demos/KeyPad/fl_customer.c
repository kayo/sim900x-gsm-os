/***************************************************************************
 *
 *            SIMCOM Application Foreground Layer
 *
 *              Copyright (c) 2010 SIMCOM Ltd.
 *
 ***************************************************************************
 *
 *   $Id: fl_flash.h#1 $
 *   $Revision: #1 $
 *   $DateTime: 2010/07/22 09:00:00 $
 *
 ***************************************************************************
 *
 *  Designed by    :   MXN
 *  Coded by       :   MXN
 *  Tested by      :
 *
 ***************************************************************************
 *
 * File Description
 * ----------------
 *   Flash processing fonctions
 *
 *
 ***************************************************************************
 *
 ***************************************************************************/

//#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include "fl_typ.h"
#include "fl_appinit.h"
#include "fl_interface.h"
#include "fl_error.h"
#include "fl_timer.h"
#include "fl_fcm.h"
#include "fl_flash.h"
#include "fl_trace.h"
#include "fl_pinforsim900.h"
#include "fl_periphery.h"
#include "fl_stdlib.h"

#define MEMORY_PID		0x00
#define MEMORY_PLEN	0x2000
#define MEMORY_PIDMAX	0x80

gascii ByteBuf[MEMORY_PLEN];/*if you want to define a char or unsigned char variable, you should use gu8 to define a variable.*/
u32  SysTick;
s32 p1, p2, p3, p4, p5, p6;/*if you want to define a char or unsigned char variable, you should use gu8 to define a variable.*/
FlEventBuffer	__align__ flEventBuffer;
FlEventBuffer	__align__ flEventBufferM1;
FlEventBuffer	__align__ flEventBufferM2;
FlEventBuffer	__align__ flEventBufferM3;
FlEventBuffer	__align__ flEventBufferM4;
FlEventBuffer	__align__ flEventBufferM5;
extern void waitForSysInt(void);
extern void fl_MultiTaskPrio1(void);
extern void fl_MultiTaskPrio2(void);
extern void fl_MultiTaskPrio3(void);
extern void fl_MultiTaskPrio4(void);
extern void fl_MultiTaskPrio5(void);
/** Init function */
fl_task_entry fl_init = waitForSysInt;

/** Entry functions */
fl_task_entry fl_entries[] = { fl_MultiTaskPrio1, fl_MultiTaskPrio2, fl_MultiTaskPrio3, fl_MultiTaskPrio4, fl_MultiTaskPrio5, NULL };
/***************************************************************************/
/*  Function:       fl_entry                                                  */
/*  Object:         Customer application                        */
/*  Variable Name     |IN |OUT|GLB|  Utilisation                           */
/***************************************************************************/
void fl_entry()
{
	u16 i;
	ebdat7_01DebugTrace((const char *)"\r\n<<<Entry Customer Task Work>>>\r\n");	

	ebdat9_03SetModemdataToFL(TRUE);
	ebdat9_01SendToModem((u8*)"AT+ICF=3,3;+IPR=115200\r",strlen("AT+ICF=3,3;+IPR=115200\r"));

	p1=9;
	p2=3;
	p3=4;
	p4=5;
	fl_memset(ByteBuf, 0x00, 20);
	i = sprintf((char *)ByteBuf, "AT+CRWP=%d,%d,%d",p1, p2, p3);
	ebdat7_01DebugTrace((const char *)"Sprintf %d : %d, %d, %d\r\n", p1, p2, p3);
	
//	SysTick = ebdat8_08GetSystemTickCounter( );
//	SysTick += 20;
//	while(ebdat8_08GetSystemTickCounter( )<=SysTick);
	
	i = sscanf((const char *)ByteBuf, "AT+CRWP=%d,%d,%d",&p1, &p2, &p3);
	ebdat7_01DebugTrace((const char *)"sscanf %d para: %d, %d, %d\r\n", i, p1, p2, p3);
	
	
	ebdat7_01DebugTrace((const char *)"<<<<<<<EAT Task Work Loop begin>>>>>>>");
	while (TRUE)
	{
		ebdat7_01DebugTrace("eat1_02GetEvent in");
		eat1_02GetEvent(&flEventBuffer);
		ebdat7_01DebugTrace((const char *)"eat1_02GetEvent OUT");
		switch(flEventBuffer.eventTyp)
		{
			case EVENT_TIMER:
				ebdat7_01DebugTrace((const char *)"EVENT_TIMER");
				break;
			case EVENT_MODEMDATA:
				ebdat7_01DebugTrace((const char *)"EVENT_MODEMDATA");
				ebdat7_01DebugTrace((const char *)"OUTPUTDATA TYPE: %x", flEventBuffer.eventData.modemdata_evt.type);
				if(flEventBuffer.eventData.modemdata_evt.type == MODEM_CMD)
				{
					if(ebdat9_05GetSerialPortTxStatus())
					{
						ebdat9_02SendToSerialPort((char *)flEventBuffer.eventData.modemdata_evt.data, flEventBuffer.eventData.modemdata_evt.length);
					}
				}
				else if(flEventBuffer.eventData.modemdata_evt.type == MODEM_CRWP)
				{
					flEventBuffer.eventData.modemdata_evt.data[flEventBuffer.eventData.modemdata_evt.length] = 0;
					ebdat7_01DebugTrace((const char *)flEventBuffer.eventData.modemdata_evt.data);
					i = sscanf((const char *)flEventBuffer.eventData.modemdata_evt.data, "AT+CRWP=%d,%d,%d,%d,%d,%d",&p1, &p2, &p3, &p4, &p5, &p6);
					ebdat7_01DebugTrace((const char *)"Get %d para: %d, %d, %d", i, p1, p2);
					switch(p1)/*FLASH API sample*/
					{
						case 1:/*key pad*/
						{
							switch(p2)
							{
								case 1:/*enable keypad function*/
								{
									if (ebdat6_15KeySubscribe() == FL_OK)/*enable keypad successfully*/
									{
										if(ebdat9_05GetSerialPortTxStatus())
										{
											ebdat9_02SendToSerialPort((char *)"\r\nset pin to keypad successfully\r\n", sizeof("\r\nset pin to keypad successfully\r\n"));
										}
										
									}
									else/*enable keypad faild*/
									{
										if(ebdat9_05GetSerialPortTxStatus())
										{
											ebdat9_02SendToSerialPort((char *)"\r\nset pin to keypad failed\r\n", sizeof("\r\nset pin to keypad failed\r\n"));
										}
									}
								}
								break;
								case 2:
								{
									/*before you want to set the keypad to gpio, you should call the following function to set the keypad to unused*/
									if (ebdat6_08pinConfigureToUnused(FL_PIN_40) == FL_OK)
									{
										if(ebdat9_05GetSerialPortTxStatus())
										{
											ebdat9_02SendToSerialPort((char *)"\r\nset keypad to pin successfully\r\n", sizeof("\r\nset keypad to pin successfully\r\n"));
										}
										if (ebdat6_02GpioSubscribe(FL_PIN_40,FL_GPIO_OUTPUT,1) == FL_OK)
										{
											if(ebdat9_05GetSerialPortTxStatus())
											{
												ebdat9_02SendToSerialPort((char *)"\r\nset pin level successfully\r\n", sizeof("\r\nset pin level successfully\r\n"));
											}
										}
										else
										{
											if(ebdat9_05GetSerialPortTxStatus())
											{
												ebdat9_02SendToSerialPort((char *)"\r\nset pin level failed\r\n", sizeof("\r\nset pin level failed\r\n"));
											}
										}
									}
									else
									{
										if(ebdat9_05GetSerialPortTxStatus())
										{
											ebdat9_02SendToSerialPort((char *)"\r\nset keypad to pin failed\r\n", sizeof("\r\nset keypad to pin failed\r\n"));
										}
									}
								}
								break;
								case 3:/*disable the power key*/
								{
									ebdat6_17DisablePowerOffKey();
								}
								break;
								case 4:/*enable the power key*/
								{
									ebdat6_18EnablePowerOffKey();
								}
								break;
							}
						}
						break;
					}
				}
				break;
			case EVENT_UARTDATA :
				{
				}
				break;
			case EVENT_KEY:/*When you enable the keypad and a key is pressed or released, the EVENT_KEY will be received*/
				{
					/*		KROW0			KROW1		KROW2	KROW3	KROW4
					KCOL0	POWER KEY		0x0001		0x0002	0x0003	POWER KEY
					KCOL1	0x0100			0x0101		0x0102	0x0103	0x0108
					KCOL2	0x0200			0x0201		0x0202	0x0203	0x0208
					KCOL3	0x0500			0x0501		0x0502	0x0503	0x0508
					KCOL4	0x0400			0x0401		0x0402	0x0403	0x0408
					*/
					
					
					
					fl_memset(ByteBuf, 0x00, sizeof(ByteBuf));
					if (flEventBuffer.eventData.key_evt.isPressed == 0)/*a key is released*/
					{
						fl_sprintf(ByteBuf,"\r\nkey %X is released\r\n",flEventBuffer.eventData.key_evt.key_val);
						if(ebdat9_05GetSerialPortTxStatus())
						{
							ebdat9_02SendToSerialPort((char *)ByteBuf, fl_strlen(ByteBuf));
						}
					}
					else
					{
						/*a key is pressed*/
						fl_sprintf(ByteBuf,"\r\nkey %X is pressed\r\n",flEventBuffer.eventData.key_evt.key_val);
						if(ebdat9_05GetSerialPortTxStatus())
						{
							ebdat9_02SendToSerialPort((char *)ByteBuf, fl_strlen(ByteBuf));
						}
					}
				}
				break;
			case EVENT_INTR:
				{
				}
				break;
			case EVENT_SERAILSTATUS:
				{
				}
				break;
			default:
				ebdat7_01DebugTrace((const char *)"defualt Event: %d\r\n", flEventBuffer.eventTyp);
				break;
		}
	}
}



void fl_MultiTaskPrio1(void)
{
	while (TRUE)
	{
		eat1_02GetEvent(&flEventBufferM1);
		ebdat7_01DebugTrace((const char *)"fl_MultiTaskPrio1");
	}
}

void fl_MultiTaskPrio2(void)
{
	while (TRUE)
	{
		eat1_02GetEvent(&flEventBufferM2);
		ebdat7_01DebugTrace((const char *)"fl_MultiTaskPrio2");
	}
}

void fl_MultiTaskPrio3(void)
{
	while (TRUE)
	{
		eat1_02GetEvent(&flEventBufferM3);
		ebdat7_01DebugTrace((const char *)"fl_MultiTaskPrio3");
	}
}

void fl_MultiTaskPrio4(void)
{
	while (TRUE)
	{
		eat1_02GetEvent(&flEventBufferM4);
		ebdat7_01DebugTrace((const char *)"fl_MultiTaskPrio4");
	}
}

void fl_MultiTaskPrio5(void)
{
	while (TRUE)
	{
		ebdat7_01DebugTrace((const char *)"fl_MultiTaskPrio5");
		eat1_02GetEvent(&flEventBufferM5);
	}
}

void waitForSysInt(void)
{
	bool v_uartInit = FALSE;
	bool v_fileSystem = FALSE;
	while((v_uartInit == FALSE) ||
		(v_fileSystem == FALSE))
	{
		eat1_02GetEvent(&flEventBuffer);
		switch(flEventBuffer.eventTyp)
		{
			case EVENT_FLASH_READY:
				v_fileSystem = TRUE;
				break;
			case EVENT_UART_READY:
				v_uartInit = TRUE;
				break;
			default:
				break;
		}
	}

}






