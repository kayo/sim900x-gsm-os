/***************************************************************************
 *
 *            SIMCOM Application Foreground Layer
 *
 *              Copyright (c) 2010 SIMCOM Ltd.
 *
 ***************************************************************************
 *
 *   $Revision: #1 $
 *   $DateTime: 2010/07/22 09:00:00 $
 *
 ***************************************************************************
 *
 *  Designed by    :   CY
 *  Coded by        :   CY
 *  Tested by       :
 *
 ***************************************************************************
 *
 * File Description
 * ----------------
 *   Flash processing fonctions
 *   When you execute an AT command, the command will be sent to embedded application,
 *   and the return values of command will be sent to serial port.
 *
 *   The data flow :
 *   input AT command (from serial port) -> embedded application -> modem
 *   output data (from modem) -> embedded application -> serial port
 ***************************************************************************
 *
 ***************************************************************************/
#include "fl_stdlib.h"
#include "fl_typ.h"
#include "fl_appinit.h"
#include "fl_interface.h"
#include "fl_error.h"
#include "fl_fcm.h"
#include "fl_trace.h"
#include "fl_simcard.h"
/***************************************************************************
*  Function:       fl_entry                                                  
*  Object:         Customer application                        
*  Variable Name     |IN |OUT|GLB|  Utilisation                           
***************************************************************************/
FlEventBuffer  flEventBuffer;
void waitForSysInt(void);

void fl_entry()
{
    bool  keepGoing = TRUE;
    u8  len;
    bool  Txstatus;
    bool sendMuxCommand = 0;
	u8 *p;
	u8 *p1;
	u8 resetType;
	SIMCARDRESET_CNF resetCnf;
	SIMCARDAPDU_DATA apduData;
	extern void ebdat9_22SetMuxMode(u8 muxmode);

    ebdat13_00SetModemAPDUToFL(TRUE);
    ebdat13_01SetSIMCardAPDUToFL(TRUE);
	waitForSysInt();
    /*The input data from serial port is sent to embedded application*/
    ebdat9_04SetUartdataToFL(TRUE); 

    /*Sends the output data from core to embedded application*/
    ebdat9_03SetModemdataToFL(TRUE);
    ebdat7_01DebugTrace("fl_entry");
    ebdat9_01SendToModem("AT+IPR=115200;E0\r\n", strlen("AT+IPR=115200;E0\r\n"));
	ebdat9_21SetFCMTimeout(20);
    while (keepGoing == TRUE)
    {
        memset((u8*)&flEventBuffer,0x00,sizeof(flEventBuffer));
        eat1_02GetEvent(&flEventBuffer);
       
        switch(flEventBuffer.eventTyp)
        {
            case EVENT_MODEMDATA:
            {
				ebdat7_01DebugTrace("EVENT_MODEMDATA");
                ebdat7_01DebugTrace("length:%d",flEventBuffer.eventData.modemdata_evt.length);
                ebdat7_01DebugTrace("value:%s",flEventBuffer.eventData.modemdata_evt.data);
								
                /*Gets data length*/
                len=flEventBuffer.eventData.modemdata_evt.length;
								
                /*Gets thetransmit buffer's status of the serial port.*/
                Txstatus=ebdat9_05GetSerialPortTxStatus();		
								
                /*There are data in the transmit buffer*/
                while(ebdat9_05GetSerialPortTxStatus() == 0);
                {
                    ebdat7_01DebugTrace("\r\nthe transmit buffer is null\r\n");
                    /*send data to serial port*/
//			if (sendMuxCommand == 0)
                    ebdat9_02SendToSerialPort((char *)flEventBuffer.eventData.modemdata_evt.data,len);
                }
                
            }
            break;

			  /* data from serial port */			
            case EVENT_UARTDATA:
            {
				ebdat7_01DebugTrace("EVENT_UARTDATA");
                ebdat7_01DebugTrace("length:%d",flEventBuffer.eventData.uartdata_evt.length);
                ebdat7_01DebugTrace("value:%s",flEventBuffer.eventData.uartdata_evt.data);
                
                len=flEventBuffer.eventData.uartdata_evt.length;
                /*sends data to core buffer*/
                
		   p = strstr(flEventBuffer.eventData.uartdata_evt.data, "AT+CMUX=");
		   p1 = strstr(flEventBuffer.eventData.uartdata_evt.data, "OK");
		   
//		   if (p1 == NULL)
//		   if (sendMuxCommand == 0)
		   {
		   	ebdat9_01SendToModem(flEventBuffer.eventData.uartdata_evt.data,len);
		   }
		   if (p != NULL)
	   	  {
	   	  	ebdat7_01DebugTrace("mux command receive!");
	   	  	//sendMuxCommand = 1;
	   	  }
            }
            break;
		case EVENT_MODEM_APDU:
		{
			if (flEventBuffer.eventData.modemAPDU_evt.apduType == FL_MOD_APDU_RESET)
			{
				resetType = flEventBuffer.eventData.modemAPDU_evt.v_resetType;
				ebdat13_03SendResetReqToSIMCard(resetType);
			}
			else if (flEventBuffer.eventData.modemAPDU_evt.apduType == FL_MOD_APDU_DISCONNECT)
			{
				
			}
			else if (flEventBuffer.eventData.modemAPDU_evt.apduType == FL_MOD_APDU_REQ_DATA)
			{
				ebdat13_08SendAPDUReqToSIMCard(flEventBuffer.eventData.modemAPDU_evt.apduData,0);
			}
			else if (flEventBuffer.eventData.modemAPDU_evt.apduType == FL_MOD_APDU_SEND_DATA)
			{
				ebdat13_08SendAPDUReqToSIMCard(flEventBuffer.eventData.modemAPDU_evt.apduData,1);
			}
		}
		break;
		case EVENT_SIMCARD_APDU:
		{
			if (flEventBuffer.eventData.simcardAPDU_evt.apduType == FL_SIM_ADPU_RESET_CNF)
			{
				resetCnf = flEventBuffer.eventData.simcardAPDU_evt.resetCnf;
				ebdat13_05SendSIMCardResetCnfToModem(resetCnf);
			}
			else if(flEventBuffer.eventData.simcardAPDU_evt.apduType == FL_SIM_APDU_DATA)
			{
				memset((char*)&apduData, 0x00, sizeof(SIMCARDAPDU_DATA));
				apduData.v_len = flEventBuffer.eventData.simcardAPDU_evt.apduData.v_len;
				memcpy(apduData.a_RData, flEventBuffer.eventData.simcardAPDU_evt.apduData.a_RData, apduData.v_len);
				ebdat13_10SendAPDUCnfToModem(apduData);
			}
		}
		break;
            case EVENT_INTR:

            break;
            case EVENT_SERAILSTATUS:

            break;
            case EVENT_TIMER:

            break;
		case EVENT_UART:
				sendMuxCommand = 0;
		break;
            default:
            break;
        }
    }
}

void waitForSysInt(void)
{
	bool v_simCardResetCnf = FALSE;
	bool v_uartInit = FALSE;
	bool v_fileSystem = FALSE;
	u8 resetType;
	SIMCARDRESET_CNF resetCnf;
	SIMCARDAPDU_DATA apduData;
	while((v_simCardResetCnf == FALSE) ||
		(v_uartInit == FALSE) ||
		(v_fileSystem == FALSE))
	{
		eat1_02GetEvent(&flEventBuffer);
		switch(flEventBuffer.eventTyp)
		{
			case EVENT_FLASH_READY:
				v_fileSystem = TRUE;
				break;
			case EVENT_UART_READY:
				v_uartInit = TRUE;
				break;
			case EVENT_MODEM_APDU:
				if (flEventBuffer.eventData.modemAPDU_evt.apduType == FL_MOD_APDU_RESET)
				{
					resetType = flEventBuffer.eventData.modemAPDU_evt.v_resetType;
					ebdat13_03SendResetReqToSIMCard(resetType);
				}
				else if (flEventBuffer.eventData.modemAPDU_evt.apduType == FL_MOD_APDU_DISCONNECT)
				{
					
				}
				else if (flEventBuffer.eventData.modemAPDU_evt.apduType == FL_MOD_APDU_REQ_DATA)
				{
					ebdat13_08SendAPDUReqToSIMCard(flEventBuffer.eventData.modemAPDU_evt.apduData,0);
				}
				else if (flEventBuffer.eventData.modemAPDU_evt.apduType == FL_MOD_APDU_SEND_DATA)
				{
					ebdat13_08SendAPDUReqToSIMCard(flEventBuffer.eventData.modemAPDU_evt.apduData,1);
				}
				break;
			case EVENT_SIMCARD_APDU:
				if (flEventBuffer.eventData.simcardAPDU_evt.apduType == FL_SIM_ADPU_RESET_CNF)
				{
					resetCnf = flEventBuffer.eventData.simcardAPDU_evt.resetCnf;
					ebdat13_05SendSIMCardResetCnfToModem(resetCnf);
					v_simCardResetCnf = TRUE;
				}
				else if(flEventBuffer.eventData.simcardAPDU_evt.apduType == FL_SIM_APDU_DATA)
				{
					memset((char*)&apduData, 0x00, sizeof(SIMCARDAPDU_DATA));
					apduData.v_len = flEventBuffer.eventData.simcardAPDU_evt.apduData.v_len;
					memcpy(apduData.a_RData, flEventBuffer.eventData.simcardAPDU_evt.apduData.a_RData, apduData.v_len);
					ebdat13_10SendAPDUCnfToModem(apduData);
				}
				break;
		}
	}

}
