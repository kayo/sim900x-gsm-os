/***************************************************************************
 *
 *            SIMCOM Application Foreground Layer
 *
 *              Copyright (c) 2010 SIMCOM Ltd.
 *
 ***************************************************************************
 *
 *   $Revision: #1 $
 *   $DateTime: 2010/08/30 09:00:00 $
 *
 ***************************************************************************
 *
 *  Designed by    :   CY
 *  Coded by       :   CY
 *  Tested by      :
 *
 ***************************************************************************
 *
 * File Description
 * ----------------
 *  TCP processing fonctions:
 *  execute"AT+CRWP"command to start up TCP connection, and after excute the following
 *  commands automatically(please change the ip and port according to your environment ), 
 *  the data received from the server will be shown from the serial port. 
 *  
 *  command list:
 *  AT+CIPMUX=0    
 *  AT+CIPSTART="TCP","116.228.221.51","2333"     
 *  AT+CIPSEND
 *  >I WANT TO GET DATA
 *  AT+CIPSHUT
 ***************************************************************************
 *
 ***************************************************************************/
#include "fl_stdlib.h"
#include "fl_typ.h"
#include "fl_appinit.h"
#include "fl_interface.h"
#include "fl_error.h"
#include "fl_timer.h"
#include "fl_fcm.h"
#include "fl_trace.h"

/*the possible retrun values of AT command*/
#define STR_ERR			         "ERROR"
#define STR_OK			             "OK"
#define STR_INPUT              ">"
#define STR_CONOK             "CONNECT OK"
#define STR_SENDOK           "SEND OK"

#define FL_END 100     

#define MAXTIME 600           /* max connection time (s)*/
#define MAX_AT_LEN 100

typedef enum ATCmdTypTag
{
    AT_NULL=0,
    AT_TEST,           
    AT_READ,          
    AT_WRITE,       
    AT_EXECUTE,
    AT_ELSE           
}ATCmdTyp;

typedef enum ResTag
{
    RES_OK,              /* AT command response is OK */
    RES_ELSE           /* AT command response is not OK */
}t_res;

typedef struct CurcmdTag
{
     ascii cmd_string[MAX_AT_LEN];
	  u8   cmd_type;
} t_curcmd;

typedef s8(*fl_cmdhandle)(u8 *data,t_curcmd *curcmd);

typedef struct attab
{
	u8* cmd;
	fl_cmdhandle	hd;
}t_attab; 

void  clearcmd(t_curcmd *cmd);
void  savecmd(u8 * cmddata, t_curcmd * cmd, ATCmdTyp cmdtype);
FlEventBuffer	__align__ flEventBuffer;
FlEventBuffer	__align__ flEventBufferM1;
FlEventBuffer	__align__ flEventBufferM2;
FlEventBuffer	__align__ flEventBufferM3;
FlEventBuffer	__align__ flEventBufferM4;
FlEventBuffer	__align__ flEventBufferM5;
extern void waitForSysInt(void);
extern void fl_MultiTaskPrio1(void);
extern void fl_MultiTaskPrio2(void);
extern void fl_MultiTaskPrio3(void);
extern void fl_MultiTaskPrio4(void);
extern void fl_MultiTaskPrio5(void);
/** Init function */
fl_task_entry fl_init = waitForSysInt;

/** Entry functions */
fl_task_entry fl_entries[] = { fl_MultiTaskPrio1, fl_MultiTaskPrio2, fl_MultiTaskPrio3, fl_MultiTaskPrio4, fl_MultiTaskPrio5, NULL };

/*******************************************************************************
 * Function:  CHECK_CIPMUX
 
 * Description: check the return value of  AT+CIPMUX=0   command  from modem.
 
 *******************************************************************************/
		
static s8 CHECK_CIPMUX(u8 *data, t_curcmd *curcmd)
{
    s8 *p1=NULL;
    s8 *p2=NULL;
	
    p1 = (s8 *)strstr((const char *)data,STR_OK);
    p2 = (s8 *)strstr((const char *)data,STR_ERR);
	
    /* return OK */
    if(NULL!=p1)
    {
        savecmd((u8*)"AT+CIPSTART\r", curcmd,AT_WRITE);
		
        /*establish a TCP connection */
        ebdat9_01SendToModem((u8*)"AT+CIPSTART=\"TCP\",\"116.228.221.51\",\"2333\"\r",\
        sizeof("AT+CIPSTART=\"TCP\",\"116.228.221.51\",\"2333\"\r"));
        return RES_OK;
	}
	
	/* return ERROR */
    if(NULL!=p2)
    {
        return FL_ERROR;
    }		
    return RES_ELSE;
}

/*******************************************************************************
 * Function:  CHECK_CIPSTART
 
 * Description: check the return value of AT+CIPSTART="TCP","116.228.221.51","2333" 
 *                   command  from modem
 
 *******************************************************************************/
static s8 CHECK_CIPSTART(u8 *data,t_curcmd *curcmd)
{

    s8 *p1=NULL;
    s8 *p2=NULL;
		
    ebdat7_01DebugTrace("data %s",data);
	
    /*find the return value*/
    p1 = (s8 *)strstr((const char*)data,STR_OK);
    p2 = (s8*)strstr((const char*)data,STR_CONOK);

    /*return  "OK" */    
	if((NULL!=p1)&&(NULL==p2))
   {
       ebdat7_01DebugTrace("ok returned");
		return RES_OK;
	}

    /*return "CONNECT OK" */
    else if(NULL != p2)
    { 
        ebdat7_01DebugTrace("CONNECT OK");
        savecmd((u8*)"AT+CIPSEND\r",curcmd,AT_WRITE);
        ebdat9_01SendToModem((u8*)"AT+CIPSEND\r", sizeof("AT+CIPSEND\r"));	 
        return RES_OK;
    }
    return RES_ELSE;		
}

/*******************************************************************************
 * Function:  CHECK_CIPSEND
 
 * Description: check the return value of AT+CIPSEND command  from modem
 
 *******************************************************************************/
static s8 CHECK_CIPSEND(u8 *data,t_curcmd *curcmd)
{
    s8  *p1=NULL;
    s8  *p2=NULL;
    t_emb_Timer timer;

    p1 = (s8 *)strstr((const char*)data,STR_INPUT);
    p2 = (s8 *)strstr((const char*)data,STR_SENDOK);
		
    /*return  ">" */		
    if(NULL != p1)
	{
        ebdat9_01SendToModem((u8*)"I WANT TO GET DATA\x1a",sizeof("I WANT TO GET DATA\x1a"));

        /*begin timer*/
        timer.timeoutPeriod = ebdat8_04SecondToTicks(MAXTIME);
        timer.timerId =1;
        ebdat8_01StartTimer( timer);		
        return RES_OK;
    }
    /*return  "SEND OK" */	
    if(NULL != p2)
    {	
        ebdat7_01DebugTrace("<<<CIPSEND OK>>>");
        clearcmd(curcmd);
        return RES_OK;
    }
    return RES_ELSE;
}

/*******************************************************************************
 * Function:  CHECK_CIPSHUT
 
 * Description: check the return value of AT+CIPSHUT command  from modem
 
 *******************************************************************************/
static s8 CHECK_CIPSHUT(u8 *data,t_curcmd *curcmd)
{
    s8 *p1=NULL;
    s8 *p2=NULL;
	
    p1 = (s8 *)strstr((const char *)data,STR_OK);
    p2 = (s8 *)strstr((const char *)data,STR_ERR);
	
    if(NULL != p1)
    {
        ebdat7_01DebugTrace("CIPSHUT!");
        return FL_END;
    }
    else if(NULL != p2)
    {
        return FL_ERROR;
    }
    else
    {
        return RES_ELSE;
    }
}

/*init tab of AT command*/
t_attab attab[] = {
    {(u8*)"AT+CIPMUX",CHECK_CIPMUX},
    {(u8*)"AT+CIPSTART",CHECK_CIPSTART},  
    {(u8*)"AT+CIPSEND",CHECK_CIPSEND},
    {(u8*)"AT+CIPSHUT",CHECK_CIPSHUT},
};


/*******************************************************************************
 * Function:  clearcmd
 
 * Description: clear the flcurcmd table.
 
 *******************************************************************************/
void  clearcmd(t_curcmd *cmd)
{
    memset(cmd->cmd_string,0,MAX_AT_LEN);
    cmd->cmd_type=AT_NULL;
}

/*******************************************************************************
 * Function:  savecmd
 
 * Description: save the command, make sure the command table contains the latest command
 
 *******************************************************************************/
void savecmd(u8 *cmddata, t_curcmd *cmd, ATCmdTyp cmdtype)
{
    char *p1 = NULL;
    char *p2 = NULL;		
		
    /*clean the flcurcmd content*/
    clearcmd(cmd);
	
    p1 = (char*)cmddata;
    p2 = strchr((const char *)cmddata,0x0D);
		
    memcpy(cmd->cmd_string,p1,p2-p1);
    cmd->cmd_type=cmdtype;
}
			



/***************************************************************************
*  Function:       fl_entry                                                                                                
*  Object:         Customer application                                                                            
*  Variable Name     |IN |OUT|GLB|  Utilisation                                                              

****************************************************************************/
void fl_entry()
{
    bool  keepGoing = TRUE;
		
    u16 i;
    s8 vl_res=-2; 
    s8 vl_num=0;
	
	 t_curcmd *flcurcmd;	
 
    flcurcmd = ebdat4_01GetMemory(sizeof(t_curcmd ));
    if( NULL== flcurcmd)
    {
        ebdat7_01DebugTrace("ALLOCATE MEMORY FAILED");
        return;
    }
	 
    /*clear the flcurcmd*/ 
    clearcmd(flcurcmd);
		 
    /* Echo mode off*/
    ebdat9_01SendToModem((u8*)"ATE0\r",strlen("ATE0\r"));
    ebdat7_00EnterDebugMode();
    vl_num = (sizeof(attab)/sizeof(t_attab));

    while (keepGoing == TRUE)
    {
        memset((u8*)&flEventBuffer,0x00,sizeof(flEventBuffer));
        eat1_02GetEvent(&flEventBuffer);
       
        switch(flEventBuffer.eventTyp)
        {	
            case EVENT_MODEMDATA:        
            {
                /*the AT command is CRWP.*/
                if( MODEM_CRWP == flEventBuffer.eventData.modemdata_evt.type)
                {    
                    /*set the destination of return values from modem to the FL_TASK*/
                    ebdat9_03SetModemdataToFL(TRUE);
										
                    /*start to execute the first AT command */			 
                    savecmd((u8*)"AT+CIPMUX\r", flcurcmd,AT_WRITE);
                    ebdat9_01SendToModem((u8*)"AT+CIPMUX=0\r", sizeof("AT+CIPMUX=0\r"));
										
                    /*trace*/				
                    ebdat7_01DebugTrace("string: %s type=: %d ", flcurcmd->cmd_string, flcurcmd->cmd_type);
	             }
								
                if( MODEM_CMD == flEventBuffer.eventData.modemdata_evt.type )
                {

                    if(AT_NULL != (flcurcmd->cmd_type) )
                    {
                        /*search  the AT COMMAND */
                        for(i=0;i<vl_num;i++)
                        {
                            /*compare the current command to attab.*/
                            if(strcmp((const char*)flcurcmd->cmd_string,(const char*)attab[i].cmd) == 0)
                            {
                                /*call the  function according to the current command*/						 	 
                                vl_res = attab[i].hd((u8*)flEventBuffer.eventData.modemdata_evt.data,flcurcmd);	

                                /*handle the return values of the function*/									
                                switch(vl_res)
                                {
                                    case RES_OK:	
                                    {                                         
                                        ebdat7_01DebugTrace("OK\r\n");											  
                                    }
                                    break;	

                                    /*other return values which does not affect the whole flow, send it to the serial port */																																				
                                    case RES_ELSE:
                                    {     
                                        ebdat7_01DebugTrace("UNKNOW RES ");	
                                        ebdat9_02SendToSerialPort((char * )flEventBuffer.eventData.modemdata_evt.data,\
																			 	                flEventBuffer.eventData.modemdata_evt.length);
                                    }
                                    break;
																		
                                    /*ERROR occurs in the flow*/
                                    case  FL_ERROR:									 	
                                    {                 
                                        ebdat7_01DebugTrace("%s flow ERROR !!! ",flcurcmd->cmd_string );	
                                        clearcmd(flcurcmd);		
                                        ebdat9_03SetModemdataToFL(FALSE);										 
                                    }		
                                    break;
										 /*the whole flow ends*/							
                                    case FL_END:
                                    {		
                                        clearcmd(flcurcmd);
                                        ebdat9_03SetModemdataToFL(FALSE); 
                                    }	
                                    break;
																		
                                    default:
                                    break;
                                }
                            }		                      								
                        }
                    }
										
                    else  
                    {
                        /*send the data received from server to serial port directly.*/
                        ebdat9_02SendToSerialPort((char * )flEventBuffer.eventData.modemdata_evt.data,flEventBuffer.eventData.modemdata_evt.length);
                        break;
                    }								
                }
            }
            break;

            case EVENT_UARTDATA:

            break;
	
            case EVENT_INTR:

            break;

            case EVENT_SERAILSTATUS:

            break;

            case EVENT_TIMER:            
            {
                /*exceed the max connection time, send CIPSHUT command to end the flow!*/
                ebdat7_01DebugTrace("time out!");
                savecmd((u8*)"AT+CIPSHUT\r", flcurcmd, AT_EXECUTE);
                ebdat9_01SendToModem((u8*)"AT+CIPSHUT\r",sizeof("AT+CIPSHUT\r"));
            }				
            break;
            default:
            break;
       }
    }
}

void fl_MultiTaskPrio1(void)
{
	while (TRUE)
	{
		eat1_02GetEvent(&flEventBufferM1);
		ebdat7_01DebugTrace((const char *)"fl_MultiTaskPrio1");
	}
}

void fl_MultiTaskPrio2(void)
{
	while (TRUE)
	{
		eat1_02GetEvent(&flEventBufferM2);
		ebdat7_01DebugTrace((const char *)"fl_MultiTaskPrio2");
	}
}

void fl_MultiTaskPrio3(void)
{
	while (TRUE)
	{
		eat1_02GetEvent(&flEventBufferM3);
		ebdat7_01DebugTrace((const char *)"fl_MultiTaskPrio3");
	}
}

void fl_MultiTaskPrio4(void)
{
	while (TRUE)
	{
		eat1_02GetEvent(&flEventBufferM4);
		ebdat7_01DebugTrace((const char *)"fl_MultiTaskPrio4");
	}
}

void fl_MultiTaskPrio5(void)
{
	while (TRUE)
	{
		ebdat7_01DebugTrace((const char *)"fl_MultiTaskPrio5");
		eat1_02GetEvent(&flEventBufferM5);
	}
}

void waitForSysInt(void)
{
	bool v_uartInit = FALSE;
	bool v_fileSystem = FALSE;
	while((v_uartInit == FALSE) ||
		(v_fileSystem == FALSE))
	{
		eat1_02GetEvent(&flEventBuffer);
		switch(flEventBuffer.eventTyp)
		{
			case EVENT_FLASH_READY:
				v_fileSystem = TRUE;
				break;
			case EVENT_UART_READY:
				v_uartInit = TRUE;
				break;
			default:
				break;
		}
	}

}
