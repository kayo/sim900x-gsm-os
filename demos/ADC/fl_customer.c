/***************************************************************************
 *
 *            SIMCOM Application Foreground Layer
 *
 *              Copyright (c) 2010 SIMCOM Ltd.
 *
 ***************************************************************************
 *
 *   $Revision: #1 $
 *   $DateTime: 2011/11/02 09:00:00 $
 *
 ***************************************************************************
 *
 *  Designed by    :   CY
 *  Coded by       :   CY
 *  Tested by      :
 *
 ***************************************************************************
 *
 * File Description
 * ----------------
 *  TCP processing fonctions:
 *  execute"AT+CRWP=1"command to start timer, check the voltage periodly if it is over 1.4v 
 *  GPIO1 will be drived up to high level. 
 * execut "AT+CRWP=2" command to stop timer, the checking action will be stopped.
 *  
 ***************************************************************************
 *
 ***************************************************************************/
#include "fl_stdlib.h"
#include "fl_typ.h"
#include "fl_appinit.h"
#include "fl_interface.h"
#include "fl_error.h"
#include "fl_timer.h"
#include "fl_fcm.h"
#include "fl_trace.h"
#include "fl_pinforsim900.h"
#include "fl_periphery.h"
FlEventBuffer	__align__ flEventBuffer;
FlEventBuffer	__align__ flEventBufferM1;
FlEventBuffer	__align__ flEventBufferM2;
FlEventBuffer	__align__ flEventBufferM3;
FlEventBuffer	__align__ flEventBufferM4;
FlEventBuffer	__align__ flEventBufferM5;
extern void waitForSysInt(void);
extern void fl_MultiTaskPrio1(void);
extern void fl_MultiTaskPrio2(void);
extern void fl_MultiTaskPrio3(void);
extern void fl_MultiTaskPrio4(void);
extern void fl_MultiTaskPrio5(void);
/** Init function */
fl_task_entry fl_init = waitForSysInt;

/** Entry functions */
fl_task_entry fl_entries[] = { fl_MultiTaskPrio1, fl_MultiTaskPrio2, fl_MultiTaskPrio3, fl_MultiTaskPrio4, fl_MultiTaskPrio5, NULL };
/***************************************************************************
*  Function:       fl_entry                                                                                                
*  Object:         Customer application                                                                            
*  Variable Name     |IN |OUT|GLB|  Utilisation                                                              

****************************************************************************/
void fl_entry()
{
    bool  keepGoing = TRUE;
    t_emb_Timer timer1;
    u32 para1;
    u32 status, value=0;
    FlPinName pinName=FL_PIN_37;
    ebdat7_00EnterDebugMode();
    
    if (ebdat6_08pinConfigureToUnused(pinName) == FL_OK) /*set pin to UNUSED status successfully*/
    {    
        if (ebdat6_02GpioSubscribe(pinName, FL_GPIO_OUTPUT, 0) == FL_OK)
        {
            ebdat7_01DebugTrace("Set FL_PIN_37 output successfully! \r\n");  
        }
        else
        {
            ebdat7_01DebugTrace("Set FL_PIN_37 output ERROR! \r\n");  
        }
    }
    else
    {
        ebdat7_01DebugTrace("ebdat6_08pinConfigureToUnused ERROR! \r\n");  
    }
    timer1.timeoutPeriod = ebdat8_05MillisecondToTicks(10000);  //10 seconds
    timer1.timerId=1;     
	ebdat9_01SendToModem((u8*)"AT+IPR=115200;+ICF=3,3\r",strlen("AT+IPR=115200;+ICF=3,3\r"));
    
    while (keepGoing == TRUE)
    {
        memset((u8*)&flEventBuffer,0x00,sizeof(flEventBuffer));
        eat1_02GetEvent(&flEventBuffer);
        /*set the timer include timeperiod and timerId*/

        switch(flEventBuffer.eventTyp)
        {
            case EVENT_MODEMDATA:
            {
                flEventBuffer.eventData.modemdata_evt.data[flEventBuffer.eventData.modemdata_evt.length] = 0;
                ebdat7_01DebugTrace((const char *)flEventBuffer.eventData.modemdata_evt.data);                                                          
                /*Received " AT+CRWP" command*/
                if( MODEM_CRWP == flEventBuffer.eventData.modemdata_evt.type )
                {
                    /*get AT+CRWP parameters */
                    sscanf((const char *)flEventBuffer.eventData.modemdata_evt.data, "AT+CRWP=%d",&para1);
                    ebdat7_01DebugTrace("\n\rGet para: %d\r\n", para1);

                    /*start the timer*/
                    if( 1 == para1 )
                    {     
                        ebdat9_03SetModemdataToFL(TRUE);
                        ebdat9_04SetUartdataToFL(TRUE);
                        
                        /*start timer 1 */
                        if( FL_OK == ebdat8_01StartTimer(timer1) )
                        {
                            ebdat7_01DebugTrace("!!!start timer1 : timeoutPeriod=%d,timerId=%d \r\n",timer1.timeoutPeriod,timer1.timerId);
                            /*Check CADC*/
                            ebdat9_01SendToModem((u8*)"AT+CADC?\r",strlen("AT+CADC?\r")); 
                        }
                    }
                                                                                
                    /*stop the timer*/
                    else if( 2 == para1 ) 
                    {        
                        /* kill timer1*/
                        ebdat8_02StopTimer(timer1);
                        ebdat7_01DebugTrace("timer1 is stopped!\r\n");
                        ebdat9_03SetModemdataToFL(FALSE);

                    }
                }
                
                /*check the result of "AT+CADC?" command "+CADC: <status>,<value>"*/
                if( MODEM_CMD == flEventBuffer.eventData.modemdata_evt.type )
                {
                    /*get the result */
                   if( (s8*)strstr((const char *)flEventBuffer.eventData.modemdata_evt.data,"+CADC")!=NULL)
                   {
                        ebdat7_01DebugTrace("\n\rGet result: %s\r\n", (const char *)flEventBuffer.eventData.modemdata_evt.data);
                        sscanf((const char *)flEventBuffer.eventData.modemdata_evt.data, "%*[^:]: %d,%d",&status, &value);    
                        ebdat7_01DebugTrace("\n\rGet result: status=%d, value=%d\r\n", status, value);
                        if((1==status) && (1400>value) )
                        {
                            /*Operating voltage is too low */
                            ebdat7_01DebugTrace("Votage too low!!!\r\n");
                            if (ebdat6_04WriteGpio(pinName, 0) == FL_OK)/*set the gpo level successfully*/
                            {
                                ebdat7_01DebugTrace("FL_PIN_37 -> high level! \r\n");                                
                            }              
                            else
                            {
                                ebdat7_01DebugTrace("ebdat6_04WriteGpio ERROR! \r\n"); 
                            }
                        }
						else if((1==status) && (1400<value) )
						{
                            if (ebdat6_04WriteGpio(pinName, 1) == FL_OK)/*set the gpo level successfully*/
                            {
                                ebdat7_01DebugTrace("FL_PIN_37 -> high level! \r\n");                                
                            }              
                            else
                            {
                                ebdat7_01DebugTrace("ebdat6_04WriteGpio ERROR! \r\n"); 
                            }
						}
                        else
                        {
                             ;
                        }
                    }
                }
                 /*MODEM_DATA */                                       
                else 
                {
                 ;
                }
            }
            break;
            case EVENT_UARTDATA:
            {
                if(flEventBuffer.eventData.uartdata_evt.type == DATA_SERIAL)
                {
                    flEventBuffer.eventData.uartdata_evt.data[flEventBuffer.eventData.uartdata_evt.length] = 0;
                    ebdat9_01SendToModem((u8*)flEventBuffer.eventData.uartdata_evt.data, flEventBuffer.eventData.uartdata_evt.length);
                }
            }
            break;
            case EVENT_INTR:

            break;
            case EVENT_SERAILSTATUS:

            break;
            case EVENT_TIMER:
            {   
                /* time out ,start it again*/                       
				if (1 == flEventBuffer.eventData.timer_evt.timer_id)
				{
					if( FL_OK == ebdat8_01StartTimer(timer1) )
					{
						ebdat7_01DebugTrace("!!!start timer1 : timeoutPeriod=%d,timerId=%d \r\n",timer1.timeoutPeriod,timer1.timerId);
						/*Check CADC*/
						ebdat9_01SendToModem((u8*)"AT+CADC?\r",strlen("AT+CADC?\r")); 
					}
				}
            }
            break;
            default:
            break;
        }
    }
}

void fl_MultiTaskPrio1(void)
{
	while (TRUE)
	{
		eat1_02GetEvent(&flEventBufferM1);
		ebdat7_01DebugTrace((const char *)"fl_MultiTaskPrio1");
	}
}

void fl_MultiTaskPrio2(void)
{
	while (TRUE)
	{
		eat1_02GetEvent(&flEventBufferM2);
		ebdat7_01DebugTrace((const char *)"fl_MultiTaskPrio2");
	}
}

void fl_MultiTaskPrio3(void)
{
	while (TRUE)
	{
		eat1_02GetEvent(&flEventBufferM3);
		ebdat7_01DebugTrace((const char *)"fl_MultiTaskPrio3");
	}
}

void fl_MultiTaskPrio4(void)
{
	while (TRUE)
	{
		eat1_02GetEvent(&flEventBufferM4);
		ebdat7_01DebugTrace((const char *)"fl_MultiTaskPrio4");
	}
}

void fl_MultiTaskPrio5(void)
{
	while (TRUE)
	{
		ebdat7_01DebugTrace((const char *)"fl_MultiTaskPrio5");
		eat1_02GetEvent(&flEventBufferM5);
	}
}

void waitForSysInt(void)
{
	bool v_uartInit = FALSE;
	bool v_fileSystem = FALSE;
	while((v_uartInit == FALSE) ||
		(v_fileSystem == FALSE))
	{
		eat1_02GetEvent(&flEventBuffer);
		switch(flEventBuffer.eventTyp)
		{
			case EVENT_FLASH_READY:
				v_fileSystem = TRUE;
				break;
			case EVENT_UART_READY:
				v_uartInit = TRUE;
				break;
			default:
				break;
		}
	}

}
