/***************************************************************************
 *
 *            SIMCOM Application Foreground Layer
 *
 *              Copyright (c) 2010 SIMCOM Ltd.
 *
 ***************************************************************************
 *
 *   $Revision: #1 $
 *   $DateTime: 2012/09/30 09:00:00 $
 *
 ***************************************************************************
 *
 *  Designed by    :   HJY
 *  Coded by       :   HJY
 *  Tested by      :
 *
 ***************************************************************************
 *
 * File Description
 * ----------------
 *   i2c processing functions (use timer)
 *
 *
 ***************************************************************************
 *
 ***************************************************************************/
#include "fl_stdlib.h"
#include "fl_typ.h"
#include "fl_appinit.h"
#include "fl_interface.h"
#include "fl_error.h"
#include "fl_fcm.h"
#include "fl_timer.h"
#include "fl_trace.h"
#include "Fl_pinforsim900.h"
#include "fl_periphery.h"

FlEventBuffer	__align__ flEventBuffer;
FlEventBuffer	__align__ flEventBufferM1;
FlEventBuffer	__align__ flEventBufferM2;
FlEventBuffer	__align__ flEventBufferM3;
FlEventBuffer	__align__ flEventBufferM4;
FlEventBuffer	__align__ flEventBufferM5;
extern void waitForSysInt(void);
extern void fl_MultiTaskPrio1(void);
extern void fl_MultiTaskPrio2(void);
extern void fl_MultiTaskPrio3(void);
extern void fl_MultiTaskPrio4(void);
extern void fl_MultiTaskPrio5(void);
/** Init function */
fl_task_entry fl_init = waitForSysInt;

/** Entry functions */
fl_task_entry fl_entries[] = { fl_MultiTaskPrio1, fl_MultiTaskPrio2, fl_MultiTaskPrio3, fl_MultiTaskPrio4, fl_MultiTaskPrio5, NULL };

/*I2C globle variables*/

u32 iic_read_data =0;
u32 iic_write_data=0; 
u8  iic_read_data_xyz[8] ={0};


u32 time_counter =0;
u8  SlaveAddressIIC=0x38; /* the slave address of MMA8452 etc. */




/* I2C read single byte */
u8 fl_IIC_SingleByteRead(u8 iic_device_address, u8 reg_address)
{
    
    Uint32 cmd=((reg_address<<8)|(iic_device_address)) ;
    GpsrTransferType *pTransfer;        
    Uint32 loop_counter=0;

    
    pTransfer=ebdat15_05I2C_INITIALIZE_TRANSFER();

    /* set i2c status:busy  */  
    ebdat15_04I2C_SetStatus(I2C_STATUS_BUSY);
	   
    /* config */    
    pTransfer->pCmd = &cmd;  
    pTransfer->cmdSize = 2;                                      
    pTransfer->pTxData = ((void *)0);                                       
    pTransfer->txDataSize = 0;                                           
    pTransfer->pRxData = (void *)&iic_read_data;                                           
    pTransfer->rxDataSize = 1;                                      
    pTransfer->pIsrCallbackFct = ebdat15_02I2C_ReadWriteDone;      

    /* get data */  
    ebdat15_07I2C_GET_DATA(pTransfer);
    
    
    /* wait until read finished */  
    while (I2C_STATUS_BUSY==ebdat15_03I2C_GetStatus()&&(loop_counter <0x1000))
    {
        loop_counter++;
    }  
	
    return (u8)iic_read_data;
}


/* I2C read  bytes (stored in iic_read_data_xyz[])*/
void fl_IIC_RegRead_XYZ(u8 iic_device_address, u8 reg_address)
{
    
    Uint32 cmd=((reg_address<<8)|(iic_device_address)) ;
    GpsrTransferType *pTransfer;           
    pTransfer=ebdat15_05I2C_INITIALIZE_TRANSFER();   

    
    /* set i2c status:busy  */  
    ebdat15_04I2C_SetStatus(I2C_STATUS_BUSY);
    
    /* config */  
    pTransfer->pCmd = &cmd;                                          
    pTransfer->cmdSize = 2;                                      
    pTransfer->pTxData = ((void *)0);                                       
    pTransfer->txDataSize = 0;                                           
    pTransfer->pRxData = (void *)&iic_read_data_xyz[0];                                           
    pTransfer->rxDataSize = 6;                                      
    pTransfer->pIsrCallbackFct = ebdat15_02I2C_ReadWriteDone;     
       
    /* get data */  
    ebdat15_07I2C_GET_DATA(pTransfer); 
	
    /* wait until read finished */  
    while (I2C_STATUS_BUSY==ebdat15_03I2C_GetStatus()); 
    
    return ;
}


/* I2C write one byte*/
void fl_IIC_RegWrite(u8 iic_device_address, u8 reg_address,u8 val)
{
    Uint32 cmd=((reg_address<<8)|(iic_device_address)) ;
    GpsrTransferType *pTransfer;           
    pTransfer=ebdat15_05I2C_INITIALIZE_TRANSFER();

    
    iic_write_data=(u32)val;

    /* set i2c status:busy  */  
    ebdat15_04I2C_SetStatus(I2C_STATUS_BUSY);
    
    
    /* config */ 
    pTransfer->pCmd = &cmd;                                          
    pTransfer->cmdSize = 2;                                      
    pTransfer->pTxData = (void *)&iic_write_data;                                           
    pTransfer->txDataSize = 1;                                      
    pTransfer->pIsrCallbackFct = ebdat15_02I2C_ReadWriteDone;   
    
    /* put data */                    
    ebdat15_06I2C_PUT_DATA(pTransfer); 

    /* wait until read finished */  
    while (I2C_STATUS_BUSY==ebdat15_03I2C_GetStatus()); 
    
    return ;

}


/***************************************************************************
*  Function:       fl_entry                                                  
*  Object:         Customer application                        
*  Variable Name     |IN |OUT|GLB|  Utilisation                           
***************************************************************************/
void fl_entry()
{
    bool  keepGoing = TRUE;
    t_emb_Timer timer1,timer2;
    t_emb_SysTimer stimer;
    u32 para1,para2,i;

    
    /*set the timer include timeperiod and timeId*/
    timer1.timeoutPeriod = ebdat8_05MillisecondToTicks(5000);
    timer1.timerId=1;

    /*get current system time */
    ebdat8_06GetSystemTime(&stimer);
    ebdat7_01DebugTrace("start time :%d(h)-%d(m)-%d(s)\r\n",  stimer.hour,stimer.minute,stimer.second);       

    /*start timer 1 */
    if( FL_OK == ebdat8_01StartTimer(timer1) )
    {
        ebdat7_01DebugTrace("!!!start timer1 : timeoutPeriod=%d,timerId=%d \r\n",timer1.timeoutPeriod,timer1.timerId);
    }
    
    while (keepGoing == TRUE)
    {
        memset((u8*)&flEventBuffer,0x00,sizeof(flEventBuffer));
        eat1_02GetEvent(&flEventBuffer);

        
        
        switch(flEventBuffer.eventTyp)
        {
            case EVENT_TIMER:
            {   
                
                /*judge the time_id, if it is timer1,restart timer1 1 second*/
                if (1 == flEventBuffer.eventData.timer_evt.timer_id)
                {
                    ebdat7_01DebugTrace("timer1 time out!\r\n");
										
                    /*set the timer include timeperiod and timeId*/
                    timer1.timeoutPeriod = ebdat8_05MillisecondToTicks(1000);
                    timer1.timerId=1;
                    

                    /*restart timer 1 */
                    if( FL_OK == ebdat8_01StartTimer(timer1) )
                    {
                        
                        ebdat7_01DebugTrace("!!!restart timer1 : timeoutPeriod=%d ms,timerId=%d,time_counter=%d \r\n",
                                            (timer1.timeoutPeriod*9230)/1000,
                                            timer1.timerId,
                                            time_counter);
                    }
                    
                    /* !CAUTION!: It's important to pull up SDA and SCL pins of I2C bus */
                    switch(time_counter)
                    {   
                        /* config I2C slave device @100Khz (run once) */
                        case 0: 
                        {
                             ebdat15_01I2C_SpeedConfig(I2C_SPEED_STD_RATE_100K);
                        }break; 
                        
                        /* send single byte to I2C slave device @100Khz */
                        case 1: 
                        {
                             fl_IIC_SingleByteRead (0x38,0x10); 
                        }break; 
                        
                        /* not change speed(keep100Khz),send single byte to I2C slave device */
                        case 2: 
                        {
                             fl_IIC_SingleByteRead (0x38,0x11); 
                        }break; 
                        #if 0
                        /* config I2C slave device @ 400Hz (run once) */
                        /* send single byte to I2C slave device @400Khz */
                        case 3: 
                        {
                             ebdat15_01I2C_SpeedConfig(I2C_SPEED_FAST_RATE_400K);
                             fl_IIC_SingleByteRead (0x38,0x40); 
                        }break;
                        /* not change speed(keep400Khz),send single byte to I2C slave device */
                        case 4: 
                        {
                             fl_IIC_SingleByteRead (0x38,0x11); 
                        }break; 
                        #endif
                        
                        default:
                            break;
                            
                    }
                    
                    time_counter++;
                    time_counter%=5;

                    if (0==time_counter)
                    {time_counter=1;}
                }
                
                /*get current system time */
                ebdat8_06GetSystemTime(&stimer);
                ebdat7_01DebugTrace("Time :%d(h)-%d(m)-%d(s)\r\n", 
                                    stimer.hour,
                                    stimer.minute,
                                    stimer.second);                                           
            }
            break;

            
            default:
            break;
        }
    }
}

void fl_MultiTaskPrio1(void)
{
	while (TRUE)
	{
		eat1_02GetEvent(&flEventBufferM1);
		ebdat7_01DebugTrace((const char *)"fl_MultiTaskPrio1");
	}
}

void fl_MultiTaskPrio2(void)
{
	while (TRUE)
	{
		eat1_02GetEvent(&flEventBufferM2);
		ebdat7_01DebugTrace((const char *)"fl_MultiTaskPrio2");
	}
}

void fl_MultiTaskPrio3(void)
{
	while (TRUE)
	{
		eat1_02GetEvent(&flEventBufferM3);
		ebdat7_01DebugTrace((const char *)"fl_MultiTaskPrio3");
	}
}

void fl_MultiTaskPrio4(void)
{
	while (TRUE)
	{
		eat1_02GetEvent(&flEventBufferM4);
		ebdat7_01DebugTrace((const char *)"fl_MultiTaskPrio4");
	}
}

void fl_MultiTaskPrio5(void)
{
	while (TRUE)
	{
		ebdat7_01DebugTrace((const char *)"fl_MultiTaskPrio5");
		eat1_02GetEvent(&flEventBufferM5);
	}
}

void waitForSysInt(void)
{
	bool v_uartInit = FALSE;
	bool v_fileSystem = FALSE;
	while((v_uartInit == FALSE) ||
		(v_fileSystem == FALSE))
	{
		eat1_02GetEvent(&flEventBuffer);
		switch(flEventBuffer.eventTyp)
		{
			case EVENT_FLASH_READY:
				v_fileSystem = TRUE;
				break;
			case EVENT_UART_READY:
				v_uartInit = TRUE;
				break;
			default:
				break;
		}
	}

}
