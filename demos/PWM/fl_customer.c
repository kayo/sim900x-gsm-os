/***************************************************************************
 *
 *            SIMCOM Application Foreground Layer
 *
 *              Copyright (c) 2010 SIMCOM Ltd.
 *
 ***************************************************************************
 *
 *   $Id: fl_flash.h#1 $
 *   $Revision: #1 $
 *   $DateTime: 2010/07/22 09:00:00 $
 *
 ***************************************************************************
 *
 *  Designed by    :   MXN
 *  Coded by       :   MXN
 *  Tested by      :
 *
 ***************************************************************************
 *
 * File Description
 * ----------------
 *   Flash processing fonctions
 *
 *
 ***************************************************************************
 *
 ***************************************************************************/

//#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include "fl_typ.h"
#include "fl_appinit.h"
#include "fl_interface.h"
#include "fl_error.h"
#include "fl_timer.h"
#include "fl_fcm.h"
#include "fl_flash.h"
#include "fl_trace.h"
#include "fl_pinforsim900.h"
#include "fl_periphery.h"
#include "fl_stdlib.h"

#define MEMORY_PID		0x00
#define MEMORY_PLEN	0x2000
#define MEMORY_PIDMAX	0x80

gascii ByteBuf[MEMORY_PLEN];/*if you want to define a char or unsigned char variable, you should use gu8 to define a variable.*/
u32  SysTick;
s32 p1, p2, p3, p4, p5, p6;/*if you want to define a char or unsigned char variable, you should use gu8 to define a variable.*/
FlEventBuffer	__align__ flEventBuffer;
FlEventBuffer	__align__ flEventBufferM1;
FlEventBuffer	__align__ flEventBufferM2;
FlEventBuffer	__align__ flEventBufferM3;
FlEventBuffer	__align__ flEventBufferM4;
FlEventBuffer	__align__ flEventBufferM5;
extern void waitForSysInt(void);
extern void fl_MultiTaskPrio1(void);
extern void fl_MultiTaskPrio2(void);
extern void fl_MultiTaskPrio3(void);
extern void fl_MultiTaskPrio4(void);
extern void fl_MultiTaskPrio5(void);
/** Init function */
fl_task_entry fl_init = waitForSysInt;

/** Entry functions */
fl_task_entry fl_entries[] = { fl_MultiTaskPrio1, fl_MultiTaskPrio2, fl_MultiTaskPrio3, fl_MultiTaskPrio4, fl_MultiTaskPrio5, NULL };
/***************************************************************************/
/*  Function:       fl_entry                                                  */
/*  Object:         Customer application                        */
/*  Variable Name     |IN |OUT|GLB|  Utilisation                           */
/***************************************************************************/
void fl_entry()
{
	u16 i;
	ebdat7_01DebugTrace((const char *)"\r\n<<<Entry Customer Task Work>>>\r\n");	

	ebdat9_03SetModemdataToFL(TRUE);
	ebdat9_01SendToModem((u8*)"AT+ICF=3,3;+IPR=115200\r",strlen("AT+ICF=3,3;+IPR=115200\r"));

	p1=9;
	p2=3;
	p3=4;
	p4=5;
	fl_memset(ByteBuf, 0x00, 20);
	i = sprintf((char *)ByteBuf, "AT+CRWP=%d,%d,%d",p1, p2, p3);
	ebdat7_01DebugTrace((const char *)"Sprintf %d : %d, %d, %d\r\n", p1, p2, p3);
	
//	SysTick = ebdat8_08GetSystemTickCounter( );
//	SysTick += 20;
//	while(ebdat8_08GetSystemTickCounter( )<=SysTick);
	
	i = sscanf((const char *)ByteBuf, "AT+CRWP=%d,%d,%d",&p1, &p2, &p3);
	ebdat7_01DebugTrace((const char *)"sscanf %d para: %d, %d, %d\r\n", i, p1, p2, p3);
	
	
	ebdat7_01DebugTrace((const char *)"<<<<<<<EAT Task Work Loop begin>>>>>>>");
	while (TRUE)
	{
		ebdat7_01DebugTrace("eat1_02GetEvent in");
		eat1_02GetEvent(&flEventBuffer);
		ebdat7_01DebugTrace((const char *)"eat1_02GetEvent OUT");
		switch(flEventBuffer.eventTyp)
		{
			case EVENT_TIMER:
				ebdat7_01DebugTrace((const char *)"EVENT_TIMER");
				break;
			case EVENT_MODEMDATA:
				ebdat7_01DebugTrace((const char *)"EVENT_MODEMDATA");
				ebdat7_01DebugTrace((const char *)"OUTPUTDATA TYPE: %x", flEventBuffer.eventData.modemdata_evt.type);
				if(flEventBuffer.eventData.modemdata_evt.type == MODEM_CMD)
				{
					if(ebdat9_05GetSerialPortTxStatus())
					{
						ebdat9_02SendToSerialPort((char *)flEventBuffer.eventData.modemdata_evt.data, flEventBuffer.eventData.modemdata_evt.length);
					}
				}
				else if(flEventBuffer.eventData.modemdata_evt.type == MODEM_CRWP)
				{
					flEventBuffer.eventData.modemdata_evt.data[flEventBuffer.eventData.modemdata_evt.length] = 0;
					ebdat7_01DebugTrace((const char *)flEventBuffer.eventData.modemdata_evt.data);
					i = sscanf((const char *)flEventBuffer.eventData.modemdata_evt.data, "AT+CRWP=%d,%d,%d,%d,%d,%d",&p1, &p2, &p3, &p4, &p5, &p6);
					ebdat7_01DebugTrace((const char *)"Get %d para: %d, %d, %d", i, p1, p2);
					switch(p1)/*FLASH API sample*/
					{
						case 1:/*PWM*/
						{
							/*PWMPRD:it is the frequnce period
							PWMPLS:it is the PWM pulse high time, it is equal to high time / period.
							 
							eg:
							ebdat6_19SqWaveSubscribe(FL_PWM_0, 100,50);
							PWMPRD:100--->101 pwmclk
							PWMPLS:50---->51 pwmclk
							 
							pwmclk=sysclk(26Mhz)/8=3.25Mhz
							 
							PWM out:3.25Mhz/101 = 32.178Khz
							high time:51*pwmclk
							 
							in our reference code there is limitation of input level.
							if (level*period/100) = 0
							then PWMPLS=127
							if PWMPLS>PWMPRD then pwm out low level
							 
							 
							if you want to set pwmclk=3.25Mhz/3=1.08Mhz
							ebdat6_19SqWaveSubscribe(FL_PWM_0, 2,50);
							PWMPRD:2--->3 pwmclk
							50*2/100 = 1
							PWMPLS:1---->2 pwmclk */
							switch(p2)
							{
								case 1:
								{
									/*Note: pwmhalfPeriod is the period of the PWM and the pwmlevel is the duty of PWM.
									            The pwmhalfPeriod should be less than 126 and the pwmlevel should be less than 100*/
									if (ebdat6_19SqWaveSubscribe((FlPWM)p3, (u8)p4, (u8)p5) == FL_OK)
									{
										if(ebdat9_05GetSerialPortTxStatus())
										{
											ebdat9_02SendToSerialPort((char *)"\r\nset pin to PWM successfully\r\n", sizeof("\r\nset pin to PWM successfully\r\n"));
										}
									}
									else
									{
										if(ebdat9_05GetSerialPortTxStatus())
										{
											ebdat9_02SendToSerialPort((char *)"\r\nset pin to PWM failed\r\n", sizeof("\r\nset pin to PWM failed\r\n"));
										}
									}
								}
								break;
								case 2:
								{
									if (ebdat6_20SqWaveUnsubscribe((FlPWM)p3) == FL_OK)
									{
										if(ebdat9_05GetSerialPortTxStatus())
										{
											ebdat9_02SendToSerialPort((char *)"\r\nset PWM to UNUSED successfully\r\n", sizeof("\r\nset PWM to UNUSED successfully\r\n"));
										}
									}
									else
									{
										if(ebdat9_05GetSerialPortTxStatus())
										{
											ebdat9_02SendToSerialPort((char *)"\r\nset PWM to UNUSED failed\r\n", sizeof("\r\nset PWM to UNUSED failed\r\n"));
										}
									}
								}
								break;
							}
						}
						break;
					}
				}
				break;
			case EVENT_UARTDATA :
				{
				}
				break;
			case EVENT_KEY:/*When you enable the keypad and a key is pressed or released, the EVENT_KEY will be received*/
				{
				}
				break;
			case EVENT_INTR:
				{
				}
				break;
			case EVENT_SERAILSTATUS:
				{
				}
				break;
			default:
				ebdat7_01DebugTrace((const char *)"defualt Event: %d\r\n", flEventBuffer.eventTyp);
				break;
		}
	}
}


void fl_MultiTaskPrio1(void)
{
	while (TRUE)
	{
		eat1_02GetEvent(&flEventBufferM1);
		ebdat7_01DebugTrace((const char *)"fl_MultiTaskPrio1");
	}
}

void fl_MultiTaskPrio2(void)
{
	while (TRUE)
	{
		eat1_02GetEvent(&flEventBufferM2);
		ebdat7_01DebugTrace((const char *)"fl_MultiTaskPrio2");
	}
}

void fl_MultiTaskPrio3(void)
{
	while (TRUE)
	{
		eat1_02GetEvent(&flEventBufferM3);
		ebdat7_01DebugTrace((const char *)"fl_MultiTaskPrio3");
	}
}

void fl_MultiTaskPrio4(void)
{
	while (TRUE)
	{
		eat1_02GetEvent(&flEventBufferM4);
		ebdat7_01DebugTrace((const char *)"fl_MultiTaskPrio4");
	}
}

void fl_MultiTaskPrio5(void)
{
	while (TRUE)
	{
		ebdat7_01DebugTrace((const char *)"fl_MultiTaskPrio5");
		eat1_02GetEvent(&flEventBufferM5);
	}
}

void waitForSysInt(void)
{
	bool v_uartInit = FALSE;
	bool v_fileSystem = FALSE;
	while((v_uartInit == FALSE) ||
		(v_fileSystem == FALSE))
	{
		eat1_02GetEvent(&flEventBuffer);
		switch(flEventBuffer.eventTyp)
		{
			case EVENT_FLASH_READY:
				v_fileSystem = TRUE;
				break;
			case EVENT_UART_READY:
				v_uartInit = TRUE;
				break;
			default:
				break;
		}
	}

}


