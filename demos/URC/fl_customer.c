#include <stdio.h>
#include <stdlib.h>
#include <fl_typ.h>
#include <fl_appinit.h>
#include <fl_trace.h>
#include <fl_fcm.h>
#include <fl_error.h>
#include <Fl_stdlib.h>
#include <fl_interface.h>
#include "ctype.h"

typedef struct URCCallBackStructTag
{
	u8 urcString[32];
	u32 stringLen;
	fl_urchandle hd;
	u8 isWholeStringCmp;
}URCCallBackStruct;

FlEventBuffer	__align__ flEventBuffer;
FlEventBuffer	__align__ flEventBufferM1;
FlEventBuffer	__align__ flEventBufferM2;
FlEventBuffer	__align__ flEventBufferM3;
FlEventBuffer	__align__ flEventBufferM4;
FlEventBuffer	__align__ flEventBufferM5;
extern void waitForSysInt(void);
extern void fl_MultiTaskPrio1(void);
extern void fl_MultiTaskPrio2(void);
extern void fl_MultiTaskPrio3(void);
extern void fl_MultiTaskPrio4(void);
extern void fl_MultiTaskPrio5(void);
/** Init function */
fl_task_entry fl_init = waitForSysInt;

/** Entry functions */
fl_task_entry fl_entries[] = { fl_MultiTaskPrio1, fl_MultiTaskPrio2, fl_MultiTaskPrio3, fl_MultiTaskPrio4, fl_MultiTaskPrio5, NULL };
URCCallBackStruct cfunURC;
u32 parameter1=0,parameter2=0;


void cfunHandle(u8 *data, u32 datalen)
{
	ebdat7_01DebugTrace("cfunHandle:%s",data);
	ebdat7_01DebugTrace("datalen:%d",datalen);
	while(ebdat9_05GetSerialPortTxStatus() == FALSE);
	ebdat9_02SendToSerialPort((char*)data,strlen((const char *)data));
	while(ebdat9_05GetSerialPortTxStatus() == FALSE);
	ebdat9_02SendToSerialPort((char*)"\r\nSIMCOM CFUN\r\n",strlen("\r\nSIMCOM CFUN\r\n"));
}


void InitURC(void)
{
	ebdat9_03SetModemdataToFL(TRUE);
	cfunURC.isWholeStringCmp = 0;
	strcpy((char *)cfunURC.urcString,"+CFUN:");
	cfunURC.stringLen = strlen((char *)cfunURC.urcString);
	cfunURC.hd = cfunHandle;
	if (ebdat9_15SubscribeURC(cfunURC.urcString,cfunURC.stringLen,cfunURC.hd,cfunURC.isWholeStringCmp) == FL_OK)
	{
		ebdat7_01DebugTrace("cfunURC SUB OK");
	}
}

void removeCRLFAndUpperString(char *s, char *d)
{
	char uart_log[256];
	u32 len = 0;
	u32 i = 0;
	ascii *p;
	memset(uart_log, 0x00, sizeof(uart_log));
	len = strlen(s);
	p = ebdat4_10strRemoveCRLF(uart_log, s, len);
	len = strlen(p);
	strcpy(d, p);
	for (i = 0; i < len; i++)
	{
		if (isalpha((int)d[i]))
		{
			d[i] = toupper((int)d[i]);
		}
	}
}


void fl_entry()
{
	bool keepGoing = TRUE;
	char          uart_log[128];
	s32 returnValue = FL_OK;
	u16 len;
	InitURC();
	memset(uart_log, 0x00, sizeof(uart_log));
	while (keepGoing == TRUE)
	{
		memset((u8*)&flEventBuffer,0x00,sizeof(flEventBuffer));
		eat1_02GetEvent(&flEventBuffer);
		switch(flEventBuffer.eventTyp)
		{
			case EVENT_MODEMDATA:
				if(flEventBuffer.eventData.modemdata_evt.type == MODEM_CMD)
				{
					len=flEventBuffer.eventData.modemdata_evt.length;
					ebdat9_02SendToSerialPort((char *)flEventBuffer.eventData.modemdata_evt.data,len);
				}
				if(flEventBuffer.eventData.modemdata_evt.type == MODEM_CRWP)
				{
					removeCRLFAndUpperString((char *)flEventBuffer.eventData.modemdata_evt.data, (char *)uart_log);
					sscanf(uart_log,"AT+CRWP=%d,%d",&parameter1,&parameter2);
					if (1 == parameter1)
					{
						returnValue = ebdat9_15SubscribeURC(cfunURC.urcString,cfunURC.stringLen,cfunURC.hd,cfunURC.isWholeStringCmp);
					}
					else if (2 == parameter1)
					{
						returnValue = ebdat9_16UnSubscribeURC(cfunURC.urcString);
					}

					if (returnValue == FL_OK)
					{
						ebdat9_02SendToSerialPort((char*)"OK\r\n",strlen("OK\r\n"));
					}
					else
					{
						ebdat9_02SendToSerialPort((char*)"ERROR\r\n",strlen("ERROR\r\n"));
						ebdat7_01DebugTrace("returnValue:%d", returnValue);
					}
				}
				break;
				default:
					break;
		}
	}
}

void fl_MultiTaskPrio1(void)
{
	while (TRUE)
	{
		eat1_02GetEvent(&flEventBufferM1);
		ebdat7_01DebugTrace((const char *)"fl_MultiTaskPrio1");
	}
}

void fl_MultiTaskPrio2(void)
{
	while (TRUE)
	{
		eat1_02GetEvent(&flEventBufferM2);
		ebdat7_01DebugTrace((const char *)"fl_MultiTaskPrio2");
	}
}

void fl_MultiTaskPrio3(void)
{
	while (TRUE)
	{
		eat1_02GetEvent(&flEventBufferM3);
		ebdat7_01DebugTrace((const char *)"fl_MultiTaskPrio3");
	}
}

void fl_MultiTaskPrio4(void)
{
	while (TRUE)
	{
		eat1_02GetEvent(&flEventBufferM4);
		ebdat7_01DebugTrace((const char *)"fl_MultiTaskPrio4");
	}
}

void fl_MultiTaskPrio5(void)
{
	while (TRUE)
	{
		ebdat7_01DebugTrace((const char *)"fl_MultiTaskPrio5");
		eat1_02GetEvent(&flEventBufferM5);
	}
}

void waitForSysInt(void)
{
	bool v_uartInit = FALSE;
	bool v_fileSystem = FALSE;
	while((v_uartInit == FALSE) ||
		(v_fileSystem == FALSE))
	{
		eat1_02GetEvent(&flEventBuffer);
		switch(flEventBuffer.eventTyp)
		{
			case EVENT_FLASH_READY:
				v_fileSystem = TRUE;
				break;
			case EVENT_UART_READY:
				v_uartInit = TRUE;
				break;
			default:
				break;
		}
	}

}
