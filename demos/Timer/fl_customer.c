/***************************************************************************
 *
 *            SIMCOM Application Foreground Layer
 *
 *              Copyright (c) 2010 SIMCOM Ltd.
 *
 ***************************************************************************
 *
 *   $Revision: #1 $
 *   $DateTime: 2010/07/22 09:00:00 $
 *
 ***************************************************************************
 *
 *  Designed by    :   CY
 *  Coded by       :   CY
 *  Tested by      :
 *
 ***************************************************************************
 *
 * File Description
 * ----------------
 *   timer processing fonctions
 *
 *   Sample1:  execute AT+CRWP=1 to get current system time.    
 *
 *   Sample2:  execute AT+CRWP=2 to trigger the timer flow: start time1 and timer2 at the 
 *                   same time,  time1 is shorter than timer2, when timer1 time out ,kill timer2.                                           
 *
 ***************************************************************************
 *
 ***************************************************************************/
#include "fl_stdlib.h"
#include "fl_typ.h"
#include "fl_appinit.h"
#include "fl_interface.h"
#include "fl_error.h"
#include "fl_fcm.h"
#include "fl_timer.h"
#include "fl_trace.h"
FlEventBuffer	__align__ flEventBuffer;
FlEventBuffer	__align__ flEventBufferM1;
FlEventBuffer	__align__ flEventBufferM2;
FlEventBuffer	__align__ flEventBufferM3;
FlEventBuffer	__align__ flEventBufferM4;
FlEventBuffer	__align__ flEventBufferM5;
extern void waitForSysInt(void);
extern void fl_MultiTaskPrio1(void);
extern void fl_MultiTaskPrio2(void);
extern void fl_MultiTaskPrio3(void);
extern void fl_MultiTaskPrio4(void);
extern void fl_MultiTaskPrio5(void);
/** Init function */
fl_task_entry fl_init = waitForSysInt;

/** Entry functions */
fl_task_entry fl_entries[] = { fl_MultiTaskPrio1, fl_MultiTaskPrio2, fl_MultiTaskPrio3, fl_MultiTaskPrio4, fl_MultiTaskPrio5, NULL };
/***************************************************************************
*  Function:       fl_entry                                                  
*  Object:         Customer application                        
*  Variable Name     |IN |OUT|GLB|  Utilisation                           
***************************************************************************/
void fl_entry()
{
    bool  keepGoing = TRUE;
    t_emb_Timer timer1,timer2;
    t_emb_SysTimer stimer;
    u32 para1,para2,i;
    while (keepGoing == TRUE)
    {
        memset((u8*)&flEventBuffer,0x00,sizeof(flEventBuffer));
        eat1_02GetEvent(&flEventBuffer);
       
        switch(flEventBuffer.eventTyp)
        {
            case EVENT_MODEMDATA:
            {
                flEventBuffer.eventData.modemdata_evt.data[flEventBuffer.eventData.modemdata_evt.length] = 0;
                ebdat7_01DebugTrace((const char *)flEventBuffer.eventData.modemdata_evt.data);								
                
                if( MODEM_CRWP == flEventBuffer.eventData.modemdata_evt.type )
                {
                    /*get AT+CRWP parameters */
                    i = sscanf((const char *)flEventBuffer.eventData.modemdata_evt.data, "AT+CRWP=%d,%d",&para1, &para2);
                    ebdat7_01DebugTrace("\n\rGet %d para: %d, %d, %d\r\n", i, para1, para2);

                    /*sample 1*/
                    if( 1 == para1 )
                    {  
                        /*get system time */
                        ebdat8_06GetSystemTime(&stimer);
                        ebdat7_01DebugTrace("system time is :%d(y)-%d(m)-%d(d),%d(h)-%d(m)-%d(s)",\
													stimer.year,stimer.month,stimer.day,stimer.hour,stimer.minute,stimer.second);                                       
                    }
										
                    /*sample 2*/
                    else if( 2 == para1 ) 
                    {        
                        /*set the timer include timeperiod and timeId*/
                        timer1.timeoutPeriod = ebdat8_05MillisecondToTicks(10000);
                        timer1.timerId=1;
                        timer2.timeoutPeriod = ebdat8_04SecondToTicks(20);
                        timer2.timerId=2;

                        /*get current system time */
                        ebdat8_06GetSystemTime(&stimer);
                        ebdat7_01DebugTrace("start time :%d(h)-%d(m)-%d(s)\r\n",  stimer.hour,stimer.minute,stimer.second);       
                    
                        /*start timer 1 */
                        if( FL_OK == ebdat8_01StartTimer(timer1) )
                        {
                            ebdat7_01DebugTrace("!!!start timer1 : timeoutPeriod=%d,timerId=%d \r\n",timer1.timeoutPeriod,timer1.timerId);
                        }
                        /*start timer2 */
                        else if( FL_OK == ebdat8_01StartTimer(timer2) )
                        {
                            ebdat7_01DebugTrace("!!!start timer2 : timeoutPeriod=%d,timerId=%d \r\n",timer2.timeoutPeriod,timer2.timerId);
                        }
                    }
                }

				  /*MODEM_CMD  or MODEM_DATA */					
				  else 
				  {
				      ;
				  }
            }
            break;
            case EVENT_UARTDATA:
     
            break;
            case EVENT_INTR:

            break;
            case EVENT_SERAILSTATUS:

            break;
            case EVENT_TIMER:
            {   
                /* time out ,get current time*/                       
                ebdat8_06GetSystemTime(&stimer);
                ebdat7_01DebugTrace("stop time :%d(h)-%d(m)-%d(s)\r\n", stimer.hour,stimer.minute,stimer.second);                                           

                /*judge the time_id, if it is timer1 ,kill timer2*/
                if (1 == flEventBuffer.eventData.timer_evt.timer_id)
                {
                    ebdat7_01DebugTrace("timer1 time out!\r\n");
										
                    /* kill timer2*/
                    ebdat8_02StopTimer(timer2);
                    ebdat7_01DebugTrace("timer2 is stopped!\r\n");
                }
								
                /*timer2 time out should not happened.if it occus,return ERROR*/
                else if (2 == flEventBuffer.eventData.timer_evt.timer_id)
                {
                    ebdat7_01DebugTrace("timer 2 ERROR!\r\n");
                }
                else
                {
                     ;
                }
            }
            break;
            default:
            break;
        }
    }
}

void fl_MultiTaskPrio1(void)
{
	while (TRUE)
	{
		eat1_02GetEvent(&flEventBufferM1);
		ebdat7_01DebugTrace((const char *)"fl_MultiTaskPrio1");
	}
}

void fl_MultiTaskPrio2(void)
{
	while (TRUE)
	{
		eat1_02GetEvent(&flEventBufferM2);
		ebdat7_01DebugTrace((const char *)"fl_MultiTaskPrio2");
	}
}

void fl_MultiTaskPrio3(void)
{
	while (TRUE)
	{
		eat1_02GetEvent(&flEventBufferM3);
		ebdat7_01DebugTrace((const char *)"fl_MultiTaskPrio3");
	}
}

void fl_MultiTaskPrio4(void)
{
	while (TRUE)
	{
		eat1_02GetEvent(&flEventBufferM4);
		ebdat7_01DebugTrace((const char *)"fl_MultiTaskPrio4");
	}
}

void fl_MultiTaskPrio5(void)
{
	while (TRUE)
	{
		ebdat7_01DebugTrace((const char *)"fl_MultiTaskPrio5");
		eat1_02GetEvent(&flEventBufferM5);
	}
}

void waitForSysInt(void)
{
	bool v_uartInit = FALSE;
	bool v_fileSystem = FALSE;
	while((v_uartInit == FALSE) ||
		(v_fileSystem == FALSE))
	{
		eat1_02GetEvent(&flEventBuffer);
		switch(flEventBuffer.eventTyp)
		{
			case EVENT_FLASH_READY:
				v_fileSystem = TRUE;
				break;
			case EVENT_UART_READY:
				v_uartInit = TRUE;
				break;
			default:
				break;
		}
	}

}
