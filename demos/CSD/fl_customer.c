/***************************************************************************
 *
 *            SIMCOM Application Foreground Layer
 *
 *              Copyright (c) 2011 SIMCOM Ltd.
 *
 ***************************************************************************
 *
 *   $Revision: #1 $
 *   $DateTime: 2011/12/26 09:00:00 $
 *
 ***************************************************************************
 *
 *  Designed by    :   HJ
 *  Coded by       :   HJ
 *  Tested by      :
 *
 ***************************************************************************
 *
 * File Description
 * ----------------
 *  csd processing fonctions:
 *  execute "AT+CRWP" command to start up CSD Call
 *  
 ***************************************************************************
 *
 ***************************************************************************/
#include <stdarg.h>
#include "fl_stdlib.h"
#include "fl_typ.h"
#include "fl_appinit.h"
#include "fl_interface.h"
#include "fl_error.h"
#include "fl_fcm.h"
#include "fl_timer.h"
#include "fl_trace.h"

#define MAX_AT_LEN 100

typedef enum CSDStatusTypeTag
{
    CSD_STATUS_IDLE = 0,
    CSD_STATUS_CALLING,
    CSD_STATUS_CALLED, 
    CSD_STATUS_CALLED_CMDED,
    NUM_OF_CSD_STATUS
}CSDStatusType;

typedef enum ATCmdTypTag
{
     AT_NULL=0,
	  AT_TEST,           
     AT_READ,          
     AT_WRITE,       
     AT_EXECUT,
     AT_ELSE           
}ATCmdTyp;

typedef enum ResTag
{
    RES_OK,              // AT command response is OK 
    RES_ERROR,
    RES_ELSE
}t_res;

typedef struct CurcmdTag
{
    ascii cmd_string[MAX_AT_LEN];
    u8   cmd_type;
} t_curcmd;

typedef s8(*fl_cmdhandle)(u8 *data,t_curcmd *curcmd);


typedef struct attab
{
    u8* cmd;
    fl_cmdhandle	hd;
}t_attab; 



static s8 CHECK_PLUS3(u8 *data,t_curcmd *curcmd);
static s8 CHECK_ATH(u8 *data,t_curcmd *curcmd);
static s8 CHECK_ATD(u8 *data,t_curcmd *curcmd);
static s8 updatecmd(u8 *cmddata, t_curcmd *cmd, ATCmdTyp cmdtype);



t_attab Attab[] = {
	 {(u8*)"ATD" ,  CHECK_ATD},
	 {(u8*)"+++" ,  CHECK_PLUS3},
	 {(u8*)"ATH" ,  CHECK_ATH},
};

t_curcmd *flcurcmd;	
gu8 gcsdNum[30];
gu8 csdConnect =  CSD_STATUS_IDLE;
gu8 csdretry = 0;

FlEventBuffer	__align__ flEvent;
FlEventBuffer	__align__ flEventBufferM1;
FlEventBuffer	__align__ flEventBufferM2;
FlEventBuffer	__align__ flEventBufferM3;
FlEventBuffer	__align__ flEventBufferM4;
FlEventBuffer	__align__ flEventBufferM5;
extern void waitForSysInt(void);
extern void fl_MultiTaskPrio1(void);
extern void fl_MultiTaskPrio2(void);
extern void fl_MultiTaskPrio3(void);
extern void fl_MultiTaskPrio4(void);
extern void fl_MultiTaskPrio5(void);
/** Init function */
fl_task_entry fl_init = waitForSysInt;

/** Entry functions */
fl_task_entry fl_entries[] = { fl_MultiTaskPrio1, fl_MultiTaskPrio2, fl_MultiTaskPrio3, fl_MultiTaskPrio4, fl_MultiTaskPrio5, NULL };


/*******************************************************************************
 * Function:  clearcmd
 
 * Description: clear the flcurcmd table.
 
 *******************************************************************************/
void  clearcmd(t_curcmd *cmd)
{
	/*set 0*/
	memset(cmd->cmd_string, 0x0, MAX_AT_LEN);
	cmd->cmd_type = AT_NULL;
}

/*******************************************************************************
 * Function:  updatecmd
 
 * Description: update the flcurcmd table, make sure it contains the latest command to be exauted.
 
 *******************************************************************************/
static s8 updatecmd(u8 *cmddata, t_curcmd *cmd, ATCmdTyp cmdtype)
{
    char *p1 = NULL;
    char *p2 = NULL;		
	
	 /*check the input parameter*/
	if(NULL==cmd||NULL==cmddata)
	{
		return RES_ERROR;
	}		
	//clean the flcurcmd content
    clearcmd(cmd);
	
    p1 = (char*)cmddata;
    p2 = strchr((const char *)cmddata,0x0D);		
    memcpy(cmd->cmd_string,p1,p2-p1);
    cmd->cmd_type=cmdtype;
	return RES_OK;
}


static s8 CHECK_ATD(u8 *data,t_curcmd *curcmd)
{
    s8 *p1=NULL;
    s8 *p2=NULL;
    s8 *p3=NULL;
    s8 *p4=NULL;
    s8 *p5=NULL;
 
    ebdat7_01DebugTrace("CHECK_ATD");

    /* Parser the return value*/
    p1 = (s8*)strstr((const char*)data,"NO CARRIER");
    p2 = (s8*)strstr((const char*)data,"ERROR");
    p3 = (s8*)strstr((const char*)data,"NO DIALTONE");
    p4 = (s8*)strstr((const char*)data,"BUSY");
    p5 = (s8*)strstr((const char*)data,"NO ANSWER");

    /* "ERROR" */    
	if((NULL!=p1) || (NULL!=p2) || (NULL!=p3) || (NULL!=p4) || (NULL!=p5))
    {
       csdConnect = CSD_STATUS_IDLE;//csd fail
       clearcmd(curcmd);	
		return RES_ERROR;
	}
  
    p1 = (s8*)strstr((const char*)data,"CONNECT");
    p2 = (s8*)strstr((const char*)data,"CONNECT 2400");
    p3 = (s8*)strstr((const char*)data,"CONNECT 4800");
    p4 = (s8*)strstr((const char*)data,"CONNECT 9600");

    /*OK*/
	if((NULL!=p1) || (NULL!=p2) || (NULL!=p3) || (NULL!=p4))
    {
      csdConnect = CSD_STATUS_CALLED;//csd OK
      csdretry = 0;
      clearcmd(curcmd);	
		return RES_OK;
	}


    return RES_ELSE;		
}

static s8 CHECK_PLUS3(u8 *data,t_curcmd *curcmd)
{
    s8 *p1=NULL;
 
 
    ebdat7_01DebugTrace("CHECK_PLUS3");

    /* Parser the return value*/
    p1 = (s8*)strstr((const char*)data,"OK");

    /* "ERROR" */    
	if((NULL!=p1) )
    {
        csdConnect = CSD_STATUS_CALLED_CMDED;
        clearcmd(curcmd);	
        updatecmd((u8*)"ATH\r", curcmd,AT_WRITE);
        ebdat9_01SendToModem((u8*)"ATH\r", strlen((const char*)"ATH\r"));
        return RES_OK;
	}

    return RES_ELSE;		
}

static s8 CHECK_ATH(u8 *data,t_curcmd *curcmd)
{
    s8 *p1=NULL;
    s8 *p2=NULL;
    s8 *p3=NULL;
    s8 *p4=NULL;
    s8 *p5=NULL;
 
    ebdat7_01DebugTrace("CHECK_ATH");


    /* Parser the return value*/
    p1 = (s8*)strstr((const char*)data,"OK");

    /* "OK" */    
	if((NULL!=p1) )
    {
        csdConnect = CSD_STATUS_IDLE;
        clearcmd(curcmd);	
        return RES_OK;
	}


    /* Parser the return value*/
    p1 = (s8*)strstr((const char*)data,"NO CARRIER");
    p2 = (s8*)strstr((const char*)data,"ERROR");
    p3 = (s8*)strstr((const char*)data,"NO DIALTONE");
    p4 = (s8*)strstr((const char*)data,"BUSY");
    p5 = (s8*)strstr((const char*)data,"NO ANSWER");

    /* "ERROR" */    
	if((NULL!=p1) || (NULL!=p2) || (NULL!=p3) || (NULL!=p4) || (NULL!=p5))
    {
      csdConnect = CSD_STATUS_IDLE;
      clearcmd(curcmd);	
		return RES_ERROR;
	}
    return RES_ELSE;		
}


void dealModemCRWP(FlEventBuffer *pEvent)
{     
    u8 sendAtBuffer[40]={0};
    memset(gcsdNum,0,30);
    pEvent->eventData.modemdata_evt.data[pEvent->eventData.modemdata_evt.length] = 0;
    ebdat7_01DebugTrace((const char *)pEvent->eventData.modemdata_evt.data);
    sscanf((const char*)pEvent->eventData.modemdata_evt.data, (const char*)"AT+CRWP=%s",gcsdNum);

    csdConnect=CSD_STATUS_CALLING;//send atd
    sprintf((char*)&sendAtBuffer[0], (const char*)"ATD%s\r",&gcsdNum[0]);
    updatecmd((u8*)"ATD\r", flcurcmd,AT_WRITE);
    ebdat9_01SendToModem((u8*)&sendAtBuffer[0],strlen((const char*)sendAtBuffer));
    /*set uart data to FL*/
    ebdat9_04SetUartdataToFL(TRUE);

 
}

void dealModemDATA(FlEventBuffer *pEvent)
{
    if(csdConnect == CSD_STATUS_CALLED)
    {
        while (!ebdat9_05GetSerialPortTxStatus());
        
        ebdat9_02SendToSerialPort((char*)pEvent->eventData.modemdata_evt.data, pEvent->eventData.modemdata_evt.length);  
        
    }
}

void dealModemCMD(FlEventBuffer *pEvent)
{
    ebdat7_01DebugTrace("dealModemCMD: %d,%s",pEvent->eventData.modemdata_evt.length,pEvent->eventData.modemdata_evt.data);

    if(csdConnect == CSD_STATUS_CALLED)
    {
        s8 *q1=NULL;
        s8 *q2=NULL;
        s8 *q3=NULL;
        s8 *q4=NULL;

        q1 = (s8*)strstr((const char *)pEvent->eventData.modemdata_evt.data,"NO CARRIER");//may be only no carrier
        q2 = (s8*)strstr((const char *)pEvent->eventData.modemdata_evt.data,"NO DIALTONE");
        q3 = (s8*)strstr((const char *)pEvent->eventData.modemdata_evt.data,"BUSY");
        q4 = (s8*)strstr((const char *)pEvent->eventData.modemdata_evt.data,"NO ANSWER");

        /* "NO CARRIER" */    
        if((NULL!=q1) || (NULL!=q2) || (NULL!=q3) || (NULL!=q4) )
        {
            csdConnect = CSD_STATUS_IDLE;//csd fail
        }
    }
    

    if( (flcurcmd->cmd_type)!= AT_NULL)
    {
         u16 i;
         s8 vl_res=0;
         
         for(i = 0;i<(sizeof(Attab)/sizeof(t_attab));i++)
         {
             /*compare the current command to attab*/
             if(strcmp((const char*)flcurcmd->cmd_string,(const char*)Attab[i].cmd) == 0)
             {   	
                 vl_res = Attab[i].hd((u8*)pEvent->eventData.modemdata_evt.data,flcurcmd);
                 
                 if ((RES_ERROR == vl_res) || (RES_OK == vl_res))
                 {
                     clearcmd(flcurcmd);
                 }
                 break;
             }		                      								
        }
    }
   
}


	
/***************************************************************************
*  Function:       fl_entry                                                  
*  Object:         Customer application                        
*  Variable Name     |IN |OUT|GLB|  Utilisation                           
***************************************************************************/
void fl_entry()
{
    bool  keepGoing = TRUE;
    s32 ret = 0;
    flcurcmd = ebdat4_01GetMemory(sizeof(t_curcmd));
    clearcmd(flcurcmd);

    ebdat9_03SetModemdataToFL(TRUE);
    
    while (keepGoing == TRUE)
    {
        memset((u8*)&flEvent, 0x0,sizeof(flEvent));
        eat1_02GetEvent(&flEvent);
 
        ebdat7_01DebugTrace("eventType = %ld", flEvent.eventTyp); 
        switch(flEvent.eventTyp)
        {
            case EVENT_MODEMDATA:
            { 
                ebdat7_01DebugTrace("modem Type = %ld", flEvent.eventData.modemdata_evt.type); 
                if(flEvent.eventData.modemdata_evt.type == MODEM_CRWP)
                {
                    dealModemCRWP(&flEvent);
                }
                if(flEvent.eventData.modemdata_evt.type == MODEM_DATA)
                {   
                    dealModemDATA(&flEvent);
                }
                if(flEvent.eventData.modemdata_evt.type == MODEM_CMD)
                {
                    u8 *p = NULL;
                    p = (u8*)strstr((char*)flEvent.eventData.modemdata_evt.data, "AT+CRWP");
                    if (p == NULL)
                    dealModemCMD(&flEvent);
                }
            }
            break;
			
            case EVENT_UARTDATA:
            { 
                if(flEvent.eventData.uartdata_evt.type == DATA_SERIAL)
                {
                    if(csdConnect == CSD_STATUS_CALLED)
                    {
                        ret = ebdat9_01SendToModem(flEvent.eventData.uartdata_evt.data,flEvent.eventData.uartdata_evt.length);
                        ebdat7_01DebugTrace("cse send %d ,ret = %ld",flEvent.eventData.uartdata_evt.length,ret); 
                        /*Input data 10 times later the firmware will hang up a CSD call with +++ and ATH, Customers can 
                        modify to adjust their application*/
                        csdretry++;
                        if(csdretry > 10)
                        {
                            updatecmd((u8*)"+++\r", flcurcmd,AT_WRITE);
                            ebdat9_01SendToModem((u8*)"+++",3);
                        }
                    }
                }
            }
            break;
			

            default:
            break;
        }
    }
}

void fl_MultiTaskPrio1(void)
{
	while (TRUE)
	{
		eat1_02GetEvent(&flEventBufferM1);
		ebdat7_01DebugTrace((const char *)"fl_MultiTaskPrio1");
	}
}

void fl_MultiTaskPrio2(void)
{
	while (TRUE)
	{
		eat1_02GetEvent(&flEventBufferM2);
		ebdat7_01DebugTrace((const char *)"fl_MultiTaskPrio2");
	}
}

void fl_MultiTaskPrio3(void)
{
	while (TRUE)
	{
		eat1_02GetEvent(&flEventBufferM3);
		ebdat7_01DebugTrace((const char *)"fl_MultiTaskPrio3");
	}
}

void fl_MultiTaskPrio4(void)
{
	while (TRUE)
	{
		eat1_02GetEvent(&flEventBufferM4);
		ebdat7_01DebugTrace((const char *)"fl_MultiTaskPrio4");
	}
}

void fl_MultiTaskPrio5(void)
{
	while (TRUE)
	{
		ebdat7_01DebugTrace((const char *)"fl_MultiTaskPrio5");
		eat1_02GetEvent(&flEventBufferM5);
	}
}

void waitForSysInt(void)
{
	bool v_uartInit = FALSE;
	bool v_fileSystem = FALSE;
	while((v_uartInit == FALSE) ||
		(v_fileSystem == FALSE))
	{
		eat1_02GetEvent(&flEvent);
		switch(flEvent.eventTyp)
		{
			case EVENT_FLASH_READY:
				v_fileSystem = TRUE;
				break;
			case EVENT_UART_READY:
				v_uartInit = TRUE;
				break;
			default:
				break;
		}
	}

}

