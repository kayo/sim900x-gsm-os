/***************************************************************************
 *
 *            SIMCOM Application Foreground Layer
 *
 *              Copyright (c) 2010 SIMCOM Ltd.
 *
 ***************************************************************************
 *
 *   $Revision: #1 $
 *   $DateTime: 2010/08/30 09:00:00 $
 *
 ***************************************************************************
 *
 *  Designed by    :   CY
 *  Coded by       :   CY
 *  Tested by      :
 *
 ***************************************************************************
 *
 * File Description
 * ----------------
 *   sms processing fonctions: execute AT+CRWP=1 to send a short message.
 *   command list:
 *   AT+CMGF=1
 *   AT+CSCS="UCS2"
 *   AT+CSMP=17,167,0,25   
 *   AT+CMGS="00310035003800300030003800300036003100300030"
 *   >4F60597D (ctrl+z)
 *
 ***************************************************************************
 *
 ***************************************************************************/
#include "fl_stdlib.h"
#include "fl_typ.h"
#include "fl_appinit.h"
#include "fl_interface.h"
#include "fl_error.h"
#include "fl_timer.h"
#include "fl_fcm.h"
#include "fl_flash.h"
#include "fl_trace.h"

#define STR_CME_EER		      "+CME ERROR:"
#define STR_ERR			         "ERROR"
#define STR_OK			             "OK"
#define STR_INPUT               ">"
#define SEND_OK                  "CMGS"

#define FL_END 100

#define MAX_AT_LEN 100
#define MAX_DATALEN 200

typedef enum ATCmdTypTag
{
    AT_NULL=0,
    AT_TEST,           
    AT_READ,          
    AT_WRITE,       
    AT_EXECUT,
    AT_ELSE           
}ATCmdTyp;

typedef enum ResTag
{
    RES_OK,              // AT command response is OK 
    RES_ERROR,
    RES_ELSE
}t_res;

typedef struct CurcmdTag
{
    ascii cmd_string[MAX_AT_LEN];
    u8   cmd_type;
} t_curcmd;

typedef s8(*fl_cmdhandle)(u8 *data,t_curcmd *curcmd);

typedef struct attab
{
    u8* cmd;
    fl_cmdhandle	hd;
}t_attab; 

void  clearcmd(t_curcmd *cmd);
static s8 updatecmd(u8 *cmddata, t_curcmd *cmd, ATCmdTyp cmdtype);

FlEventBuffer	__align__ flEventBuffer;
FlEventBuffer	__align__ flEventBufferM1;
FlEventBuffer	__align__ flEventBufferM2;
FlEventBuffer	__align__ flEventBufferM3;
FlEventBuffer	__align__ flEventBufferM4;
FlEventBuffer	__align__ flEventBufferM5;
extern void waitForSysInt(void);
extern void fl_MultiTaskPrio1(void);
extern void fl_MultiTaskPrio2(void);
extern void fl_MultiTaskPrio3(void);
extern void fl_MultiTaskPrio4(void);
extern void fl_MultiTaskPrio5(void);
/** Init function */
fl_task_entry fl_init = waitForSysInt;

/** Entry functions */
fl_task_entry fl_entries[] = { fl_MultiTaskPrio1, fl_MultiTaskPrio2, fl_MultiTaskPrio3, fl_MultiTaskPrio4, fl_MultiTaskPrio5, NULL };

/*******************************************************************************
 * Function:  CHECK_OK
 
 * Description: check the return value of AT command. 
 
 *******************************************************************************/
static s8 CHECK_OK(u8 *data, t_curcmd *curcmd)
{
	s8 *p1=NULL;
	s8 *p2=NULL;	
	p1 = (s8 *)strstr((const char *)data,STR_OK);
	p2 = (s8 *)strstr((const char *)data,STR_ERR);
	/*return OK*/
	if(NULL != p1)
   {	
		return RES_OK;  
	}
	/*return ERROR*/
	if(NULL != p2)
   {
		return RES_ERROR; 
	}		
	return RES_ELSE;   
}

/*******************************************************************************
 * Function:  CHECK_CMGF
 
 * Description: check the return value of AT+CMGF command  from modem, if the value is correct, 
                      do the following tasks.
 
 *******************************************************************************/
		
static s8 CHECK_CMGF(u8 *data, t_curcmd *curcmd)
{
	s8 checkok=RES_ERROR;
	checkok=CHECK_OK(data,curcmd);	 
	switch (checkok)
	{
	    case RES_OK:
	    {       
	        updatecmd((u8*)"AT+CSCS\r", curcmd,AT_WRITE);
	        ebdat9_01SendToModem((u8*)"AT+CSCS=\"UCS2\"\r", sizeof("AT+CSCS=\"UCS2\"\r"));
	        return RES_OK;
	    }
	    case RES_ERROR:
	    return RES_ERROR;
				 
	    case RES_ELSE:		
	    return RES_ELSE;
    }
    return FL_ERROR;
}

/*******************************************************************************
 * Function:  CHECK_CSCS
 
 * Description: check the return value of AT+CSCS command  from modem, if the value is correct, 
                      do the following tasks.
 
 *******************************************************************************/
static s8 CHECK_CSCS(u8 *data, t_curcmd *curcmd)
{
	s8 checkok=RES_ERROR;
	checkok=CHECK_OK(data,curcmd);	 
	switch (checkok)
	{
		case RES_OK:
		{   
			updatecmd((u8*)"AT+CSMP\r", curcmd,AT_WRITE);		
			ebdat9_01SendToModem((u8*)"AT+CSMP=17,167,0,25\r", sizeof("AT+CSMP=17,167,0,25\r"));
			return RES_OK;
		}
		case RES_ERROR:
		return RES_ERROR;
				 
		case RES_ELSE:		
		return RES_ELSE;
	}
	return FL_ERROR;
}

/*******************************************************************************
 * Function:  CHECK_CSMP
 
 * Description: check the return value of AT+CSMP command  from modem, if the value is correct, 
                      do the following tasks.
 
 *******************************************************************************/
static s8 CHECK_CSMP(u8 *data, t_curcmd *curcmd)
{
	s8 checkok=RES_ERROR;
	checkok=CHECK_OK(data,curcmd);	 
	switch (checkok)
	{
		case RES_OK:
		{   
			updatecmd((u8*)"AT+CMGS\r",  curcmd,AT_WRITE);
			/*please input your own cell number!*/
			ebdat9_01SendToModem((u8*)"AT+CMGS=\"00310035003800300030003800300036003100300030\"\r",\
			                                       sizeof("AT+CMGS=\"00310035003800300030003800300036003100300030\"\r"));		
			return RES_OK;
		}
		case RES_ERROR:
		return RES_ERROR;
				 
		case RES_ELSE:		
		return RES_ELSE;
	}
	return FL_ERROR;	
}

/*******************************************************************************
 * Function:  CHECK_CSGS
 
 * Description: check the return value of sms send command, if the return value contains"CMGS",
                      it indicates that the msg is sent to the destination successfully.
 
 *******************************************************************************/
static s8 CHECK_CMGS(u8 *data, t_curcmd *curcmd)
{
	s8 *pKey=NULL;
	pKey = (s8*)strstr((const char *)data,STR_INPUT);	
	if(NULL != pKey)
	{
		updatecmd((u8*)">\r",  curcmd,AT_ELSE);				
		ebdat9_01SendToModem((u8*)"4F60597D\x1a\r", sizeof("4F60597D\x1a\r")); 
		return RES_OK;
	}
	pKey = (s8*)strstr((const char *)data,STR_ERR);
	if(NULL != pKey)
	{
		return RES_ERROR;
	}
	return FL_ERROR;
}

/*******************************************************************************
 * Function: CHECK_RESULT
 
 * Description: check the return value of sms send command, if the return value contains"CMGS",
                      it indicates that the msg is sent to the destination successfully.
 
 *******************************************************************************/
static s8 CHECK_RESULT(u8 *data, t_curcmd *curcmd)
{
	s8 *pKey=NULL;
	pKey = (s8*)strstr((const char *)data,SEND_OK);
	
	if(NULL != pKey)
	{	
		ebdat7_01DebugTrace("***SMS SEND OK!!!***");
		return RES_OK;
	}
	pKey = (s8*)strstr((const char *)data,STR_OK);
	if(NULL != pKey)
	{
		curcmd->cmd_string[0]=0;
		curcmd->cmd_type=AT_NULL;
		ebdat7_01DebugTrace("***SMS SEND OVER!!!***");			
		return FL_END;
	}
	
	pKey = (s8*)strstr((const char *)data,STR_ERR);
	if(NULL != pKey)
	{
		ebdat7_01DebugTrace("***SMS SEND ERROR!!!***");		
		return RES_ERROR;
	}
	return FL_ERROR;
}

/*init tab of AT command*/
t_attab attab[] = {
  {(u8*)"AT+CMGF",CHECK_CMGF},
  {(u8*)"AT+CSCS",CHECK_CSCS},  
  {(u8*)"AT+CSMP",CHECK_CSMP},
  {(u8*)"AT+CMGS",CHECK_CMGS},
  {(u8*)">",CHECK_RESULT},
};


/*******************************************************************************
 * Function:  clearcmd
 
 * Description: clear the flcurcmd table.
 
 *******************************************************************************/
void  clearcmd(t_curcmd *cmd)
{
	/*set 0*/
	memset(cmd->cmd_string,0,MAX_AT_LEN);
	cmd->cmd_type=AT_NULL;
}

/*******************************************************************************
 * Function:  updatecmd
 
 * Description: update the flcurcmd table, make sure it contains the latest command to be exauted.
 
 *******************************************************************************/
static s8 updatecmd(u8 *cmddata, t_curcmd *cmd, ATCmdTyp cmdtype)
{
    char *p1 = NULL;
    char *p2 = NULL;		
	
	 /*check the input parameter*/
	if((NULL == cmd)||(NULL == cmddata))
	{
		  return FL_ERROR;
	}		
    clearcmd(cmd);	
    p1 = (char*)cmddata;
    p2 = strchr((const char *)cmddata,0x0D);		
    memcpy(cmd->cmd_string,p1,p2-p1);
    cmd->cmd_type=cmdtype;
	return FL_OK;
}
			


/***************************************************************************
*  Function:       fl_entry                                                                                                
*  Object:         Customer application                                                                             
*  Variable Name     |IN |OUT|GLB|  Utilisation                                                              
*                                                                                                                                                                              
****************************************************************************/
void fl_entry()
{
    bool  keepGoing = TRUE;
    u32 para1;
    u16 i;
    s8 result=-2; 

	 t_curcmd *flcurcmd;	
    flcurcmd = ebdat4_01GetMemory(sizeof(t_curcmd ));
    if( NULL== flcurcmd)
    {
        ebdat7_01DebugTrace("ALLOCATE MEMORY FAILED");
    }
	 
    /*INIT currentcmd table*/
    clearcmd(flcurcmd);

    /* Echo mode off*/
    ebdat9_01SendToModem((u8*)"ATE0\r",strlen("ATE0\r"));	
    ebdat7_00EnterDebugMode();

    while (keepGoing == TRUE)
    {
        memset((u8*)&flEventBuffer,0x00,sizeof(flEventBuffer));
        eat1_02GetEvent(&flEventBuffer);
       
        switch(flEventBuffer.eventTyp)
        {		

            case EVENT_MODEMDATA:        
            {
                if( MODEM_CRWP == flEventBuffer.eventData.modemdata_evt.type )
                {
                    flEventBuffer.eventData.modemdata_evt.data[flEventBuffer.eventData.modemdata_evt.length] = 0;
										
                    /*get AT+CRWP parameters */
                    i = sscanf((const char *)flEventBuffer.eventData.modemdata_evt.data, "AT+CRWP=%d",&para1);
										
                    /*execute AT+CRWP=1  to trigger sms sending function.*/
                    if( para1 == 1 )
                    {  												 
                        /*note the first AT command*/ 			
                        updatecmd((u8*)"AT+CMGF\r", flcurcmd,AT_WRITE);
								 
                        /*sends the output data from core to embedded application*/
                        ebdat9_03SetModemdataToFL(TRUE);
                        ebdat9_01SendToModem((u8*)"AT+CMGF=1\r", sizeof("AT+CMGF=1\r"));	
                    }
                }

                if( MODEM_CMD == flEventBuffer.eventData.modemdata_evt.type )
                {

                    if(AT_NULL != flcurcmd->cmd_type)
                    {
                        /*find the AT COMMAND from mytab*/
                        for(i = 0;i< (sizeof(attab)/sizeof(t_attab));i++)
                        {
                     
                            if(strcmp((const char*)flcurcmd->cmd_string,(const char*)attab[i].cmd) == 0)
                            {
                                /*compare the current command to attab*/
                                result = attab[i].hd((u8 *)flEventBuffer.eventData.modemdata_evt.data,flcurcmd);	
                                if( RES_OK == result )  
                                {                                         
                                    ebdat7_01DebugTrace("OK\r\n");										  					 
                                }
                                if( FL_END == result )
                                {								    	
                                    ebdat9_03SetModemdataToFL(FALSE);
                                }
										
                                if( RES_ERROR == result )
                                {      
                                    ebdat7_01DebugTrace("%s command return ERROR!",flcurcmd->cmd_string);	
																		
                                    /*clean the flcurcmd*/
                                    clearcmd(flcurcmd);
                                    ebdat9_03SetModemdataToFL(FALSE);
                                }
                                if( RES_ELSE == result )
                                {     
                                    ebdat7_01DebugTrace("UNKNOW RES ");	
                                    ebdat9_02SendToSerialPort((char * )flEventBuffer.eventData.modemdata_evt.data,\
																			flEventBuffer.eventData.modemdata_evt.length);
                                    break;
                                } 
																
                                /*ERROR occurs in the flow*/
                                if( FL_ERROR == result )  									 	
                                {                 
                                    ebdat7_01DebugTrace("flow ERROR in %s",flcurcmd->cmd_string );	
																		
                                    /*clean the flcurcmd*/
                                    clearcmd(flcurcmd);
																		
										 /*sends the output data from core to the port directly*/							
                                    ebdat9_03SetModemdataToFL(FALSE);
                                }											 
                                break;
                             }		                      								
                         }
                     }
					
                     else
                     {
                         /*sends string to serial port*/
                         ebdat9_02SendToSerialPort((char * )flEventBuffer.eventData.modemdata_evt.data,\
                                                               flEventBuffer.eventData.modemdata_evt.length);
                     }								
                 } 
                 else
                 {
                     ;
                 }
            }
            break;

            case EVENT_UARTDATA:

            break;
	
            case EVENT_INTR:

            break;

            case EVENT_SERAILSTATUS:

            break;

            case EVENT_TIMER:            
                          
            break;

            default:
            break;
        }
    }
}

void fl_MultiTaskPrio1(void)
{
	while (TRUE)
	{
		eat1_02GetEvent(&flEventBufferM1);
		ebdat7_01DebugTrace((const char *)"fl_MultiTaskPrio1");
	}
}

void fl_MultiTaskPrio2(void)
{
	while (TRUE)
	{
		eat1_02GetEvent(&flEventBufferM2);
		ebdat7_01DebugTrace((const char *)"fl_MultiTaskPrio2");
	}
}

void fl_MultiTaskPrio3(void)
{
	while (TRUE)
	{
		eat1_02GetEvent(&flEventBufferM3);
		ebdat7_01DebugTrace((const char *)"fl_MultiTaskPrio3");
	}
}

void fl_MultiTaskPrio4(void)
{
	while (TRUE)
	{
		eat1_02GetEvent(&flEventBufferM4);
		ebdat7_01DebugTrace((const char *)"fl_MultiTaskPrio4");
	}
}

void fl_MultiTaskPrio5(void)
{
	while (TRUE)
	{
		ebdat7_01DebugTrace((const char *)"fl_MultiTaskPrio5");
		eat1_02GetEvent(&flEventBufferM5);
	}
}

void waitForSysInt(void)
{
	bool v_uartInit = FALSE;
	bool v_fileSystem = FALSE;
	while((v_uartInit == FALSE) ||
		(v_fileSystem == FALSE))
	{
		eat1_02GetEvent(&flEventBuffer);
		switch(flEventBuffer.eventTyp)
		{
			case EVENT_FLASH_READY:
				v_fileSystem = TRUE;
				break;
			case EVENT_UART_READY:
				v_uartInit = TRUE;
				break;
			default:
				break;
		}
	}

}
