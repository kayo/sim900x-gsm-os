#include <stdio.h>
#include <stdlib.h>
#include <fl_typ.h>
#include <fl_interface.h>
#include <fl_appinit.h>
#include <fl_trace.h>
#include <fl_fcm.h>
#include <fl_error.h>
#include <Fl_stdlib.h>
#include <string.h>
#include <ctype.h>
u32 	parameter1=0,parameter2=0;
FlEventBuffer	__align__ flEventBuffer;
FlEventBuffer	__align__ flEventBufferM1;
FlEventBuffer	__align__ flEventBufferM2;
FlEventBuffer	__align__ flEventBufferM3;
FlEventBuffer	__align__ flEventBufferM4;
FlEventBuffer	__align__ flEventBufferM5;
extern void waitForSysInt(void);
extern void fl_MultiTaskPrio1(void);
extern void fl_MultiTaskPrio2(void);
extern void fl_MultiTaskPrio3(void);
extern void fl_MultiTaskPrio4(void);
extern void fl_MultiTaskPrio5(void);
/** Init function */
fl_task_entry fl_init = waitForSysInt;

/** Entry functions */
fl_task_entry fl_entries[] = { fl_MultiTaskPrio1, fl_MultiTaskPrio2, fl_MultiTaskPrio3, fl_MultiTaskPrio4, fl_MultiTaskPrio5, NULL };

void removeCRLFAndUpperString(char *s, char *d)
{
	char uart_log[128];
	u32 len = 0;
	u32 i = 0;
	ascii *p;
	memset(uart_log, 0x00, sizeof(uart_log));
	len = strlen(s);
	p = ebdat4_10strRemoveCRLF(uart_log, s, len);
	len = strlen(p);
	strcpy(d, p);
	for (i = 0; i < len; i++)
	{
		if (isalpha((int)d[i]))
		{
			d[i] = toupper((int)d[i]);
		}
	}
}

void InitATCommand(void)
{
	s32 returnValue;
	returnValue = ebdat9_19SubscribeATCommand((ascii *)"at+IPRCUST", 0x05);
	if (returnValue != FL_OK)
	{	
		ebdat7_01DebugTrace("IPRCUST:%d", returnValue);
	}
	returnValue = ebdat9_19SubscribeATCommand((ascii *)"at+ICFCUST", 0x8900);
	if (returnValue != FL_OK)
	{	
		ebdat7_01DebugTrace("ICFCUST:%d", returnValue);
	}

	returnValue = ebdat9_19SubscribeATCommand((ascii *)"at+IFCCUST", 0x1000);
	if (returnValue != FL_OK)
	{	
		ebdat7_01DebugTrace((const char *)"IFCCUST:%d", returnValue);
	}
}

void atIPRCustCommand(char *string)
{
	char uart_log[128];
	if (strcmp((const char *)string,(const char *)"AT+IPRCUST?")==(int)NULL)
	{
		memset(uart_log, 0x00, sizeof(uart_log));
		sprintf((char *)uart_log, (const char *)"+IPRCUST: %d\r\n",ebdat9_10GetMainUartBaudRate());
		ebdat9_02SendToSerialPort((char*)uart_log,strlen((const char *)uart_log));
		ebdat9_02SendToSerialPort((char*)"OK\r\n",strlen((const char *)"OK\r\n"));
	}
	else if(strcmp(string,"AT+IPRCUST=?")==(int)NULL)
	{
		ebdat7_01DebugTrace((char*)"AT+IPRCUST=?");
		ebdat9_02SendToSerialPort("+IPRCUST: (),(0,1200,2400,4800,9600,19200,38400,57600,115200)\r\n",
								strlen("+IPRCUST: (),(0,1200,2400,4800,9600,19200,38400,57600,115200)\r\n"));
		while (ebdat9_05GetSerialPortTxStatus() == 0);
		ebdat9_02SendToSerialPort((char*)"OK\r\n",strlen("OK\r\n"));
		
	}
	else if (strstr(string,"AT+IPRCUST=")!=NULL)
	{
		sscanf(string,"AT+IPRCUST=%d",&parameter1);
		ebdat7_01DebugTrace("parameter1:%d", parameter1 );
		if (ebdat9_09ChangeMainUartBaudRate(parameter1) == FL_OK)
		{
			ebdat9_02SendToSerialPort((char*)"OK\r\n",strlen("OK\r\n"));
		}
		else
		{
			ebdat9_02SendToSerialPort((char*)"ERROR\r\n",strlen("ERROR\r\n"));
		}
	}
	else
	{
		ebdat9_02SendToSerialPort((char*)"ERROR\r\n",strlen("ERROR\r\n"));
	}
}

void atICFCustCommand(char *string)
{
	char uart_log[128];
    FlMainUartDataFormat uartDataFormat;
	u8 paraNum = 0;
	memset(uart_log, 0x00, sizeof(uart_log));
	uartDataFormat = ebdat9_12GetMainUartDataFormat();
	if (strcmp(string,"AT+ICFCUST?")==(int)NULL)
	{
		sprintf(uart_log,"+ICFCUST: %d,%d\r\nOK\r\n",uartDataFormat.uartFormat,uartDataFormat.uartParity);
		ebdat9_02SendToSerialPort((char*)uart_log,strlen(uart_log));
	}
	else if (strcmp(string,"AT+ICFCUST=?")==(int)NULL)
	{
		sprintf(uart_log,"+ICFCUST: (1-6),(0,1,3)");
		ebdat9_02SendToSerialPort((char*)uart_log,strlen(uart_log));
	}
	else if (strstr(string,"AT+ICFCUST=")!=(char *)NULL)
	{
		paraNum = sscanf(string,"AT+ICFCUST=%d,%d",&parameter1,&parameter2);
		if ((paraNum == 1) || (paraNum == 2))
		{
			uartDataFormat.uartFormat = (FlMainUartFormat)parameter1;
			if (paraNum == 2)
			{
				uartDataFormat.uartParity= (FlMainUartParity)parameter2;
			}
			if (ebdat9_11ChangeMainUartDataFormat(uartDataFormat) == FL_OK)
			{
				ebdat9_02SendToSerialPort((char*)"OK\r\n",strlen("OK\r\n"));
			}
			else
			{
				ebdat9_02SendToSerialPort((char*)"ERROR1\r\n",strlen("ERROR1\r\n"));
			}
		}
		else
		{
			ebdat9_02SendToSerialPort((char*)"ERROR2\r\n",strlen("ERROR2\r\n"));
		}
	}
	else
	{
		ebdat9_02SendToSerialPort((char*)"ERROR3\r\n",strlen("ERROR3\r\n"));
	}
}

void atIFCCustCommand(char *string)
{
	char uart_log[128];
    FlMainUartFlowControlStruct uartFlowControl;
	u8 paraNum = 0;
	memset(uart_log, 0x00, sizeof(uart_log));
	uartFlowControl = ebdat9_14GetMainUartFlowControl();
	if (strcmp(string,"AT+IFCCUST?")==(int)NULL)
	{
		sprintf(uart_log,"+IFCCUST: %d,%d\r\nOK\r\n",uartFlowControl.dcebydte,uartFlowControl.dtebydce);
		ebdat9_02SendToSerialPort((char*)uart_log,strlen(uart_log));
	}
	else if (strcmp(string,"AT+IFCCUST=?")==(int)NULL)
	{
		sprintf(uart_log,"+IFCCUST: (0,2),(0,2)");
		ebdat9_02SendToSerialPort((char*)uart_log,strlen(uart_log));
	}
	else if (strstr(string,"AT+IFCCUST=")!=(char *)NULL)
	{
		paraNum = sscanf(string,"AT+IFCCUST=%d,%d",&parameter1,&parameter2);
		if ((paraNum == 1) || (paraNum == 2))
		{
			uartFlowControl.dcebydte = (FlMainUartFlowControl)parameter1;
			if (paraNum == 2)
			{
				uartFlowControl.dtebydce = (FlMainUartFlowControl)parameter2;
			}
			if (ebdat9_13ChangeMainUartFlowControl(uartFlowControl) == FL_OK)
			{
				ebdat9_02SendToSerialPort((char*)"OK\r\n",strlen("OK\r\n"));
			}
			else
			{
				ebdat9_02SendToSerialPort((char*)"ERROR1\r\n",strlen("ERROR1\r\n"));
			}
		}
		else
		{
			ebdat9_02SendToSerialPort((char*)"ERROR2\r\n",strlen("ERROR2\r\n"));
		}
	}
	else
	{
		ebdat9_02SendToSerialPort((char*)"ERROR3\r\n",strlen("ERROR3\r\n"));
	}
}


void fl_entry()
{
	bool keepGoing = TRUE;
	s32 returnValue;
	char uart_log[128];
	InitATCommand();
	while (keepGoing == TRUE)
	{
		memset((u8*)&flEventBuffer,0x00,sizeof(flEventBuffer));
		eat1_02GetEvent(&flEventBuffer);
		switch(flEventBuffer.eventTyp)
		{
			case EVENT_MODEMDATA:
				if(flEventBuffer.eventData.modemdata_evt.type == MODEM_CRWP)
				{
					removeCRLFAndUpperString((char *)flEventBuffer.eventData.modemdata_evt.data, (char *)uart_log);
					sscanf(uart_log,"AT+CRWP=%d,%d",&parameter1,&parameter2);
					returnValue = FL_ERROR;
					switch(parameter1)
					{
						case 0:
							if (parameter2 == 0)
							{
								returnValue = ebdat9_20UnsubscribeATCommand("at+IPRCUST");
							}
							else if (parameter2 == 1)
							{
								returnValue = ebdat9_19SubscribeATCommand("at+IPRCUST", 0x05);
							}
							break;
						case 1:
							if (parameter2 == 0)
							{
								returnValue = ebdat9_20UnsubscribeATCommand("at+ICFCUST");
							}
							else if (parameter2 == 1)
							{
								returnValue = ebdat9_19SubscribeATCommand("at+ICFCUST", 0x8900);
							}
							break;
						case 2:
							if (parameter2 == 0)
							{
								returnValue = ebdat9_20UnsubscribeATCommand("at+IFCCUST");
							}
							else if (parameter2 == 1)
							{
								returnValue = ebdat9_19SubscribeATCommand("at+IFCCUST", 0x1000);
							}
							break;
						case 3:
							ebdat9_09ChangeMainUartBaudRate(115200);
							break;
						case 4:
							ebdat9_09ChangeMainUartBaudRate(0);
							break;
						default:
							ebdat7_01DebugTrace("default");
							break;
					}
					if ((parameter1 != 3) && (parameter1 != 4))
					{
						if (returnValue == FL_OK)
						{
							ebdat9_02SendToSerialPort((char*)"OK\r\n",strlen("OK\r\n"));
						}
						else
						{
							ebdat9_02SendToSerialPort((char*)"ERROR\r\n",strlen("ERROR\r\n"));
							ebdat7_01DebugTrace("returnValue:%d", returnValue);
						}
					}
				}
				else if (flEventBuffer.eventData.modemdata_evt.type == MODEM_OTHER_AT)
				{
					ebdat7_01DebugTrace("%s", flEventBuffer.eventData.modemdata_evt.data );
					removeCRLFAndUpperString((char *)flEventBuffer.eventData.modemdata_evt.data, (char *)uart_log);
					if (flEventBuffer.eventData.modemdata_evt.atCommandIndex == 0x05)/*AT+IPRCUST*/
					{
						atIPRCustCommand(uart_log);
					}
					else if (flEventBuffer.eventData.modemdata_evt.atCommandIndex == 0x8900)/*AT+ICFCUST*/
					{
						atICFCustCommand(uart_log);
					}
					else if (flEventBuffer.eventData.modemdata_evt.atCommandIndex == 0x1000)/*AT+IFCCUST*/
					{
						atIFCCustCommand(uart_log);
					}
				}
				break;
				default:
					break;
		}
	}
}


void fl_MultiTaskPrio1(void)
{
	while (TRUE)
	{
		eat1_02GetEvent(&flEventBufferM1);
		ebdat7_01DebugTrace((const char *)"fl_MultiTaskPrio1");
	}
}

void fl_MultiTaskPrio2(void)
{
	while (TRUE)
	{
		eat1_02GetEvent(&flEventBufferM2);
		ebdat7_01DebugTrace((const char *)"fl_MultiTaskPrio2");
	}
}

void fl_MultiTaskPrio3(void)
{
	while (TRUE)
	{
		eat1_02GetEvent(&flEventBufferM3);
		ebdat7_01DebugTrace((const char *)"fl_MultiTaskPrio3");
	}
}

void fl_MultiTaskPrio4(void)
{
	while (TRUE)
	{
		eat1_02GetEvent(&flEventBufferM4);
		ebdat7_01DebugTrace((const char *)"fl_MultiTaskPrio4");
	}
}

void fl_MultiTaskPrio5(void)
{
	while (TRUE)
	{
		ebdat7_01DebugTrace((const char *)"fl_MultiTaskPrio5");
		eat1_02GetEvent(&flEventBufferM5);
	}
}

void waitForSysInt(void)
{
	bool v_uartInit = FALSE;
	bool v_fileSystem = FALSE;
	while((v_uartInit == FALSE) ||
		(v_fileSystem == FALSE))
	{
		eat1_02GetEvent(&flEventBuffer);
		switch(flEventBuffer.eventTyp)
		{
			case EVENT_FLASH_READY:
				v_fileSystem = TRUE;
				break;
			case EVENT_UART_READY:
				v_uartInit = TRUE;
				break;
			default:
				break;
		}
	}

}