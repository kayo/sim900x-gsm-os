default: build
all: default

coredir ?= .

incdir += $(coredir)/include
srcdir += $(coredir)/src
lddir := $(coredir)/ld
blobdir := $(coredir)/lib

include $(coredir)/toolchain.mk
include $(coredir)/features.mk

target ?= gsmfw
objdir ?= .obj

build: $(objdir)/$(target).bin

SFLAGS := -c -mlong-calls $(addprefix -I,$(incdir)) -mcpu=arm9e -mlittle-endian -mthumb-interwork -msoft-float -mfpu=vfp -Wall -Wstrict-prototypes -Os
CFLAGS := $(SFLAGS) -std=c99 -DCOMP_GCC
LDFLAGS := --just-symbols $(blobdir)/$(blob).x $(lddir)/ctype_.o -T $(lddir)/$(ld).ld -z muldefs
OBJCOPYFLAGS := -R .note -R .comment

xobj = $(addprefix $(objdir)/,$(patsubst %.$(1),%.$(1).o,$(subst /,--,$(subst ./,,$(subst ../,,$(2))))))
wsrc = $(foreach d,$(2),$(wildcard $(d)/*.$(1)))
src += $(call wsrc,c,$(srcdir))
asm += $(call wsrc,S,$(srcdir))
obj = $(call xobj,c,$(src)) $(call xobj,S,$(asm))

$(objdir)/$(target).bin: $(objdir)/$(target).elf
	$(OBJCOPY) $(OBJCOPYFLAGS) -O binary $< $@

$(objdir)/$(target).elf: $(obj)
	$(LD) $(LDFLAGS) -Map $(patsubst %.elf,%.map,$@) -o $@ $^

$(objdir):
	mkdir -p $(objdir)

define sobjr
$(call xobj,S,$(1)): $(1) $(objdir)
	$(CC) $(SFLAGS) -o $$@ $$<
endef

define cobjr
$(call xobj,c,$(1)): $(1) $(objdir)
	$(CC) $(CFLAGS) -o $$@ $$<
endef

$(foreach t,$(asm),$(eval $(call sobjr,$(t))))
$(foreach t,$(src),$(eval $(call cobjr,$(t))))

clean:
	$(RM) $(objdir)
