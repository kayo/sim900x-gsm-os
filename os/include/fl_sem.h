/***************************************************************************
 *
 *            SIMCOM Application Foreground Layer
 *
 *              Copyright (c) 2012 SIMCOM Ltd.
 *
 ***************************************************************************
 *
 *   $Id: fl_sem.h#2 $
 *   $Revision: #2 $
 *   $DateTime: 2012/02/28 09:00:00 $
 *
 ***************************************************************************
 *
 *  Designed by    :   MXN
 *  Coded by       :   MXN
 *  Tested by      :
 *
 ***************************************************************************
 *
 * File Description
 * ----------------
 *   Timer functions
 *
 *
 ***************************************************************************
 *
 ***************************************************************************/
#ifndef FL_SEM_H
#define FL_SEM_H

typedef enum FlSemaphoreIDTag
{
	FL_SEM_0,
	FL_SEM_1,
	FL_SEM_2,
	FL_SEM_3,
	FL_SEM_4,
	FL_SEM_5,
	FL_SEM_6,
	FL_SEM_7,
	FL_SEM_8,
	FL_SEM_9,
	FL_SEM_10,
	FL_SEM_11,
	FL_SEM_12,
	FL_SEM_13,
	FL_SEM_14,
	FL_SEM_15,
	NUM_OF_SEM
}FlSemaphoreID;
/***************************************************************************/
/*Function   : ebdat14_00CreateSem                                         */
/*-------------------------------------------------------------------------*/
/*Object     : Initialize a semaphore                                      */
/*                                                                         */
/*Return     : FL_RET_ERR_PARAM:     parameter incorrect.                  */
/*             FL_OK:                Initialize the semaphore successfully.*/
/*-------------------------------------------------------------------------*/
/*Variable Name    |IN |OUT|GLB| Utilisation                               */
/*--------------+--+----+---+----------------------------------------------*/
/*sem              | X |   |   | the semaphore which is used to initialize.*/
/*vp_Count         | X |   |   | set the default value of the semaphore.   */
/*vp_CountMax      | X |   |   | set the maximum value of the semaphore.   */
/*--------------+---+---+---+----------------------------------------------*/
extern s32 ebdat14_00CreateSem(FlSemaphoreID sem, int vp_Count, int vp_CountMax);
extern s32 (*const eat_CreateSem)(FlSemaphoreID sem, int vp_Count, int vp_CountMax);

/***************************************************************************/
/*Function   : ebdat14_01semPend                                           */
/*-------------------------------------------------------------------------*/
/*Object     : Pend a semaphore                                            */
/*                                                                         */
/*Return     : FL_RET_ERR_PARAM:     parameter incorrect.                  */
/*             FL_OK:                Pend the semaphore successfully.      */
/*-------------------------------------------------------------------------*/
/*Variable Name    |IN |OUT|GLB|   Utilisation                             */
/*--------------+--+---+---+-----------------------------------------------*/
/*sem              | X |   |   |   the semaphore which is used to pend.    */
/*--------------+---+---+---+----------------------------------------------*/
extern s32 ebdat14_01semPend(FlSemaphoreID sem);
extern s32 (*const eat_semPend)(FlSemaphoreID sem);

/***************************************************************************/
/*Function   : ebdat14_02semPost                                           */
/*-------------------------------------------------------------------------*/
/*Object     : Post a semaphore                                            */
/*                                                                         */
/*Return     : FL_RET_ERR_PARAM:     parameter incorrect.                  */
/*                FL_OK:             Post the semaphore successfully.      */
/*-------------------------------------------------------------------------*/
/*Variable Name    |IN |OUT|GLB|   Utilisation                             */
/*--------------+--+----+---+----------------------------------------------*/
/*sem              | X |   |   |   the semaphore which is used to post.    */
/*--------------+---+---+---+----------------------------------------------*/
extern s32 ebdat14_02semPost(FlSemaphoreID sem);
extern s32 (*const eat_semPost)(FlSemaphoreID sem);
#endif