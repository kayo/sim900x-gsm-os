/***************************************************************************
 *
 *            SIMCOM Application Foreground Layer
 *
 *              Copyright (c) 2011 SIMCOM Ltd.
 *
 ***************************************************************************
 *
 *   $Id: fl_bsd.h#1 $
 *   $Revision: #1 $
 *   $DateTime: 2011/12/30 09:00:00 $
 *
 ***************************************************************************
 *
 *  Designed by    :   HJ
 *  Coded by       :   HJ
 *  Tested by      :
 *
 ***************************************************************************
 *
 * File Description
 * ----------------
 *   Network related functions and definitions
 *
 *
 ***************************************************************************
 *
 ***************************************************************************/
#if !defined (FL_BSD_H)
#define       FL_BSD_H


/****************************************************************************
 *  Function Prototypes
 ***************************************************************************/

/***************************************************************************/
/*Function   : ebdat11_10GprsActive                                        */
/*-------------------------------------------------------------------------*/
/*Object     : This function is used to activate gprs bearer.              */
/*                                                                         */
/*Return     : FL_OK: Legal parameter, start to activate gprs scenario     */
/*             FL_ERROR: Illegal parameter, or gprs was already activated  */
/*-------------------------------------------------------------------------*/
/*Variable Name     |IN |OUT|GLB|  Utilisation                             */
/*apnName           | X |   |   |  the string of APN                       */
/*user              | X |   |   |  the string of user name, it can be null */
/*pass              | X |   |   |  the string of password, it can be null  */
/*------------------+---+---+---+------------------------------------------*/
/***************************************************************************/
extern s32 ebdat11_10GprsActive(u8 *apnName,u8 *user,u8 *pass);
extern s32 (*const eat_GprsActive)(u8 *apnName,u8 *user,u8 *pass);


/***************************************************************************/
/*Function   : ebdat11_15GprsDeactive                                      */
/*-------------------------------------------------------------------------*/
/*Object     : This function is used to release gprs bearer.               */
/*                                                                         */
/*Return     : FL_OK: Legal parameter, start to release gprs scenario      */
/*         FL_ERROR: gprs scenario was not activated and cannot be released*/
/*-------------------------------------------------------------------------*/
/*Variable Name     |IN |OUT|GLB|  Utilisation                             */
/*                                                                         */
/*------------------+---+---+---+------------------------------------------*/
/***************************************************************************/
extern s32 ebdat11_15GprsDeactive(void);
extern s32 (*const eat_GprsDeactive)(void);


/***************************************************************************/
/*Function   : ebdat11_20SocketConnect                                     */
/*-------------------------------------------------------------------------*/
/*Object     : This function is used to set up TCP or UDP socket           */
/*                                                                         */
/*Return     : The socket id, if any error occur,return 0xFFFFFFFF         */
/*-------------------------------------------------------------------------*/
/*Variable Name  |IN |OUT|GLB|  Utilisation                                */
/*type           | X |   |   |  TCP or UDP                                 */
/*url            | X |   |   |  Remote IP address or domain name           */
/*sockPort       | X |   |   |  Remote port number                         */
/*---------------+---+---+---+---------------------------------------------*/
/***************************************************************************/
extern u32 ebdat11_20SocketConnect(FlSocketType_e type,u8 *url, u16 sockPort);
extern u32 (*const eat_SocketConnect)(FlSocketType_e type,u8 *url, u16 sockPort);

/***************************************************************************/
/*Function   : ebdat11_25SocketClose                                       */
/*-------------------------------------------------------------------------*/
/*Object     : This function is used to close socket.                      */
/*                                                                         */
/*Return     : FL_OK: Legal parameter, start to close the socket.          */
/*             FL_ERROR: Socket has not been set up, and cannot be closed. */
/*-------------------------------------------------------------------------*/
/*Variable Name     |IN |OUT|GLB|  Utilisation                             */
/*socket            | X |   |   |  The socket id                           */
/*mode              | X |   |   |  1:quick close 0:slow close              */
/*------------------+---+---+---+------------------------------------------*/
/***************************************************************************/
extern s32 ebdat11_25SocketClose(u32 socket,u8 mode);
extern s32 (*const eat_SocketClose)(u32 socket,u8 mode);

/***************************************************************************/
/*Function   : ebdat11_30SocketSend                                        */
/*-------------------------------------------------------------------------*/
/*Object     : This function is used to send TCP or UDP data.              */
/*                                                                         */
/*Return     : FL_OK:  Legal parameter, start to send data.                */
/*                 FL_ERROR:  Parameter error or status error              */
/*-------------------------------------------------------------------------*/
/*Variable Name     |IN |OUT|GLB|  Utilisation                             */
/*socket            | X |   |   | The socket used for sending data         */
/*buf_p             | X |   |   |  The data buffer to be sent              */
/*len               | X |   |   |  The data length to be sent              */
/*------------------+---+---+---+------------------------------------------*/
/***************************************************************************/
extern s32 ebdat11_30SocketSend(u32 socket,void *buf_p, u16 len);
extern s32 (*const eat_SocketSend)(u32 socket,void *buf_p, u16 len);

/***************************************************************************/
/*Function   : ebdat11_35SocketRecv                                        */
/*-------------------------------------------------------------------------*/
/*Object     : This function is used to receive TCP or UDP data.           */
/*                                                                         */
/*Return     : The length of data received and written into buffer         */
/*                                                                         */
/*-------------------------------------------------------------------------*/
/*Variable Name     |IN |OUT|GLB|  Utilisation                             */
/*socket            | X |   |   |  Socket used for reading data            */
/*buf_p             |   | X |   |  buf used to store received data         */
/*len               | X |   |   |  max data length to be read              */
/*remain            | X |   |   |  the unread data length                  */
/*------------------+---+---+---+------------------------------------------*/
/***************************************************************************/
extern u16 ebdat11_35SocketRecv(u32 socket,void *buf_p, u16 len,u16 *remain);
extern u16 (*const eat_SocketRecv)(u32 socket,void *buf_p, u16 len,u16 *remain);

/***************************************************************************/
/*Function   : ebdat11_45SocketTcpServerSet                                */
/*-------------------------------------------------------------------------*/
/*Object     : This function is used to create a TCP server.               */
/*                                                                         */
/*Return     : FL_OK: Legal parameter, which is operating.                 */
/*             FL_ERROR:  Parameter error or status error                  */
/*-------------------------------------------------------------------------*/
/*Variable Name   |IN |OUT|GLB| Utilisation                                */
/*mode            | X |   |   | 1 means socket setup,0 means closing server*/
/*port            | X |   |   | Local monitor port, this parameter will not*/
/*                |   |   |   | be examined when mode is 0.                */
/*----------------+---+---+---+--------------------------------------------*/
/***************************************************************************/
extern s32 ebdat11_45SocketTcpServerSet(u8 mode,u16 port);
extern s32 (*const eat_SocketTcpServerSet)(u8 mode,u16 port);

/***************************************************************************/
/*Function   : ebdat11_50GetLocalIpAddr                                    */
/*-------------------------------------------------------------------------*/
/*Object     : This function is to get local IP address.                   */
/*                                                                         */
/*Return     : Return the local IP address of ebdat GPRS bearer.           */
/*-------------------------------------------------------------------------*/
/*                                                                         */
/*--------------------+---+---+---+----------------------------------------*/
/***************************************************************************/
extern u32 ebdat11_50GetLocalIpAddr(void);
extern u32 (*const eat_GetLocalIpAddr)(void);

#ifdef HAS_KEEPALIVE_CONTROL
/***************************************************************************/
/*Function   : ebdat11_51SetTcpKeepAlive                                   */
/*-------------------------------------------------------------------------*/
/*Object     : This function enables/disables TCP keep alive mechanism.    */
/*                                                                         */
/*Return     : NULL                                                        */
/*                                                                         */
/*-------------------------------------------------------------------------*/
/*Variable Name   |IN |OUT|GLB| Utilisation                                */
/*keepAliveEnable | X |   |   | TURE: Enable TCP keep alive mechanism.     */
/*                |   |   |   | FALSE: Disable TCP keep alive mechanism.   */
/*----------------+---+---+---+--------------------------------------------*/
/***************************************************************************/
extern void ebdat11_51SetTcpKeepAlive(bool keepAliveEnable);
extern void (*const eat_SetTcpKeepAlive)(bool keepAliveEnable);

/***************************************************************************/
/*Function   : ebdat11_52SetTcpKeepAliveValue                              */
/*-------------------------------------------------------------------------*/
/*Object     : This function is used to set the parameters of TCP keep alive*/
/*                                                                         */
/*Return     : TRUE: successfully.                                         */
/*             FALSE: failed.                                              */
/*-------------------------------------------------------------------------*/
/*Variable Name   |IN |OUT|GLB| Utilisation                                */
/*keepIdle        | X |   |   | Time(in second) before TCP sending the     */
/*                |   |   |   | initial keep alive probe, The default value*/
/*                |   |   |   |  is 7200S.                                 */
/*keepInterval    | X |   |   | Time(in second) between keep alive probes  */
/*                |   |   |   | retransmission. The default value is 75S.  */
/*keepCount       | X |   |   | Maximum number of keep alive probes to be  */
/*                |   |   |   | sent.The default value is 9.               */
/*----------------+---+---+---+--------------------------------------------*/
/***************************************************************************/
extern bool ebdat11_52SetTcpKeepAliveValue(u32 keepIdle, u32 keepInterval, 
u32 keepCount);
extern bool (*const eat_SetTcpKeepAliveValue)(u32 keepIdle, u32 keepInterval,
u32 keepCount);

/***************************************************************************/
/*Function   : ebdat11_53GetTcpMss                                         */
/*-------------------------------------------------------------------------*/
/*Object     : This function gets TCP MSS (Maximum Segment Size) value.    */
/*                                                                         */
/*Return     : Return the value of MSS (Maximum segment size).If MSS is -1,*/
/*             it shows that the TCP connection is not normal. It is       */
/*             noted that the function should be invoked immediately after */
/*             the TCP connection established. Otherwise the value is not  */
/*             correct.                                                    */
/*                                                                         */
/*-------------------------------------------------------------------------*/
/*Variable Name   |IN |OUT|GLB| Utilisation                                */
/*socket          | X |   |   | Socket id of TCP connection.               */
/*                |   |   |   |                                            */
/*----------------+---+---+---+--------------------------------------------*/
/***************************************************************************/
extern s16 ebdat11_53GetTcpMss(u32 socket);
extern s16 (*const eat_GetTcpMss)(u32 socket);
#endif


#endif /* FL_BSD_H */

/* END OF FILE */

