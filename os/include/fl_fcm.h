/***************************************************************************
 *
 *            SIMCOM Application Foreground Layer
 *
 *              Copyright (c) 2011 SIMCOM Ltd.
 *
 ***************************************************************************
 *
 *   $Id: fl_fcm.h#2 $
 *   $Revision: #2 $
 *   $DateTime: 2011/12/30 09:00:00 $
 *
 ***************************************************************************
 *
 *  Designed by    :   chenyang
 *  Coded by       :   chenyang
 *  Tested by      :
 *
 ***************************************************************************
 *
 * File Description
 * ----------------
 * Fcm functions
 *
 *
 ***************************************************************************
 *
 ***************************************************************************/
#ifndef FL_FCM_H
#define FL_FCM_H
/****************************************************************************
 *  Macros
 ***************************************************************************/



/****************************************************************************
 * Type Definitions
 ***************************************************************************/


/****************************************************************************
 * Function Prototypes
 ***************************************************************************/


/* Fcm Interface */

/*****************************************************************************/
/*Function   : ebdat9_01SendToModem                                          */
/*---------------------------------------------------------------------------*/
/*Object     :  This function sends data to core buffer                      */
/*              For AT commands, return information and                      */
/*              result codes Ok or ERROR are retrieved by eat1_02GetEvent    */
/*              function when ebdat9_03SetModemdataToFL is set to TRUE.      */
/*              A special character "\r" (cartridge return) should be        */
/*              appended after a string of AT command to indicate the ending.*/
/*              For example: ebdat9_01SendToModem ("ati\r",4) is the same    */
/*              as you type "ati" command and press ENTER.                   */
/*                                                                           */
/*Return     :  FL_OK                    success                             */
/*              FL_RET_ERR_PARAM         parameter error                     */
/*                                                                           */
/*---------------------------------------------------------------------------*/
/*Variable Name   |IN |OUT|GLB|  Utilisation                                 */
/*----------------+---+---+---+----------------------------------------------*/
/*senddata        | X |   |   |  the data to write into core buffer          */
/*data_len        | X |   |   |  the data length to write, can't exceed 1024 */
/*----------------+---+---+---+----------------------------------------------*/
/*****************************************************************************/
extern s32 ebdat9_01SendToModem(u8 *senddata, u16 data_len);
extern s32 (*const eat_SendToModem)(u8 *senddata, u16 data_len);

/*****************************************************************************/
/*Function   : ebdat9_02SendToSerialPort                                     */
/*---------------------------------------------------------------------------*/
/*Object     : This function is used to send string to serial port, valid    */
/*             only when ebdat9_05GetSerialPortTxStatus returns TRUE         */
/*             (means the transmit buffer is null).                          */
/*                                                                           */
/*Return     :   FL_OK                      success                          */
/*               FL_RET_ERR_PARAM           parameter error                  */
/*                                                                           */
/*---------------------------------------------------------------------------*/
/*Variable Name   |IN |OUT|GLB|  Utilisation                                 */
/*----------------+---+---+---+----------------------------------------------*/
/*src             | X |   |   |  the string you want to send to serial port  */
/*len             | X |   |   |  the lenght of the string, must be less than 256*/
/*----------------+---+---+---+----------------------------------------      */
/*****************************************************************************/
extern s32 ebdat9_02SendToSerialPort(char *src, u16 len);
extern s32 (*const eat_SendToSerialPort)(char *src, u16 len);

/*****************************************************************************/
/*Function   : ebdat9_03SetModemdataToFL                                     */
/*---------------------------------------------------------------------------*/
/*Object     : Controls the direction of output data from core               */
/*             (including return values and result codes "Ok" or "ERROR")    */
/*                                                                           */
/*Return     : none                                                          */
/*                                                                           */
/*---------------------------------------------------------------------------*/
/*Variable Name     |IN |OUT|GLB|  Utilisation                               */
/*------------------+---+---+---+--------------------------------------------*/
/*destination       | X |   |   |  TRUE(1) sends the output data from core   */
/*                                         to embeddedat application         */
/*                                 FALSE(0) sends the output data from core  */
/*                                         to serial port                    */
/*------------------+---+---+---+--------------------------------------------*/
/*****************************************************************************/
extern void ebdat9_03SetModemdataToFL(bool destination);
extern void (*const eat_SetModemdataToFL)(bool destination);

/*****************************************************************************/
/*Function   : ebdat9_04SetUartdataToFL                                      */
/*---------------------------------------------------------------------------*/
/*Object     : Controls the direction of input data from serial port         */
/*             if it is set to TRUE,the input data from serial port is sent  */
/*             to embedded application.                                      */
/*                                                                           */
/*Return     : none                                                          */
/*                                                                           */
/*---------------------------------------------------------------------------*/
/*Variable Name     |IN |OUT|GLB|  Utilisation                               */
/*------------------+---+---+---+--------------------------------------------*/
/*destination       | X |   |   |  TRUE(1) the input data from serial port   */
/*                                   is sent to embedded application.        */
/*                                 FALSE(0) for sending to core buffer       */
/*------------------+---+---+---+--------------------------------------------*/
/*****************************************************************************/
extern void ebdat9_04SetUartdataToFL(bool destination);
extern void (*const eat_SetUartdataToFL)(bool destination);

/*****************************************************************************/
/*Function   : ebdat9_05GetSerialPortTxStatus                                */
/*---------------------------------------------------------------------------*/
/*Object     :  Gets the transmit buffer's status of the serial port.        */
/*            If it returns FALSE, you cannot send any datum  to serial port.*/
/*                                                                           */
/*Return     :    TRUE(1)   the transmit buffer is null                      */
/*                FALSE(0)  there are data in the transmit buffer            */
/*                                                                           */
/*---------------------------------------------------------------------------*/
/*****************************************************************************/
extern bool ebdat9_05GetSerialPortTxStatus(void);
extern bool (*const eat_GetSerialPortTxStatus)(void);

/*****************************************************************************/
/* Procedure name :  ebdat9_09ChangeMainUartBaudRate                         */
/* Object :          change main uart baudrate                               */
/*                                                                           */
/* Return :                                                                  */
/*                FL_OK: set successfully                                    */
/*                FL_ERROR: set failed                                       */
/*---------------------------------------------------------------------------*/
/*Variable Name   |IN |OUT|GLB|  Utilisation                                 */
/*----------------+---+---+---+----------------------------------------------*/
/*BaudRate        | X |   |   |  Baudrate to set. If you want to set it  	 */
/*                               to 115200, the value should be 115200       */
/*                               It cannot be larger than 115200.            */
/*------------------+---+---+---+--------------------------------------------*/
/*****************************************************************************/
extern s32 ebdat9_09ChangeMainUartBaudRate(u32 BaudRate);
extern s32 (*const eat_ChangeMainUartBaudRate)(u32 BaudRate);

/*****************************************************************************/
/* Procedure name :  ebdat9_10GetMainUartBaudRate                            */
/* Object :  get the main uart baudrate                                      */
/*                                                                           */
/* Return :                                                                  */
/*        current main uart baudrate setting                                 */
/*---------------------------------------------------------------------------*/
/*****************************************************************************/
extern u32 ebdat9_10GetMainUartBaudRate(void);
extern u32 (*const eat_GetMainUartBaudRate)(void);

/*****************************************************************************/
/* Procedure name :  ebdat9_11ChangeMainUartDataFormat                       */
/* Object :  change main uart data format                                    */
/*---------------------------------------------------------------------------*/
/* Return                                                                    */
/*           FL_OK: set successfully                                         */
/*           FL_ERROR: set failed                                            */
/*---------------------------------------------------------------------------*/
/*Variable Name   |IN |OUT|GLB|  Utilisation                                 */
/*----------------+---+---+---+----------------------------------------------*/
/*uartDataFormat  | X |   |   |  main uart data format to be set.            */
/*----------------+---+---+---+----------------------------------------------*/
/*****************************************************************************/
extern s32 ebdat9_11ChangeMainUartDataFormat(FlMainUartDataFormat uartDataFormat);
extern s32 (*const eat_ChangeMainUartDataFormat)(FlMainUartDataFormat uartDataFormat);

/*****************************************************************************/
/* Procedure name :  ebdat9_12GetMainUartDataFormat                          */
/* Object :  get the main uart data format                                   */
/*---------------------------------------------------------------------------*/
/*Return:                                                                    */
/* -------------------                                                       */
/*           main uart data format                                           */
/*---------------------------------------------------------------------------*/
/*****************************************************************************/
extern FlMainUartDataFormat ebdat9_12GetMainUartDataFormat(void);
extern FlMainUartDataFormat (*const eat_GetMainUartDataFormat)(void);

/*****************************************************************************/
/* Procedure name :  ebdat9_13ChangeMainUartFlowControl                      */
/* Object :  set the main uart flow control                                  */
/*---------------------------------------------------------------------------*/
/* Return                                                                    */
/* -------------------                                                       */
/*           FL_OK: set successfully                                         */
/*           FL_ERROR: set failed                                            */
/*---------------------------------------------------------------------------*/
/*Variable Name   |IN |OUT|GLB|  Utilisation                                 */
/*----------------+---+---+---+----------------------------------------------*/
/*flowControl     | X |   |   |  main uart flow control to be set.           */
/*----------------+---+---+---+----------------------------------------------*/
/*****************************************************************************/
extern s32 ebdat9_13ChangeMainUartFlowControl(FlMainUartFlowControlStruct flowControl);
extern s32 (*const eat_ChangeMainUartFlowControl)(FlMainUartFlowControlStruct flowControl);

/*****************************************************************************/
/* Procedure name :  ebdat9_14GetMainUartFlowControl                         */
/* Object :  get the main uart flow control                                  */
/*---------------------------------------------------------------------------*/
/*Return                                                                     */
/* -------------------                                                       */
/*           main uart data flow control                                     */
/*---------------------------------------------------------------------------*/
/*****************************************************************************/
extern FlMainUartFlowControlStruct ebdat9_14GetMainUartFlowControl(void);
extern FlMainUartFlowControlStruct (*const eat_GetMainUartFlowControl)(void);

/*****************************************************************************/
/* Procedure name :  ebdat9_15SubscribeURC                                   */
/* Object :  Subscribe a URC                                                 */
/*---------------------------------------------------------------------------*/
/* Return                                                                    */
/* -------------------                                                       */
/*           FL_RET_ERR_PARAM: parameter error                               */
/*           FL_RET_ERR_ALREADY_SUBSCRIBED: The URC has been subscribed.     */
/*           FL_ERROR: Subscribe the URC failed. The number of               */
/*                     subscribed URC reach the maximum .                    */
/*           FL_OK: Subscribe the URC successfully.                          */
/*---------------------------------------------------------------------------*/
/*Variable Name   |IN |OUT|GLB|  Utilisation                                 */
/*----------------+---+---+---+----------------------------------------------*/
/*urcString       | X |   |   |  The URC to be subscribed. The maximum of    */
/*                               the URC which can be subscribed is 32.      */
/*stringLen       | X |   |   |  The length of the URC string                */
/*hd              | X |   |   |  The call back function                      */
/*isWholeStringCmp  | X |  |  |  if it is 1, the call back function          */
/*                               will be called  when the URC is identical   */
/*                               to the subscribed string. if it is 0,       */
/*                                the call back function will be called      */
/*                               when the URC contains one part of the       */
/*                               subscribed string.                          */
/*----------------+---+---+---+----------------------------------------------*/
/*****************************************************************************/
extern s32 ebdat9_15SubscribeURC(u8 *urcString, u32 stringLen, fl_urchandle hd, u8 isWholeStringCmp);
extern s32 (*const eat_SubscribeURC)(u8 *urcString, u32 stringLen, fl_urchandle hd, u8 isWholeStringCmp);

/*****************************************************************************/
/* Procedure name :  ebdat9_16UnSubscribeURC                                 */
/* Object :  Unsubscribe a URC                                               */
/*---------------------------------------------------------------------------*/
/* Return                                                                    */
/* -------------------                                                       */
/*           FL_RET_ERR_PARAM: parameter error                               */
/*           FL_RET_ERR_NOT_SUBSCRIBED: The URC has not been subscribed.     */
/*           FL_ERROR: Unsubscribe the URC failed.                           */
/*           FL_OK: Unsubscribe the URC successfully.                        */
/*---------------------------------------------------------------------------*/
/*Variable Name   |IN |OUT|GLB|  Utilisation                                 */
/*----------------+---+---+---+----------------------------------------------*/
/*urcString       | X |   |   |  The URC to be unsubscribed.                 */
/*----------------+---+---+---+----------------------------------------------*/
/*****************************************************************************/
extern s32 ebdat9_16UnSubscribeURC(u8 *urcString);
extern s32 (*const eat_UnSubscribeURC)(u8 *urcString);

/*****************************************************************************/
/* Procedure name :  ebdat9_17GetURCNum                                      */
/* Object :  Get the number of the subscribed URCs                           */
/*---------------------------------------------------------------------------*/
/* Return                                                                    */
/* -------------------                                                       */
/*           the number of the subscribed URCs                               */
/*****************************************************************************/
extern u8 ebdat9_17GetURCNum(void);
extern u8 (*const eat_GetURCNum)(void);

/*****************************************************************************/
/* Procedure name :  ebdat9_19SubscribeATCommand                             */
/* Object :  Subscribe a AT command                                          */
/*---------------------------------------------------------------------------*/
/* Return                                                                    */
/* -------------------                                                       */
/*           FL_RET_ERR_PARAM: parameter error                               */
/*           FL_RET_ERR_ALREADY_SUBSCRIBED: The AT command has been          */
/*                                          subscribed.                      */
/*           FL_ERROR: Subscribe the URC failed. The number of               */
/*                     subscribed AT commands reach the maximum.             */
/*           FL_OK: Subscribe the URC successfully.                          */
/*---------------------------------------------------------------------------*/
/*Variable Name   |IN |OUT|GLB|  Utilisation                                 */
/*----------------+---+---+---+----------------------------------------------*/
/*urcString       | X |   |   |The AT command to be subscribed.              */
/*                             You can subscribe a maximum of 32             */
/*                             The length of each can't exceed 20 bytes.     */
/*index           | X |   |   |index associated to the AT command            */
/*                             and it cannot be 0xFFFFFFFF.                  */
/*----------------+---+---+---+----------------------------------------------*/
/*****************************************************************************/
extern s32 ebdat9_19SubscribeATCommand(ascii *urcString, u32 index);
extern s32 (*const eat_SubscribeATCommand)(ascii *urcString, u32 index);

/*****************************************************************************/
/* Procedure name :  ebdat9_20UnsubscribeATCommand                           */
/* Object :  Unsubscribe a AT command                                        */
/*---------------------------------------------------------------------------*/
/* Return                                                                    */
/* -------------------                                                       */
/*           FL_RET_ERR_PARAM: parameter error                               */
/*           FL_RET_ERR_NOT_SUBSCRIBED: The AT command has not been          */
/*                                          subscribed.                      */
/*           FL_ERROR: Unsubscribe the AT command failed.                    */
/*           FL_OK: Unsubscribe the the AT command successfully.             */
/*---------------------------------------------------------------------------*/
/*Variable Name   |IN |OUT|GLB|  Utilisation                                 */
/*----------------+---+---+---+----------------------------------------------*/
/*pString         | X |   |   |  The AT command to be unsubscribed           */
/*----------------+---+---+---+----------------------------------------------*/
/*****************************************************************************/
extern s32 ebdat9_20UnsubscribeATCommand(ascii *pString);
extern s32 (*const eat_UnsubscribeATCommand)(ascii *pString);

/*****************************************************************************/
/*Function   : ebdat9_24MainUartPortIsTransmitterEmpty                       */
/*---------------------------------------------------------------------------*/
/*Object     :  Gets the transmit buffer's status of the serial port.        */
/*                                                                           */
/*Return     :    TRUE(1)   the transmit buffer is null.But note that the    */
/*                          last byte is still being sent on the TX pin. You */
/*                          must wait the last byte sent for a fixed time.   */
/*                FALSE(0)  there are data in the transmit buffer            */
/*                                                                           */
/*---------------------------------------------------------------------------*/
/*****************************************************************************/
extern bool ebdat9_24MainUartPortIsTransmitterEmpty(void);
extern bool (*const eat_MainUartPortIsTransmitterEmpty)(void);

#endif
/*END OF FILE*/

