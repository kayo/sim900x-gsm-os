/***************************************************************************
 *
 *            SIMCOM Application Foreground Layer
 *
 *              Copyright (c) 2011 SIMCOM Ltd.
 *
 ***************************************************************************
 *
 *   $Id: fl_audio.h#2 $
 *   $Revision: #2 $
 *   $DateTime: 2011/12/30 09:00:00 $
 *
 ***************************************************************************
 *
 *  Designed by    :   jay
 *  Coded by       :   jay
 *  Tested by      :
 *
 ***************************************************************************
 *
 * File Description
 * ----------------
 *   Audio related functions and definitions
 *
 *
 ***************************************************************************
 *
 ***************************************************************************/

#ifndef FL_AUDIO_H
#define FL_AUDIO_H
/****************************************************************************
 * Nested Include Files
 ***************************************************************************/
#include <fl_typ.h>
/****************************************************************************
 * Type Definitions
 ***************************************************************************/
typedef enum FlAudioNameTag
{
  //Continous
  FL_MELODY01 = 0,
  FL_MELODY02,
  FL_MELODY03,
  FL_MELODY04,
  FL_MELODY05,
  FL_MELODY06,
  FL_MELODY07,
  FL_MELODY08,
  FL_MELODY09,
  FL_MELODY10,
  FL_MELODY11,
  FL_MELODY12,
  FL_MELODY13,
  FL_MELODY14,
  FL_MELODY15,
  FL_MELODY16,
  FL_MELODY17,
  FL_MELODY18,
  FL_MELODY19,    
  FL_MELODY20,  
  FL_CALL_WAITING,
  FL_RINGING_TONE,
  FL_DIAL_TONE,
  //Single  
  FL_SUBSCRIBER_BUSY_TONE,
  FL_CONGESTION,
  FL_RADIO_PATH_NOT_AVAILABLE,
  FL_RADIO_PATH_ACKNOWLEDGED,
  FL_NUMBER_UNOBTAINABLE,
  FL_POSITIVE_SOUND_KISS,
  FL_NEGATIVE_SOUND_KISS,
  FL_ERROR_BEEP_KISS,
  FL_SWITCH_ON,
  FL_SWITCH_OFF,
  FL_BUMPER_SOUND,
  FL_KEY_TONE,
  FL_NEW_OCCURENCE_SOUND,
  FL_ALARM_SOUND,
  FL_AUTOREDIALSTART,
  FL_AUTOREDIALSUCCES,
  FL_GAME_INTRO,
  FL_GAME_NEW_LEVEL,
  FL_GAME_NEW_HIGH_SCORE,
  FL_GAME_LOSE_LIFE,
  FL_GAME_OVER, 
  FL_AUDIO_INVALID
}FlAudioName;


/****************************************************************************
 *  Function Prototypes
 ***************************************************************************/

/* Audio Interface */

/****************************************************************************/
/*  Function   : ebdat10_01PlayContinousAudio                               */
/*--------------------------------------------------------------------------*/
/*  Object     : play a continous audio                                     */
/*                                                                          */
/*  Return     : 1 on success                                               */
/*               0 if any error occur                                       */
/*                                                                          */
/*--------------------------------------------------------------------------*/
/*  Variable Name  |IN |OUT|GLB|  Utilisation                               */
/*-----------------+---+---+---+--------------------------------------------*/
/*  name           | X |   |   |  the audio's ID ,from FL_MELODY01 to       */
/*                                FL_DIAL_TONE                              */
/*-----------------+---+---+---+--------------------------------------------*/
/****************************************************************************/
extern bool ebdat10_01PlayContinousAudio(FlAudioName name);
extern bool (*const eat_PlayContinousAudio)(FlAudioName name);

/****************************************************************************/
/*  Function   : ebdat10_02StopContinousAudio                               */
/*--------------------------------------------------------------------------*/
/*  Object     : Stop a continous audio                                     */
/*                                                                          */
/*  Return     : 1 on success                                               */
/*                                                                          */
/*--------------------------------------------------------------------------*/
/*  Variable Name     |IN |OUT|GLB|  Utilisation                            */
/*--------------------+---+---+---+-----------------------------------------*/
/****************************************************************************/
extern bool ebdat10_02StopContinousAudio(void);
extern bool (*const eat_StopContinousAudio)(void);

/****************************************************************************/
/*  Function   : ebdat10_03PlaySingleAudio                                  */
/*--------------------------------------------------------------------------*/
/*  Object     : play a single audio                                        */
/*                                                                          */
/*  Return     : 1 on success                                               */
/*               0 if any error occur                                       */
/*                                                                          */
/*--------------------------------------------------------------------------*/
/*  Variable Name   |IN |OUT|GLB|  Utilisation                              */
/*------------------+---+---+---+-------------------------------------------*/
/*  name            | X |   |   |  the audio's ID ,from                     */
/*                                 FL_SUBSCRIBER_BUSY_TONE to FL_GAME_OVER  */
/*------------------+---+---+---+-------------------------------------------*/
/****************************************************************************/
extern bool ebdat10_03PlaySingleAudio(FlAudioName name);
extern bool (*const eat_PlaySingleAudio)(FlAudioName name);

/****************************************************************************/
/*  Function   : ebdat10_04PlaySingleAudioFromFile                          */
/*--------------------------------------------------------------------------*/
/*  Object     : play an audio file                                         */
/*                                                                          */
/*  Return     : 1 on success                                               */
/*               0 if any error occur                                       */
/*                                                                          */
/*--------------------------------------------------------------------------*/
/*  Variable Name   |IN |OUT|GLB|  Utilisation                              */
/*------------------+---+---+---+-------------------------------------------*/
/*  fileName        | X |   |   |   the audio file to be played for local            */
/*------------------+---+---+---+-------------------------------------------*/
/****************************************************************************/
extern bool ebdat10_04PlaySingleAudioFromFile(u8* fileName);
extern bool (*const eat_PlaySingleAudioFromFile)(u8* fileName);

/***************************************************************************
*  Function   : ebdat10_06DTMFDetectEnable
*-------------------------------------------------------------------------
*  Object     : enable DTMF detection
*                                                                         
*  Return     : FL_OK on success
*               FL_ERROR if any error occur
*                                                                         
*-------------------------------------------------------------------------
*  Variable Name     |IN |OUT|GLB|  Utilisation                           
*--------------------+---+---+---+----------------------------------------
*  isEnable              | x |   |   |  the audio's ID ,from FL_SUBSCRIBER_BUSY_TONE to FL_GAME_OVER
*                          TRUE: enable
*                          FALSE: disable
*--------------------+---+---+---+----------------------------------------
***************************************************************************/
extern s32 ebdat10_06DTMFDetectEnable(bool isEnable);
extern s32 (*const eat_DTMFDetectEnable)(bool isEnable);

/****************************************************************************/
/*  Function   : ebdat10_04PlaySingleAudioFromFile                          */
/*--------------------------------------------------------------------------*/
/*  Object     : play an audio file                                         */
/*                                                                          */
/*  Return     : 1 on success                                               */
/*               0 if any error occur                                       */
/*                                                                          */
/*--------------------------------------------------------------------------*/
/*  Variable Name   |IN |OUT|GLB|  Utilisation                              */
/*------------------+---+---+---+-------------------------------------------*/
/*  fileName        | X |   |   |   the audio file to be played for remote            */
/*------------------+---+---+---+-------------------------------------------*/
/****************************************************************************/
extern bool ebdat10_07PlayRemoteAmrFromFile(u8* fileName);
extern bool (*const eat_PlayRemoteAmrFromFile)(u8* fileName);

#endif

/*END OF FILE*/
