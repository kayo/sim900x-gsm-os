/***************************************************************************
 *
 *            SIMCOM Application Foreground Layer
 *
 *              Copyright (c) 2011 SIMCOM Ltd.
 *
 ***************************************************************************
 *
 *   $Id: fl_interface.h#2 $
 *   $Revision: #2 $
 *   $DateTime: 2011/12/30 09:00:00 $
 *
 ***************************************************************************
 *
 *  Designed by    :   MXN
 *  Coded by       :   MXN
 *  Tested by      :
 *
 ***************************************************************************
 *
 * File Description
 * ----------------
 *   other interface functions
 *
 *
 ***************************************************************************
 *
 ***************************************************************************/
#if !defined(FL_INTERFACE_H)
#define FL_INTERFACE_H

/****************************************************************************
 * Nested Include Files
 ***************************************************************************/
#include <fl_typ.h>
#include <fl_appinit.h>

/****************************************************************************
 * Type Definitions
 ***************************************************************************/
typedef struct BATTERYSTATUSTag
{
    u16 adc;  
}BATTERYSTATUS;

/****************************************************************************
 *  Function Prototypes
 ***************************************************************************/

/* Memory Interface */
/****************************************************************************/
/*  Function   : ebdat4_01GetMemory                                         */
/*--------------------------------------------------------------------------*/
/*  Object     : allocate the memeory                                       */
/*                                                                          */
/*  Return     : the allocated memory's address                             */
/*                                                                          */
/*--------------------------------------------------------------------------*/
/*  Variable Name     |IN |OUT|GLB|  Utilisation                            */
/*--------------------+---+---+---+-----------------------------------------*/
/*  Size              | x |   |   |  the memory size to allocate            */
/*--------------------+---+---+---+-----------------------------------------*/
/****************************************************************************/
extern void *ebdat4_01GetMemory(u16 Size);
extern void *(*const eat_GetMemory)(u16 Size);


/****************************************************************************/
/*  Function   : ebdat4_02FreeMemory                                        */
/*--------------------------------------------------------------------------*/
/*  Object     : free the allocated memeory                                 */
/*                                                                          */
/*  Return     : TRUE if free successfully                                  */
/*                                                                          */
/*--------------------------------------------------------------------------*/
/*  Variable Name     |IN |OUT|GLB|  Utilisation                            */
/*--------------------+---+---+---+-----------------------------------------*/
/*  Ptr               | x |   |   |  the address of allocated memory        */
/*--------------------+---+---+---+-----------------------------------------*/
/****************************************************************************/
extern bool ebdat4_02FreeMemory(void *Ptr);
extern bool (*const eat_FreeMemory)(void *Ptr);


/* Get Message Interface */
/****************************************************************************/
/*  Function   : eat1_02GetEvent                                            */
/*--------------------------------------------------------------------------*/
/*  Object     : get the message from message queue                         */
/*                                                                          */
/*  Return     : None                                                       */
/*                                                                          */
/*--------------------------------------------------------------------------*/
/*  Variable Name     |IN |OUT|GLB|  Utilisation                            */
/*--------------------+---+---+---+-----------------------------------------*/
/*  event_p           |   | X |   |  The message in message queue           */
/*--------------------+---+---+---+-----------------------------------------*/
/****************************************************************************/
extern void eat1_02GetEvent(FlEventBuffer	*event_p);
extern void (*const eat_GetEvent)(FlEventBuffer	*event_p);


/****************************************************************************/
/*  Function   : ebdat4_03Reset                                             */
/*--------------------------------------------------------------------------*/
/*  Object     : reset the system                                           */
/*                                                                          */
/*  Return     : None                                                       */
/*                                                                          */
/*--------------------------------------------------------------------------*/
/*  Variable Name     |IN |OUT|GLB|  Utilisation                            */
/*--------------------+---+---+---+-----------------------------------------*/
/****************************************************************************/
extern void ebdat4_03Reset(void);
extern void (*const eat_Reset)(void);

/****************************************************************************/
/*  Function   : ebdat4_04Wdtkick                                           */
/*--------------------------------------------------------------------------*/
/*  Object     : kick the watch dog                                         */
/*                                                                          */
/*  Return     : None                                                       */
/*                                                                          */
/*--------------------------------------------------------------------------*/
/*  Variable Name     |IN |OUT|GLB|  Utilisation                            */
/*--------------------+---+---+---+-----------------------------------------*/
/****************************************************************************/
extern void ebdat4_04Wdtkick(void);
extern void (*const eat_Wdtkick)(void);

/****************************************************************************/
/*  Function   : ebdat4_05PowerDown                                         */
/*--------------------------------------------------------------------------*/
/*  Object     : power down the module                                      */
/*                                                                          */
/*  Return     : None                                                       */
/*                                                                          */
/*--------------------------------------------------------------------------*/
/*  Variable Name     |IN |OUT|GLB|  Utilisation                            */
/*--------------------+---+---+---+-----------------------------------------*/
/****************************************************************************/
extern void ebdat4_05PowerDown(void);
extern void (*const eat_PowerDown)(void);

/****************************************************************************/
/*  Function   : ebdat6_17DisablePowerOffKey                                */
/*--------------------------------------------------------------------------*/
/*  Object     : Disable power key function                                 */
/*                                                                          */
/*  Return     : None                                                       */
/*                                                                          */
/*--------------------------------------------------------------------------*/
/*  Variable Name     |IN |OUT|GLB|  Utilisation                            */
/*--------------------+---+---+---+-----------------------------------------*/
/****************************************************************************/
extern void ebdat6_17DisablePowerOffKey(void);
extern void (*const eat_DisablePowerOffKey)(void);

/****************************************************************************/
/*  Function   : ebdat6_18EnablePowerOffKey                                 */
/*--------------------------------------------------------------------------*/
/*  Object     : Enable power key function                                  */
/*                                                                          */
/*  Return     : None                                                       */
/*                                                                          */
/*--------------------------------------------------------------------------*/
/*  Variable Name     |IN |OUT|GLB|  Utilisation                            */
/*--------------------+---+---+---+-----------------------------------------*/
/****************************************************************************/
extern void ebdat6_18EnablePowerOffKey(void);
extern void (*const eat_EnablePowerOffKey)(void);

/*update upper application*/
/****************************************************************************/
/*  Function   : eat1_09UpdateEmbeddedAp                                    */
/*--------------------------------------------------------------------------*/
/*  Object     : update the customer's application                          */
/*                                                                          */
/*  Return     : FL_OK if the parameter is correct.                         */
/*                                                                          */
/*--------------------------------------------------------------------------*/
/*  Variable Name     |IN |OUT|GLB|  Utilisation                            */
/*--------------------+---+---+---+-----------------------------------------*/
/*  startID           | X |   |   |  the start ID to store new application  */
/*  idCount           | X |   |   |  the count of flash id that the         */
/*                                   customer's application occupied        */
/*--------------------+---+---+---+-----------------------------------------*/
/*  osSize            | X |   |   |  the size of the customer's             */
/*                                   application                            */
/*--------------------+---+---+---+-----------------------------------------*/
/*  checksum          | X |   |   |  input the application checksum. if the */
/*                                   customer don't want to use it, please  */
/*                                   set it to NULL. The methed of calculating*/
/*                                   the checksum is add all the data of    */
/*                                   application to get an u32 value.       */
/****************************************************************************/
#ifdef UPDATE_EMBEDDED_AP_HAS_CHECKSUM
extern s32 eat1_09UpdateEmbeddedAp( u16 startID, u16 idCount, u32 osSize, u32 *checksum);
extern s32 (*const eat_UpdateEmbeddedAp)( u16 startID, u16 idCount, u32 osSize, u32 *checksum);
#else
extern s32 eat1_09UpdateEmbeddedAp( u16 startID, u16 idCount, u32 osSize);
extern s32 (*const eat_UpdateEmbeddedAp)( u16 startID, u16 idCount, u32 osSize);
#endif

/****************************************************************************/
/*  Function : ebdat4_15ExitOutOfSleepMode                                  */
/*--------------------------------------------------------------------------*/
/*  Object   : Get out of sleep mode                                        */
/*            Note that "AT+CSCLK=2" should be set first.                   */
/*                                                                          */
/*  Return   : FL_OK: Get out of sleep mode successfully                    */
/*            FL_ERROR: Get out of sleep mode failed                        */
/*--------------------------------------------------------------------------*/
/*  Variable Name     |IN |OUT|GLB|  Utilisation                            */
/*--------------------+---+---+---+-----------------------------------------*/
/****************************************************************************/
extern s32 ebdat4_15ExitOutOfSleepMode(void);
extern s32 (*const eat_ExitOutOfSleepMode)(void);

/****************************************************************************/
/*  Function   : ebdat4_17EnterSleepMode                                    */
/*--------------------------------------------------------------------------*/
/*  Object     : enter into sleep mode                                      */
/*              Note that "AT+CSCLK=2" should be set first.                 */
/*                                                                          */
/*  Return     : FL_OK: enter into sleep mode successfully                  */
/*              FL_ERROR: enter into sleep mode failed                      */
/*--------------------------------------------------------------------------*/
/*  Variable Name     |IN |OUT|GLB|  Utilisation                            */
/*--------------------+---+---+---+-----------------------------------------*/
/****************************************************************************/
extern s32 ebdat4_17EnterSleepMode(void);
extern s32 (*const eat_EnterSleepMode)(void);

/****************************************************************************/
/*  Function   : ebdat4_21SendEventMsg                                      */
/*--------------------------------------------------------------------------*/
/*  Object     : send message to app task or other ask                      */
/*                                                                          */
/*  Return     :                                                            */
/*                FL_OK: send message successfully                          */
/*                FL_RET_ERR_PARAM: Parameter incorrect.                    */
/*--------------------------------------------------------------------------*/
/*  Variable Name     |IN |OUT|GLB|  Utilisation                            */
/*--------------------+---+---+---+-----------------------------------------*/
/*  msg                   | X |      |      |  the message to be sent       */
/****************************************************************************/
extern s32 ebdat4_21SendEventMsg(MSG_EVT msg);
extern s32 (*const eat_SendEventMsg)(MSG_EVT msg);

/****************************************************************************/
/*  Function   : ebdat4_22GetADCValue                                       */
/*--------------------------------------------------------------------------*/
/*  Object     : Get the ADC value                                          */
/*                                                                          */
/*  Return     :                                                            */
/*                FL_OK: get the value successfully                         */
/*                FL_RET_ERR_PARAM: Parameter incorrect.                    */
/*--------------------------------------------------------------------------*/
/*  Variable Name     |IN |OUT|GLB|  Utilisation                            */
/*--------------------+---+---+---+-----------------------------------------*/
/*  voltage               |    |  X  |      |  the voltage on the ADC pin   */
/****************************************************************************/
extern s32 ebdat4_22GetADCValue(u16 *voltage);
extern s32 (*const eat_GetADCValue)(u16 *voltage);

/****************************************************************************/
/*  Function   : ebdat4_23GetBatteryVoltage                                 */
/*--------------------------------------------------------------------------*/
/*  Object     : Get the battery voltage                                    */
/*                                                                          */
/*  Return     :                                                            */
/*                FL_OK: get the battery voltage successfully               */
/*                FL_RET_ERR_PARAM: Parameter incorrect.                    */
/*--------------------------------------------------------------------------*/
/*  Variable Name     |IN |OUT|GLB|  Utilisation                            */
/*--------------------+---+---+---+-----------------------------------------*/
/*  voltage           |   | X |   |  the battery voltage and battery status */
/****************************************************************************/
extern s32 ebdat4_23GetBatteryVoltage(CBCValue *voltage);
extern s32 (*const eat_GetBatteryVoltage)(CBCValue *voltage);

/****************************************************************************/
/*  Function   : ebdat4_24GetModuleTemperature                              */
/*--------------------------------------------------------------------------*/
/*  Object     : Get the module tmeperature                                 */
/*                                                                          */
/*  Return     :                                                            */
/*                FL_OK: get the module tmeperature successfully            */
/*                FL_RET_ERR_PARAM: Parameter incorrect.                    */
/*--------------------------------------------------------------------------*/
/*  Variable Name     |IN |OUT|GLB|  Utilisation                            */
/*--------------------+---+---+---+-----------------------------------------*/
/*  tmp                    |    |  X  |      |  the module temperature      */
/****************************************************************************/
extern s32 ebdat4_24GetModuleTemperature(s32 *tmp);
extern s32 (*const eat_GetModuleTemperature)(s32 *tmp);

#ifdef HAS_MULTIPLE_SIM_CARD
#define EAT_SIM_CARD_ARG FlSIMCardIndex simCardIndex
#define ADD_EAT_SIM_CARD_ARG , EAT_SIM_CARD_ARG
#else
#define EAT_SIM_CARD_ARG void
#define ADD_EAT_SIM_CARD_ARG
#endif

/****************************************************************************/
/*  Function   : ebdat4_25GetRegistrationStatus                             */
/*--------------------------------------------------------------------------*/
/*  Object     : Get the module registration status. It is the same as      */
/*                  the second parameter "AT+CREG?"                         */
/*                                                                          */
/*  Return    : 0  Not registered, MT is not currently searching a new      */
/*                 operator to register to                                  */
/*              1  Registered, home network                                 */
/*              2  Not registered, but MT is currently searching a new      */
/*                 operator to register to                                  */
/*              3  Registration denied                                      */
/*              4  Unknown                                                  */
/*              5  Registered, roaming                                      */
/*--------------------------------------------------------------------------*/
/*  Variable Name     |IN |OUT|GLB|  Utilisation                            */
/*--------------------+---+---+---+-----------------------------------------*/
/****************************************************************************/
extern u8 ebdat4_25GetRegistrationStatus(EAT_SIM_CARD_ARG);
extern u8 (*const eat_GetRegistrationStatus)(EAT_SIM_CARD_ARG);

/****************************************************************************/
/*  Function   : ebdat4_26GetGPRSRegistrationStatus                         */
/*--------------------------------------------------------------------------*/
/*  Object     : Get the module GPRS registration status. It is the same    */
/*                  as the second parameter "AT+CGREG?"                     */
/*                                                                          */
/*  Return    : 0	Not registered, MT is not currently searching an          */
/*                      operator to register to.The GPRS service is         */
/*                       disabled, the UE is allowed to attach for GPRS     */
/*                      if requested by the user.                           */
/*                 1    Registered, home network.                           */
/*                 2    Not registered, but MT is currently trying to       */
/*                      attach or searching an operator to register to.     */
/*                       The GPRS service is enabled, but an allowable      */
/*                      PLMN is currently not available. The UE will start  */
/*                      a GPRS attach as soon as an allowable PLMN          */
/*                      is available.                                       */
/*                 3    Registration denied                                 */
/*                      The GPRS service is disabled, the UE is not         */
/*                      allowed to attach for GPRS if it is requested       */
/*                      by the user.                                        */
/*                 4     Unknown                                            */
/*                 5     Registered, roaming                                */
/*--------------------------------------------------------------------------*/
/*  Variable Name     |IN |OUT|GLB|  Utilisation                            */
/*--------------------+---+---+---+-----------------------------------------*/
/****************************************************************************/
extern u8 ebdat4_26GetGPRSRegistrationStatus(EAT_SIM_CARD_ARG);
extern u8 (*const eat_GetGPRSRegistrationStatus)(EAT_SIM_CARD_ARG);

/****************************************************************************/
/*  Function   : ebdat4_27GetGPRSAttachStatus                               */
/*--------------------------------------------------------------------------*/
/*  Object     : Get the GPRS attach status                                 */
/*                                                                          */
/*  Return     :   0  The GPRS is detached.                                 */
/*                    1  The GPRS is attached.                              */
/*--------------------------------------------------------------------------*/
/*  Variable Name     |IN |OUT|GLB|  Utilisation                            */
/*--------------------+---+---+---+-----------------------------------------*/
/****************************************************************************/
extern u8 ebdat4_27GetGPRSAttachStatus(EAT_SIM_CARD_ARG);
extern u8 (*const eat_GetGPRSAttachStatus)(EAT_SIM_CARD_ARG);

/****************************************************************************/
/*  Function   : ebdat4_28GetCSQValue                                       */
/*--------------------------------------------------------------------------*/
/*  Object     : Get the module rx level                                    */
/*                                                                          */
/*  Return     :                                                            */
/*                FL_OK: get the module rx level successfully               */
/*                FL_RET_ERR_PARAM: Parameter incorrect.                    */
/*--------------------------------------------------------------------------*/
/*  Variable Name     |IN |OUT|GLB|  Utilisation                            */
/*--------------------+---+---+---+-----------------------------------------*/
/*  rssi              |   | X |   |                                         */
/*              0               115 dBm or less                             */
/*              1               111 dBm                                     */
/*              2...30         110...  54 dBm                               */
/*              31              52 dBm or greater                           */
/*              99              not known or not detectable                 */
/*--------------------+---+---+---+-----------------------------------------*/
/*  ber               |   | X |   |                                         */
/*              0...7        As RXQUAL values in the table                  */
/*                           in GSM 05.08 [20] subclause 7.2.4              */
/*              99            Not known or not detectable                   */
/****************************************************************************/
extern s32 ebdat4_28GetCSQValue(u8 *rssi, u8 *ber ADD_EAT_SIM_CARD_ARG);
extern s32 (*const eat_GetCSQValue)(u8 *rssi, u8 *ber ADD_EAT_SIM_CARD_ARG);

/****************************************************************************/
/*  Function   : ebdat4_30GetNeighborCellInformation                        */
/*--------------------------------------------------------------------------*/
/*  Object     : Get the neighbor cell information                          */
/*                                                                          */
/*  Return     :   the neighbor cell information                            */
/*--------------------------------------------------------------------------*/
/*  Variable Name     |IN |OUT|GLB|  Utilisation                            */
/*--------------------+---+---+---+-----------------------------------------*/
/****************************************************************************/
extern NeighborCellInfo ebdat4_30GetNeighborCellInformation(EAT_SIM_CARD_ARG);
extern NeighborCellInfo (*const eat_GetNeighborCellInformation)(EAT_SIM_CARD_ARG);

/****************************************************************************/
/*  Function   : ebdat4_29GetServiceCellInformation                         */
/*--------------------------------------------------------------------------*/
/*  Object     : Get the Service cell information                           */
/*                                                                          */
/*  Return     :   the neighbor cell information                            */
/*--------------------------------------------------------------------------*/
/*  Variable Name     |IN |OUT|GLB|  Utilisation                            */
/*--------------------+---+---+---+-----------------------------------------*/
/****************************************************************************/
extern ServiceCellInformation ebdat4_29GetServiceCellInformation(EAT_SIM_CARD_ARG);
extern ServiceCellInformation (*const eat_GetServiceCellInformation)(EAT_SIM_CARD_ARG);

/****************************************************************************/
/*  Function   : ebdat4_31GetIMEI                                           */
/*--------------------------------------------------------------------------*/
/*  Object     : Get the IMEI of the module                                 */
/*                                                                          */
/*  Return     :   the IMEI of the module                                   */
/*--------------------------------------------------------------------------*/
/*  Variable Name     |IN |OUT|GLB|  Utilisation                            */
/*--------------------+---+---+---+-----------------------------------------*/
/****************************************************************************/
extern IMEIValue ebdat4_31GetIMEI(EAT_SIM_CARD_ARG);
extern IMEIValue (*const eat_GetIMEI)(EAT_SIM_CARD_ARG);

/****************************************************************************/
/*  Function   : ebdat4_32GetCfunValue                                      */
/*--------------------------------------------------------------------------*/
/*  Object     : Get the CFUN value                                         */
/*                                                                          */
/*  Return     :   the CFUN value, it is the same as "AT+CFUN?"             */
/*--------------------------------------------------------------------------*/
/*  Variable Name     |IN |OUT|GLB|  Utilisation                            */
/*--------------------+---+---+---+-----------------------------------------*/
/****************************************************************************/
extern u8 ebdat4_32GetCfunValue(void);
extern u8 (*const eat_GetCfunValue)(void);

/****************************************************************************/
/*  Function   : ebdat4_33GetModuleCpinStatus                               */
/*--------------------------------------------------------------------------*/
/*  Object     : Get the CPIN value. it is the same as AT+CPIN?             */
/*                                                                          */
/*  Return     :   FL_RET_ERR_PARAM: parameter incorrect.                   */
/*                    FL_OK: Get the CPIN value                             */
/*--------------------------------------------------------------------------*/
/*  Variable Name     |IN |OUT|GLB|  Utilisation                            */
/*----------------+---+---+---+---------------------------------------------*/
/*  value             |   | X |   |                                         */
/*                    EAT_READY,                                            */
/*                    EAT_SIM_PIN,                                          */
/*                    EAT_SIM_PUK,                                          */
/*                    EAT_SIM_PIN2,                                         */
/*                    EAT_SIM_PUK2,                                         */
/*                    EAT_SIM_NCK,                                          */
/*                    EAT_SIM_NSCK,                                         */
/*                    EAT_SIM_SPCK,                                         */
/*                    EAT_SIM_CCK,                                          */
/*                    EAT_SIM_NO_PRESENT,                                   */
/****************************************************************************/
extern s32 ebdat4_33GetModuleCpinStatus(u8 *value ADD_EAT_SIM_CARD_ARG);
extern s32 (*const eat_GetModuleCpinStatus)(u8 *value ADD_EAT_SIM_CARD_ARG);

/****************************************************************************/
/*  Function   : ebdat4_34GetCurrentTaskID                                  */
/*--------------------------------------------------------------------------*/
/*  Object     : Get the current task ID                                    */
/*                                                                          */
/*  Return     :   FL_EAT_TASK,                                             */
/*                    FL_MULTI_TASK_1,                                      */
/*                    FL_MULTI_TASK_2,                                      */
/*                    FL_MULTI_TASK_3,                                      */
/*                    FL_MULTI_TASK_4,                                      */
/*                    FL_MULTI_TASK_5,                                      */
/*--------------------------------------------------------------------------*/
/*  Variable Name     |IN |OUT|GLB|  Utilisation                            */
/*--------------------+---+---+---+-----------------------------------------*/
/****************************************************************************/
extern u8 ebdat4_34GetCurrentTaskID(void);
extern u8 (*const eat_GetCurrentTaskID)(void);

/****************************************************************************/
/*  Function   : ebdat4_35GetSIMCardIMSI                                    */
/*--------------------------------------------------------------------------*/
/*  Object     : Get the imsi of the SIM card. it is the same as AT+CIMI    */
/*                                                                          */
/*  Return     :   FL_RET_ERR_PARAM: parameter incorrect.                   */
/*                 FL_RET_ERR_SIM_NOT_INSERT: SIM card is not inserted.     */
/*                 FL_RET_ERR_SIM_NOT_READY:  SIM card is busy,             */
/*                                            please read it later.         */
/*                 FL_OK: Get the IMSI value                                */
/*--------------------------------------------------------------------------*/
/*  Variable Name     |IN |OUT|GLB|  Utilisation                            */
/*----------------+---+---+---+---------------------------------------------*/
/*imsi                |   | X |   |  return IMSI of the SIM card            */
/****************************************************************************/
extern s32 ebdat4_35GetSIMCardIMSI(IMSIValue *imsi ADD_EAT_SIM_CARD_ARG);
extern s32 (*const eat_GetSIMCardIMSI)(IMSIValue *imsi ADD_EAT_SIM_CARD_ARG);

/****************************************************************************/
/*  Function   : ebdat4_36GetSIMCardICCID                                   */
/*--------------------------------------------------------------------------*/
/*  Object     : Get the iccid of the SIM card. it is the same as AT+CCID   */
/*                                                                          */
/*  Return     :   FL_RET_ERR_PARAM: parameter incorrect.                   */
/*                    FL_RET_ERR_SIM_NOT_INSERT: SIM card is not inserted.  */
/*                    FL_ERROR: read ICCID failed.                          */
/*                    FL_OK: Get the ICCID value                            */
/*--------------------------------------------------------------------------*/
/*  Variable Name     |IN |OUT|GLB|  Utilisation                            */
/*----------------+---+---+---+---------------------------------------------*/
/*iccid               |   | X |   |  return ICCID of the SIM card           */
/****************************************************************************/
extern s32 ebdat4_36GetSIMCardICCID(ICCIDValue *iccid ADD_EAT_SIM_CARD_ARG);
extern s32 (*const eat_GetSIMCardICCID)(ICCIDValue *iccid ADD_EAT_SIM_CARD_ARG);

/****************************************************************************/
/*  Function   : ebdat4_37GetSIMCardSPN                                     */
/*--------------------------------------------------------------------------*/
/*  Object     : Get the service provider name of the SIM card. it is       */
/*               the same as AT+CSPN                                        */
/*                                                                          */
/*  Return     :   FL_RET_ERR_PARAM: parameter incorrect.                   */
/*                 FL_RET_ERR_SIM_NOT_INSERT: SIM card is not inserted.     */
/*                 FL_OK: Get the spn value                                 */
/*--------------------------------------------------------------------------*/
/*  Variable Name     |IN |OUT|GLB|  Utilisation                            */
/*----------------+---+---+---+---------------------------------------------*/
/*spn.spnName         |   | X |   |  return service provider name of        */
/*                                   the SIM card                           */
/*spn.mode            |   | X |   | 0 -- don't diplay PLMN. Already         */
/*                                  registered on PLMN                      */
/*                                   1 -- display PLMN                      */
/****************************************************************************/
extern s32 ebdat4_37GetSIMCardSPN(SPNValue *spn ADD_EAT_SIM_CARD_ARG);
extern s32 (*const eat_GetSIMCardSPN)(SPNValue *spn ADD_EAT_SIM_CARD_ARG);

/****************************************************************************/
/*  Function   : ebdat4_38SetSMSIndEvent                                    */
/*--------------------------------------------------------------------------*/
/*  Object     : If it is enabled, when the module received a SMS, an       */
/*                  EVENT_SMS_IND will be triggered.                        */
/*                                                                          */
/*  Return     :   NULL                                                     */
/*--------------------------------------------------------------------------*/
/*  Variable Name     |IN |OUT|GLB|  Utilisation                            */
/*----------------+---+---+---+---------------------------------------------*/
/*enSmsInd              |    |  X  |      |  enable the EVENT_SMS_IND       */
/****************************************************************************/
extern void ebdat4_38SetSMSIndEvent(bool enSmsInd);
extern void (*const eat_SetSMSIndEvent)(bool enSmsInd);

/****************************************************************************/
/*  Function   : ebdat4_39SetCregIndEvent                                   */
/*--------------------------------------------------------------------------*/
/*  Object     : If it is enabled, when the network registration status     */
/*                  is changed, an EVENT_CREG_IND will be triggered.        */
/*                                                                          */
/*  Return     :   NULL                                                     */
/*--------------------------------------------------------------------------*/
/*  Variable Name     |IN |OUT|GLB|  Utilisation                            */
/*----------------+---+---+---+---------------------------------------------*/
/*enCreg                 |    |  X  |      |  enable the EVENT_CREG_IND     */
/****************************************************************************/
extern void ebdat4_39SetCregIndEvent(bool enCreg);
extern void (*const eat_SetCregIndEvent)(bool enCreg);

/****************************************************************************/
/*  Function   : ebdat4_39SetCregIndEvent                                   */
/*--------------------------------------------------------------------------*/
/*  Object     : If it is enabled, when the GPRS registration status is     */
/*                   changed, an EVENT_CGREG_IND will be triggered.         */
/*                                                                          */
/*  Return     :   NULL                                                     */
/*--------------------------------------------------------------------------*/
/*  Variable Name     |IN |OUT|GLB|  Utilisation                            */
/*----------------+---+---+---+---------------------------------------------*/
/*enCgreg                 |    |  X  |      |  enable the EVENT_CGREG_IND   */
/****************************************************************************/
extern void ebdat4_40SetCgregIndEvent(bool enCgreg);
extern void (*const eat_SetCgregIndEvent)(bool enCgreg);

/****************************************************************************/
/*  Function   : ebdat4_55ReadIMSIMnc                                       */
/*--------------------------------------------------------------------------*/
/*  Object     : Get the mnc of the SIM card's IMSI                         */
/*                                                                          */
/*  Return     : FL_RET_ERR_PARAM: parameter incorrect.                     */
/*               FL_RET_ERR_SIM_NOT_READY: SIM card is not inserted or cpin */
/*               FL_OK: Get the mnc value                                   */
/*--------------------------------------------------------------------------*/
/*  Variable Name     |IN |OUT|GLB|  Utilisation                            */
/*----------------+---+---+---+---------------------------------------------*/
/*  mnc               |   | X |   |  Get the string of mnc                  */
/****************************************************************************/
extern s32 ebdat4_55ReadIMSIMnc(ascii *mnc ADD_EAT_SIM_CARD_ARG);
extern s32 (*const eat_ReadIMSIMnc)(ascii *mnc ADD_EAT_SIM_CARD_ARG);

/****************************************************************************/
/*  Function   : ebdat4_56ReadIMSIMcc                                       */
/*--------------------------------------------------------------------------*/
/*  Object     : Get the mcc of the SIM card's IMSI                         */
/*                                                                          */
/*  Return     : FL_RET_ERR_PARAM: parameter incorrect.                     */
/*               FL_RET_ERR_SIM_NOT_READY: SIM card is not inserted or cpin */
/*               FL_OK: Get the mcc value                                   */
/*--------------------------------------------------------------------------*/
/*  Variable Name     |IN |OUT|GLB|  Utilisation                            */
/*----------------+---+---+---+---------------------------------------------*/
/*  mcc               |   | X |   |  Get the string of mcc                  */
/****************************************************************************/
extern s32 ebdat4_56ReadIMSIMcc(ascii *mcc ADD_EAT_SIM_CARD_ARG);
extern s32 (*const eat_ReadIMSIMcc)(ascii *mcc ADD_EAT_SIM_CARD_ARG);

#endif
/* END OF FILE */
