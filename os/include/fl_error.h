/***************************************************************************
 *
 *            SIMCOM Application Foreground Layer
 *
 *              Copyright (c) 2011 SIMCOM Ltd.
 *
 ***************************************************************************
 *
 *   $Id:fl_error.h#1 $
 *   $Revision: #1 $
 *   $DateTime: 2011/12/30 09:00:00 $
 *
 ***************************************************************************
 *
 *  Designed by    :   Samuel
 *  Coded by       :   Samuel
 *  Tested by      :
 *
 ***************************************************************************
 *
 * File Description
 * ----------------
 *   Error functions and definitions  
 *
 *
 ***************************************************************************
 *
 ***************************************************************************/
#if !defined (FL_ERROR_H)
#define       FL_ERROR_H

/****************************************************************************
 *  Macros
 ***************************************************************************/
#define FL_OK   0
#define FL_ERROR -1

/* Error return codes */
#define FL_RET_ERR_PARAM                  -2
#define FL_RET_ERR_UNKNOWN_HDL            -3
#define FL_RET_ERR_ALREADY_SUBSCRIBED     -4
#define FL_RET_ERR_NOT_SUBSCRIBED         -5
#define FL_RET_ERR_FATAL                  -6
#define FL_RET_ERR_BAD_HDL                -7
#define FL_RET_ERR_BAD_STATE              -8
#define FL_RET_ERR_PIN_KO                 -9
#define FL_RET_ERR_NO_MORE_HANDLES        -10
#define FL_RET_ERR_OVERSIZE               -11
#define FL_RET_ERR_UNMATCH                -12
#define FL_RET_ERR_SIM_NOT_READY		-13
#define FL_RET_ERR_SIM_NOT_INSERT		-14
#define FL_RET_ERR_SPECIFIC_BASE          -20


/****************************************************************************
 * Type Definitions
 ***************************************************************************/

#endif /* FL_ERROR_H */
/* END OF FILE */
