/***************************************************************************
 *
 *            SIMCOM Application Foreground Layer
 *
 *              Copyright (c) 2011 SIMCOM Ltd.
 *
 ***************************************************************************
 *
 *   $Id: fl_appint.h#2 $
 *   $Revision: #2 $
 *   $DateTime: 2011/12/30 09:00:00 $
 *
 ***************************************************************************
 *
 *  Designed by    :   MXN
 *  Coded by       :   MXN
 *  Tested by      :
 *
 ***************************************************************************
 *
 * File Description
 * ----------------
 *   Data structure and  functions for embedded application
 *
 *
 ***************************************************************************
 *
 ***************************************************************************/
#if !defined(FL_APPINIT_H)
#define FL_APPINIT_H
#define EVENT_MAX_DATA 1500
/****************************************************************************
 * Type Definitions
 ***************************************************************************/
#define	EBDAT_MAX_RECVBUF		1460
/*event type*/
typedef enum FlEventTypeTag           
{
    EVENT_NULL = 0,
    EVENT_INTR,
#ifdef HAS_KBD_PERIPHERY
    EVENT_KEY,
#endif
    EVENT_UARTDATA,
    EVENT_MODEMDATA,
    EVENT_TIMER,
    EVENT_SERAILSTATUS,
    EVENT_SOCKET,
		EVENT_DTMF,
    EVENT_UART_READY,
#ifdef HAS_EVENT_APDU
    EVENT_MODEM_APDU,
    EVENT_SIMCARD_APDU,
#endif
    EVENT_FLASH_READY,
    EVENT_MSG,
    EVENT_SMS_IND,
    EVENT_CREG_IND,
    EVENT_CGREG_IND,
    EVENT_MAX = 0xFF
}FlEventType;


typedef struct TIMER_EVTTag          
{
    u16  timer_id;    
    u32  interval;   
}TIMER_EVT;

/*output data's type*/
typedef enum UARTDATA_TYPETAG
{
    DATA_SERIAL = 0,
    DATA_DEBUG,
    UARTDATA_MAX
}FlUartDataType;

typedef enum MODEMDATA_TYPETAG
{
    MODEM_CMD = 0,
    MODEM_DATA,
    MODEM_CRWP,
    MODEM_OTHER_AT,
    MODEMDATA_MAX
}FlModemDataType;

typedef enum SERIAL_BITTAG
{
	RI=0,
	DCD,
	DSR,
	DTR,
	CTS,
	RTS
}FlSerialBit;

typedef enum FlMainUartFormatTag
{
	FL_MAIN_UART_8N2_FORMAT = 1, /*8 data 0 parity 2 stop*/
	FL_MAIN_UART_8P1_FORMAT = 2, /*8 data 1 parity 1 stop*/
	FL_MAIN_UART_8N1_FORMAT = 3, /*8 data 0 parity 1 stop*/
	FL_MAIN_UART_7N2_FORMAT = 4, /*7 data 0 parity 2 stop*/
	FL_MAIN_UART_7P1_FORMAT = 5, /*7 data 1 parity 1 stop*/
	FL_MAIN_UART_7N1_FORMAT = 6  /*7 data 0 parity 1 stop*/
}FlMainUartFormat;

typedef enum FlMainUartParityTag
{
	FL_MAIN_UART_ODD = 0,      /*odd parity*/
	FL_MAIN_UART_EVEN = 1,     /*even parity*/
	FL_MAIN_UART_SPACE = 3     /*space parity*/
}FlMainUartParity;

typedef enum FlMainUartFlowControlTag
{
	FL_MAIN_UART_NO_FLOW_CONTROL,
	FL_MAIN_UART_SOFTWARE_FLOW_CONTROL,
	FL_MAIN_UART_HARDWARE_FLOW_CONTROL
}FlMainUartFlowControl;

typedef enum FlSocketEventTypeTag
{
    FL_SOCKET_CONNECT,
    FL_SOCKET_SEND,
    FL_SOCKET_RECV,
    FL_SOCKET_CLOSE,
    FL_SOCKET_REMOTE_CLOSE,
    FL_SOCKET_TCP_SERVER_START,
    FL_SOCKET_TCP_SERVER_CONNECT,
    FL_SOCKET_TCP_SERVER_STOP,
    FL_SOCKET_GPRS_ACTIVE,
    FL_SOCKET_GPRS_DEACTIVE,
    FL_SOCKET_MAX
}FlSocketEventType;

#ifdef HAS_EVENT_APDU
typedef enum FlModemAPDUTypeTag
{
    FL_MOD_APDU_RESET,
    FL_MOD_APDU_DISCONNECT,
    FL_MOD_APDU_REQ_DATA,
    FL_MOD_APDU_SEND_DATA,
    FL_MOD_APDU_MAX
}FlModemAPDUType;
typedef enum FlSIMCardAPDUTypeTag
{
    FL_SIM_APDU_INTERFACE_ERROR,
    FL_SIM_APDU_CMD_ERROR,
    FL_SIM_APDU_DATA,
    FL_SIM_ADPU_RESET_CNF,
    FL_SIM_APDU_MAX
}FlSIMCardAPDUType;
#endif

#ifdef HAS_MULTIPLE_SIM_CARD
typedef enum FlSIMCardIndexTag
    FL_SIM_CARD_1,
    FL_SIM_CARD_2,
    FL_INVALID_SIM_CARD_INDEX
}FlSIMCardIndex;
#endif

typedef struct SERIALSTATUS_EVTTag
{
	
	u8 currentVal;
	FlSerialBit sbit;
}SERIALSTATUS_EVT;

typedef struct KEY_EVTTag          
{
    u16       key_val;      
    bool   isPressed;   
}KEY_EVT;

typedef struct INTR_EVTTag          
{
    u16       pinName;      
   /*0 falling and 1 rising*/
    bool      gpioState;
}INTR_EVT;

typedef struct ModemData_EVTTag
{
    u32               length;
    u8               data[EVENT_MAX_DATA];
    FlModemDataType               type;   
	u32				 atCommandIndex;
} MODEMDATA_EVT;

typedef struct UartData_EVTTag
{
    u32               length;
    u8               data[EVENT_MAX_DATA];
    FlUartDataType               type;   
}UARTDATA_EVT;


typedef enum FlSocketTypeTag
{
  EBDAT_TCP_CONNECT,
  EBDAT_UDP_CONNECT
}FlSocketType_e;

typedef struct SOCKETEVENT_EVTTag
{    
    FlSocketEventType type;
    u32			  socketId;
    u32         bsdResult;     
}SOCKETEVENT_EVT;

typedef enum FLMsgTaskIDTag
{
	FL_EAT_TASK,
	FL_MULTI_TASK_1,
	FL_MULTI_TASK_2,
	FL_MULTI_TASK_3,
	FL_MULTI_TASK_4,
	FL_MULTI_TASK_5,
	NUM_OF_FL_MULTI_TASK
}FLMsgTaskID;

typedef struct UARTREADYEVENT_EVTTag
{	 
	u32 active;   
}UARTREADYEVENT_EVT;

#ifdef HAS_EVENT_APDU
typedef struct MODEMAPDU_DATATag
{
	u8	v_Class;
	u8	v_Instruction;
	u8	v_P1;
	u8	v_P2;
	u8	v_P3;
	u8   v_unused1;
	u8   v_unused2;
	u8   v_unused3;
	u8	a_CData[256];
}MODEMAPDU_DATA;

typedef struct MODEMAPDU_EVTTag
{
	MODEMAPDU_DATA apduData;
	u8   v_errorValue;
	u8   v_resetType;
	FlModemAPDUType apduType;
}MODEMAPDU_EVT;

typedef struct SIMCARDAPDU_DATATag
{
	u16  v_len;
	u8	a_RData[258];
	u8   v_unused1;
	u8   v_unused2;
	u8   v_unused3;
	u8   v_unused4;
}SIMCARDAPDU_DATA;


typedef struct SIMCARDRESET_CNFTag
{
	u8	v_VoltageValue;
       u8	a_AnswerToReset[33];
	u8   unused1;
	u8   unused2;
}SIMCARDRESET_CNF;

typedef struct SIMCARDAPDU_EVTTag
{
	SIMCARDAPDU_DATA apduData;
	u8   v_errorValue;
	SIMCARDRESET_CNF   resetCnf;
	FlSIMCardAPDUType apduType;
}SIMCARDAPDU_EVT;
#endif

typedef struct MSG_EVTTag
{
	FLMsgTaskID source;
	FLMsgTaskID destination;
	u8 dataBuffer[2048];
}MSG_EVT;
typedef struct DTMF_EVENTTag
{
	ascii dtmfChar;
	u8 reserve[3];
}DTMF_EVENT;

typedef struct SMSIND_EVTTag
{
	u16 index;
#ifdef HAS_MULTIPLE_SIM_CARD
	FlSIMCardIndex simCardIndex;
#endif
}SMSIND_EVT;
typedef struct CREGIND_EVTTag
{
	u8 status;
#ifdef HAS_MULTIPLE_SIM_CARD
	FlSIMCardIndex simCardIndex;
#endif
}CREGIND_EVT;
typedef struct CGREGIND_EVTTag
{
	u8 status;
#ifdef HAS_MULTIPLE_SIM_CARD
	FlSIMCardIndex simCardIndex;
#endif
}CGREGIND_EVT;


typedef union EventDataTag
{
    TIMER_EVT       timer_evt;
#ifdef HAS_KBD_PERIPHERY
	  KEY_EVT         key_evt;
#endif
    UARTDATA_EVT  uartdata_evt;
    MODEMDATA_EVT  modemdata_evt;
    INTR_EVT        intr_evt;
    SERIALSTATUS_EVT serialstatus_evt;
    SOCKETEVENT_EVT	socket_evt;
	  DTMF_EVENT	dtmf_evt;
    UARTREADYEVENT_EVT uartReady_evt;
#ifdef HAS_EVENT_APDU
    MODEMAPDU_EVT modemAPDU_evt;
    SIMCARDAPDU_EVT simcardAPDU_evt;
#endif
	  MSG_EVT 	msg_evt;
    SMSIND_EVT     smsInd_evt;
    CREGIND_EVT   cregInd_evt;
    CGREGIND_EVT cgregInd_evt;
}EventData;

typedef struct FlSignalBufferTag
{
    FlEventType  eventTyp;
    EventData    eventData;
}
FlEventBuffer;

enum
{
	EMBEDDED_AT_TASK_ID,
	MULTI_TASK_PRIO1_ID,
	MULTI_TASK_PRIO2_ID,
	MULTI_TASK_PRIO3_ID,
	MULTI_TASK_PRIO4_ID,
	MULTI_TASK_PRIO5_ID
};

enum
{
  EAT_READY,
  EAT_SIM_PIN,
  EAT_SIM_PUK,
  EAT_SIM_PIN2,
  EAT_SIM_PUK2,
  EAT_SIM_NCK,
  EAT_SIM_NSCK,
  EAT_SIM_SPCK,
  EAT_SIM_CCK,
  EAT_SIM_NO_PRESENT,
  EAT_MAX_CPIN_PARAM
};

/* Entry Point */
typedef struct flEntryTag 
{
    void (*flmain)(u8 taskID);
}flEntry;

typedef struct FlMainUartDataFormatTag
{
	FlMainUartFormat uartFormat;
	FlMainUartParity uartParity;
}FlMainUartDataFormat;
typedef struct FlMainUartFlowControlStructTag
{
	FlMainUartFlowControl dcebydte;
	FlMainUartFlowControl dtebydce;
}FlMainUartFlowControlStruct;

typedef struct CBCVALUETag
{
	u8 status;
	u8 percent;
	u16 voltage;
}CBCValue;

typedef struct ServiceCellInformationTag
{
	u16 vl_Arfcn;
	u8 vl_RxLevel;
	u8 vl_Ber;
	u16 vl_Mcc;
	u16 vl_Mnc;
	u8 vl_Bsic;
	u16 vl_CellID;
	u8 vl_RxLevAccessMin;
	u8 vl_MsTxpwrMaxCch;
	u16 vl_Lac;
	u8 vl_TA;
}ServiceCellInformation;

typedef struct NeighborOneCellInfoTag
{
	u16 vl_Arfcn;
	u8 vl_RxLevel;
	u8 vl_Bsic;
	u16 vl_CellID;
	u16 vl_Mcc;
	u16 vl_Mnc;
	u16 vl_Lac;
}NeighborOneCellInfo;

typedef struct NeighborCellInfoTag
{
	NeighborOneCellInfo cellInfo[6];
}NeighborCellInfo;

#define IMEI_VALUE_LENGTH			15
typedef struct IMEIValueTag
{
	u8 imei[IMEI_VALUE_LENGTH + 1];
}IMEIValue;

#define IMSI_VALUE_LENGTH		   15
typedef struct IMSIValueTag
{
	u8 imsi[IMSI_VALUE_LENGTH + 1];
}IMSIValue;

#define ICCID_VALUE_LENGTH		   20
typedef struct ICCIDValueTag
{
	ascii iccid[ICCID_VALUE_LENGTH + 1];
}ICCIDValue;

#define PLMN_SPN_NAMES_MAX_LEN      26
typedef struct SPNValueTag
{
	u8 spnName[PLMN_SPN_NAMES_MAX_LEN + 1];
	u8 mode;
}SPNValue;

typedef void(*fl_urchandle)(u8 *data, u32 datalen);

/** Entry function type */
typedef void (*fl_task_entry)(void);

/****************************************************************************
 *  Function Prototypes
 ***************************************************************************/

/****************************************************************************/
/*  Function   : fl_entry                                                   */
/*--------------------------------------------------------------------------*/
/*  Object     : the customer application's entry                           */
/*                                                                          */
/*  Return     : None                                                       */
/*                                                                          */
/*--------------------------------------------------------------------------*/
/*  Variable Name     |IN |OUT|GLB|  Utilisation                            */
/****************************************************************************/
extern void fl_entry(void);
#endif
