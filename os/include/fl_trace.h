/***************************************************************************
 *
 *            SIMCOM Application Foreground Layer
 *
 *              Copyright (c) 2011 SIMCOM Ltd.
 *
 ***************************************************************************
 *
 *   $Id: fl_trace.h#2 $
 *   $Revision: #2 $
 *   $DateTime: 2011/12/30 09:00:00 $
 *
 ***************************************************************************
 *
 *  Designed by    :   MXN
 *  Coded by       :   MXN
 *  Tested by      :
 *
 ***************************************************************************
 *
 * File Description
 * ----------------
 *   functions and definitions for output debug info 
 *
 *
 ***************************************************************************
 *
 ***************************************************************************/
#if !defined(FL_TRACE_H)
#define FL_TRACE_H

#include <fl_typ.h>

/****************************************************************************
 *  Macros
 ***************************************************************************/
#define _DEBUG_
#ifdef _DEBUG_
/* Print trace message */
#define TRACE_DEBUG(...)				ebdat7_01DebugTrace(__VA_ARGS__)

#if defined TRACE
#undef TRACE
#endif
  #define TRACE \
    ebdat7_01DebugTrace ( "DEBUG-TRACE$%s: %s(): line %d\r\n", __FILE__, __FUNCTION__, __LINE__)
  #define TRACEs(s) \
    ebdat7_01DebugTrace ( "DEBUG-TRACE$[%s: %s (): line %d] %s\r\n", __FILE__, __FUNCTION__, __LINE__, (s))
  #define TRACEd(d) \
    ebdat7_01DebugTrace ( "DEBUG-TRACE$[%s: %s (): line %d] %d\r\n", __FILE__, __FUNCTION__, __LINE__, (d))
  #define TRACEf(x,ARGS...) \
    ebdat7_01DebugTrace ( "DEBUG-TRACE$[%s: %s (): line %d] "x, __FILE__, __FUNCTION__, __LINE__, ##ARGS)
#else/* no trace */
#if defined TRACE
#undef TRACE
#endif
  #define TRACE
  #define TRACEs(s)
  #define TRACEd(d)
  #define TRACEf(x,ARGS...)
  #define TRACE_DEBUG(__VAR__)
#endif


/****************************************************************************
 *  Function Prototypes
 ***************************************************************************/
/****************************************************************************/
/*  Function   : ebdat7_00EnterDebugMode                                    */
/*--------------------------------------------------------------------------*/
/*  Object     : Enable debug mode to output debug info through debug port  */
/*                                                                          */
/*  Return     : None                                                       */
/*                                                                          */
/*--------------------------------------------------------------------------*/
/*  Variable Name     |IN |OUT|GLB|  Utilisation                            */
/*--------------------+---+---+---+-----------------------------------------*/
/****************************************************************************/
extern void ebdat7_00EnterDebugMode(void);
extern void (*const eat_EnterDebugMode)(void);

/****************************************************************************/
/*  Function   : ebdat7_02DebugUartSend                                     */
/*--------------------------------------------------------------------------*/
/*  Object     : Send the data to the debug port.                           */
/*                                                                          */
/*  Return     : FL_ERROR: incorrect parameter detected                     */
/*  	         FL_OK: send the data successfully                          */
/*--------------------------------------------------------------------------*/
/*  Variable Name     |IN |OUT|GLB|  Utilisation                            */
/*--------------------+---+---+---+-----------------------------------------*/
/*  buff              | X |   |   |  buffer to be sent and can't be NULL    */
/*  len               | X |   |   |  the data length to be sent             */
/*                                   and can't exceed 512                   */
/****************************************************************************/
extern s32 ebdat7_02DebugUartSend(u8 *buff, u16 len);
extern s32 (*const eat_DebugUartSend)(u8 *buff, u16 len);


/*****************************************************************************/
/*  Function : ebdat7_01DebugTrace                                           */
/*---------------------------------------------------------------------------*/
/*  Object   : output a formatted string to debug port, similar to printf()  */
/*                  \r is outputed to the beginning of a new line            */
/*                  \n is outputed to a new line                             */
/*                                                                           */
/*  Return   : None                                                          */
/*                                                                           */
/*---------------------------------------------------------------------------*/
/*  Variable Name     |IN |OUT|GLB|  Utilisation                             */
/*--------------------+---+---+---+------------------------------------------*/
/*    format          | X |   |   |  format string                           */
/*--------------------+---+---+---+------------------------------------------*/
/*    ...             | X |   |   |  arguments                               */
/*--------------------+---+---+---+------------------------------------------*/
/*****************************************************************************/
extern void ebdat7_01DebugTrace (const char *Format, ... );
extern void (*const eat_DebugTrace) (const char *Format, ... );

#ifdef HAS_DEBUG_PORT_CONTROL
/*****************************************************************************/
/*  Function   : ebdat7_03CloseUart0                                         */
/*---------------------------------------------------------------------------*/
/*  Object     : close Debug serial port(Uart 0)                             */
/*                                                                           */
/*  Return     : none                                                        */
/*                                                                           */
/*---------------------------------------------------------------------------*/
/*  Variable Name     |IN |OUT|GLB|  Utilisation                             */
/*--------------------+---+---+---+------------------------------------------*/
/*                    | x | x | x |                                          */
/*--------------------+---+---+---+------------------------------------------*/
/*****************************************************************************/
extern void ebdat7_03CloseUart0(void);
extern void (*const eat_CloseUart0)(void);

/*****************************************************************************/
/*  Function   : ebdat7_04ChangeDebugPortBaudrate                            */
/*---------------------------------------------------------------------------*/
/*  Object     : Set Debug serial port Baudrate(Uart 0)                      */
/*                                                                           */
/*  Return     : TRUE: set successfully.                                     */
/*                  FALSE: set failed.                                       */
/*---------------------------------------------------------------------------*/
/*  Variable Name     |IN   |OUT|GLB|  Utilisation                           */
/*----------------+---+---+---+---+------------------------------------------*/
/*  baudrate             | x   |     |     | only support 115200,57600,      */
/*                                           38400,19200,9600,4800,2400, 1200*/
/*----------------+---+---+---+---+------------------------------------------*/
/*****************************************************************************/
extern u8 ebdat7_04ChangeDebugPortBaudrate(u32 baudrate);
extern u8 (*const eat_ChangeDebugPortBaudrate)(u32 baudrate);

/*****************************************************************************/
/* Procedure name :  ebdat7_05ChangeDebugPortDataFormat                      */
/* Object :  change debug uart data format                                   */
/*---------------------------------------------------------------------------*/
/* Return                                                                    */
/*           FL_OK: set successfully                                         */
/*           FL_ERROR: set failed                                            */
/*---------------------------------------------------------------------------*/
/*Variable Name   |IN |OUT|GLB|  Utilisation                                 */
/*----------------+---+---+---+----------------------------------------------*/
/*uartDataFormat  | X |   |   |  debug uart data format to be set.           */
/*----------------+---+---+---+----------------------------------------------*/
/*****************************************************************************/
extern s32 ebdat7_05ChangeDebugPortDataFormat(FlMainUartDataFormat uartDataFormat);
extern s32 (*const eat_ChangeDebugPortDataFormat)(FlMainUartDataFormat uartDataFormat);
#endif

#endif
/* END OF FILE */

