/***************************************************************************
 *
 *            SIMCOM Application Foreground Layer
 *
 *              Copyright (c) 2011 SIMCOM Ltd.
 *
 ***************************************************************************
 *
 *   $Id: fl_timer.h#2 $
 *   $Revision: #2 $
 *   $DateTime: 2011/12/30 09:00:00 $
 *
 ***************************************************************************
 *
 *  Designed by    :   MXN
 *  Coded by       :   MXN
 *  Tested by      :
 *
 ***************************************************************************
 *
 * File Description
 * ----------------
 *   functions and definitions for timer operation
 *
 *
 ***************************************************************************
 *
 ***************************************************************************/
#ifndef FL_TIMER_H
#define FL_TIMER_H

/****************************************************************************
 *  Macros
 ***************************************************************************/
#define FL_TIMER_NOT_RUNNING     0xffff

/****************************************************************************
 * Type Definitions
 ***************************************************************************/

/* Timer */
typedef struct FlTimerTag
{
    u32		timeoutPeriod;
    u16		timerId;
}
t_emb_Timer;

typedef struct FlSysTimerTag
{
    u16 year;
    u8 month;
    u8 day;
    u8 hour;
    u8 minute;
    u8 second;
}
t_emb_SysTimer;

/****************************************************************************
 *  Function Prototypes
 ***************************************************************************/

/* Timer Interface */
/***************************************************************************/
/*Function   : ebdat8_01StartTimer                                         */
/*-------------------------------------------------------------------------*/
/*Object     : Start a timer                                               */
/*                                                                         */
/*Return     : FL_RET_ERR_PARAM:     starting a timer fail.                */
/*             FL_RET_ERR_BAD_STATE: The timer has been started.           */
/*             FL_OK:                starting a timer succeed.             */
/*-------------------------------------------------------------------------*/
/*Variable Name   |IN |OUT|GLB|   Utilisation                              */
/*----------------+---+---+---+--------------------------------------------*/
/*timer           | X |   |   |   the sturct of timer                      */
/*----------------+---+---+---+--------------------------------------------*/
/***************************************************************************/
extern s32 ebdat8_01StartTimer(t_emb_Timer timer);
extern s32 (*const eat_StartTimer)(t_emb_Timer timer);

/***************************************************************************/
/*Function   : ebdat8_02StopTimer                                          */
/*-------------------------------------------------------------------------*/
/*Object     : Stop a timer                                                */
/*                                                                         */
/*Return     : FL_RET_ERR_PARAM:  stopping a timer fail.                   */
/*             FL_OK:             stopping a timer succeed.                */
/*-------------------------------------------------------------------------*/
/*Variable Name     |IN |OUT|GLB|  Utilisation                             */
/*------------------+---+---+---+------------------------------------------*/
/*timer             | X |   |   |  the sturct of timer                     */
/*------------------+---+---+---+------------------------------------------*/
/***************************************************************************/
extern s32 ebdat8_02StopTimer(t_emb_Timer timer); 
extern s32 (*const eat_StopTimer)(t_emb_Timer timer); 

/***************************************************************************/
/*Function   : ebdat8_06GetSystemTime                                      */
/*-------------------------------------------------------------------------*/
/*Object     : Get the system's time                                       */
/*                                                                         */
/*Return     : FL_OK: get the system time successfully                     */
/*             FL_RET_ERR_PARAM: the parameter is not correct              */
/*                                                                         */
/*-------------------------------------------------------------------------*/
/*Variable Name     |IN |OUT|GLB|  Utilisation                             */
/*------------------+---+---+---+------------------------------------------*/
/*datetime          |   | X |   |  the sturct of FlSysTimer                */
/*------------------+---+---+---+------------------------------------------*/
/***************************************************************************/
extern s32 ebdat8_06GetSystemTime(t_emb_SysTimer *datetime);
extern s32 (*const eat_GetSystemTime)(t_emb_SysTimer *datetime);

/***************************************************************************/
/*Function   : ebdat8_03GetRelativeTime                                    */
/*-------------------------------------------------------------------------*/
/*Object     : Get remaining ticks before the timer expires                */
/*                                                                         */
/*Return     : FL_OK: get the remaining ticks successfully                 */
/*             FL_RET_ERR_PARAM: the parameter is not correct              */
/*-------------------------------------------------------------------------*/
/*Variable Name     |IN |OUT|GLB|  Utilisation                             */
/*------------------+---+---+---+------------------------------------------*/
/*timer             | X |   |   |  the timer to get the ticks from         */
/*tick              |   | X |   |  the ticks left for the timer.           */
/***************************************************************************/
extern s32  ebdat8_03GetRelativeTime(t_emb_Timer timer, u32 *tick);
extern s32  (*const eat_GetRelativeTime)(t_emb_Timer timer, u32 *tick);

/***************************************************************************/
/*Function   :  ebdat8_04SecondToTicks                                     */
/*-------------------------------------------------------------------------*/
/*Object     : convert second into system ticks                            */
/*                                                                         */
/*Return     : ticks.                                                      */
/*-------------------------------------------------------------------------*/
/*Variable Name     |IN |OUT|GLB|  Utilisation                             */
/*------------------+---+---+---+------------------------------------------*/
/*seconds           | X |   |   |  the seconds time to convert             */
/*------------------+---+---+---+------------------------------------------*/
/***************************************************************************/
extern u32 ebdat8_04SecondToTicks(u32 seconds);
extern u32 (*const eat_SecondToTicks)(u32 seconds);

/***************************************************************************/
/*Function   :  ebdat8_05MillisecondToTicks                                */
/*-------------------------------------------------------------------------*/
/*Object     : convert millisecond into system ticks                       */
/*                                                                         */
/*Return     : ticks.                                                      */
/*-------------------------------------------------------------------------*/
/*Variable Name     |IN |OUT|GLB|  Utilisation                             */
/*------------------+---+---+---+------------------------------------------*/
/*milliseconds      | X |   |   |  the milliseconds time to convert        */
/*------------------+---+---+---+------------------------------------------*/
/***************************************************************************/
extern u32 ebdat8_05MillisecondToTicks(u32 milliseconds);
extern u32 (*const eat_MillisecondToTicks)(u32 milliseconds);

/***************************************************************************/
/*Function   : ebdat8_08GetSystemTickCounter                               */
/*-------------------------------------------------------------------------*/
/*Object     : Get system ticks since powering on                          */
/*                                                                         */
/*Return     : the ticks since the system powering on.                     */
/*                                                                         */
/*-------------------------------------------------------------------------*/
/*Variable Name     |IN |OUT|GLB|  Utilisation                             */
/*------------------+---+---+---+------------------------------------------*/
/***************************************************************************/
extern u32 ebdat8_08GetSystemTickCounter(void);
extern u32 (*const eat_GetSystemTickCounter)(void);

/***************************************************************************/
/*Function   : ebdat8_10CurrentTaskSleep                                   */
/*-------------------------------------------------------------------------*/
/*Object     : the task sleep                                              */
/*                                                                         */
/*Return     : FL_OK: sleep successfully                                   */
/*             FL_RET_ERR_PARAM: the parameter is not correct              */
/*                                                                         */
/*-------------------------------------------------------------------------*/
/*Variable Name     |IN |OUT|GLB|  Utilisation                             */
/*------------------+---+---+---+------------------------------------------*/
/*tick              | X |   |   |  the time to sleep, it must use          */
/*   ebdat8_05MillisecondToTicks to convert millisecond to tick or use     */
/*   ebdat8_04SecondToTicks to convert second to tick.                     */
/***************************************************************************/
extern s32 ebdat8_10CurrentTaskSleep(u32 tick);
extern s32 (*const eat_CurrentTaskSleep)(u32 tick);

/***************************************************************************/
/*Function   : ebdat8_11StopTaskSleep                                      */
/*-------------------------------------------------------------------------*/
/*Object     : set the system time                                         */
/*                                                                         */
/*Return     : FL_OK: set the system time successfully                     */
/*             FL_RET_ERR_PARAM: the parameter is not correct              */
/*                FL_RET_ERR_FATAL: set the system time failed.            */
/*                                                                         */
/*-------------------------------------------------------------------------*/
/*Variable Name     |IN |OUT|GLB|  Utilisation                             */
/*------------------+---+---+---+------------------------------------------*/
/*taskID            | X |   |   |  task ID                                 */
/***************************************************************************/
extern s32 ebdat8_11StopTaskSleep(u8 taskID);
extern s32 (*const eat_StopTaskSleep)(u8 taskID);

/***************************************************************************/
/*Function   : ebdat8_12SetSystemTime                                      */
/*-------------------------------------------------------------------------*/
/*Object     : set the system time                                         */
/*                                                                         */
/*Return     : FL_OK: set the system time successfully                     */
/*             FL_RET_ERR_PARAM: the parameter is not correct              */
/*                FL_RET_ERR_FATAL: set the system time failed.            */
/*                                                                         */
/*-------------------------------------------------------------------------*/
/*Variable Name     |IN |OUT|GLB|  Utilisation                             */
/*------------------+---+---+---+------------------------------------------*/
/*datetime             | X |   |   |  the time you want to set             */
/*timeZone            | X |   |   |  the timezone you want to set          */
/***************************************************************************/
extern s32 ebdat8_12SetSystemTime(t_emb_SysTimer datetime, s8 timeZone);
extern s32 (*const eat_SetSystemTime)(t_emb_SysTimer datetime, s8 timeZone);

#endif
/*END OF FILE*/
