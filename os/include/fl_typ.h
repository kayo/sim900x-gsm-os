/***************************************************************************
 *
 *            SIMCOM Application Foreground Layer
 *
 *              Copyright (c) 2011 SIMCOM Ltd.
 *
 ***************************************************************************
 *
 *   $Id: fl_type.h#2 $
 *   $Revision: #2 $
 *   $DateTime: 2011/12/30 09:00:00 $
 *
 ***************************************************************************
 *
 *  Designed by    :   MXN
 *  Coded by       :   MXN
 *  Tested by      :
 *
 ***************************************************************************
 *
 * File Description
 * ----------------
 *   Header file for type definitions
 *
 *
 ***************************************************************************
 *
 ***************************************************************************/

#if !defined(FL_TYP)
#define FL_TYP


/****************************************************************************
 * Constants
 ***************************************************************************/
/* Boolean values for 'bool' type */
/****************************************************************************
 *  Macros
 ***************************************************************************/
#ifndef FALSE
#define FALSE    0
#endif
#ifndef TRUE
#define TRUE     1
#endif

#ifndef false
#define false    0
#endif
#ifndef true
#define true     1
#endif

/* standard returned values of function */
#define OK   0
#define ERROR -1

#ifndef NULL
#define NULL                    ((void *) 0)
#endif

#if defined COMP_GCC
#if !defined __align__
#define __align__ __attribute__((aligned (4)))
#define __packed__ __attribute__((packed))
#endif
#else
#define __align__ __align(4)
#define __packed__ __packed
#endif

/****************************************************************************
 * Type Definitions
 ***************************************************************************/
typedef unsigned char   bool;
typedef unsigned char   u8;
typedef signed   char   s8;
typedef          char   ascii;
typedef unsigned short  u16;
typedef          short  s16;
typedef unsigned int    u32;
typedef          int    s32;
typedef unsigned int    ticks;
typedef unsigned long long u64;
typedef long long       s64;
typedef unsigned char BYTE;
typedef unsigned short WORD;
typedef unsigned char BOOL;
typedef unsigned int    size_t  ;

#ifndef Uint64
typedef unsigned long long Uint64;
#endif /* #ifndef Uint64 */
#ifndef Uint32    
typedef unsigned long  Uint32;
#endif /* #ifndef Uint32 */

#ifndef Uint8    
typedef unsigned char  Uint8;
#endif /* #ifndef Uint8 */

#if defined COMP_GCC
#define gu8 __align__ 	u8
#define gs8 __align__ 	s8
#define gascii __align__ 	ascii
#define gu16 __align__ 	u16
#define gs16 __align__ s16
#else
#define gu8 u8 __align(4)
#define gs8 s8 __align(4)
#define gascii ascii __align(4)
#define gu16 u16 __align(4)
#define gs16 s16 __align(4)
#define gbool u8 __align(4)
#endif
#endif

/*END OF FILE*/
