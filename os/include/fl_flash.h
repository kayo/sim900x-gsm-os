/***************************************************************************
 *
 *            SIMCOM Application Foreground Layer
 *
 *              Copyright (c) 2011 SIMCOM Ltd.
 *
 ***************************************************************************
 *
 *   $Id: fl_flash.h#2 $
 *   $Revision: #2 $
 *   $DateTime: 2011/12/30 09:00:00 $
 *
 ***************************************************************************
 *
 *  Designed by    :   MXN
 *  Coded by       :   MXN
 *  Tested by      :
 *
 ***************************************************************************
 *
 * File Description
 * ----------------
 *   Flash processing functions
 *
 *
 ***************************************************************************
 *
 ***************************************************************************/
#if !defined (FL_FLASH_H)
#define       FL_FLASH_H
typedef enum FlFileOperationModeTag
{
	FL_FILE_FROM_BEGINNING,
	FL_FILE_FROM_END,
	FL_NUM_FILE_OPERATION_MODE
}FlFileOperationMode;

/******************************************************************************
 * Nested Include Files
 *****************************************************************************/
/*****************************************************************************/
/*  Function   : ebdat3_03FlashWriteData                                     */      
/*---------------------------------------------------------------------------*/
/*  Object     : Write data into Flash ROM                                   */
/*                                                                           */
/*  Return     : FL_OK on successful writing                                 */
/*               FL_RET_ERR_PARAM if one parameter has an incorrect value    */
/*               FL_RET_ERR_FATAL if a fatal error has occured               */
/*               FL_ERROR  if data area is corrupt                           */
/*                                                                           */
/*---------------------------------------------------------------------------*/
/*  Variable Name     |IN |OUT|GLB|  Utilisation                             */
/*--------------------+---+---+---+------------------------------------------*/
/*  ID                | X |   |   |Identifier assigned to the stored data.   */
/*                                 Note that it must be less than 60000.     */
/*--------------------+---+---+---+------------------------------------------*/
/*  len               | X |   |   | length of the data to be stored          */
/*                                  and it must be <=8k bytes                */
/*--------------------+---+---+---+------------------------------------------*/
/*  data              | X |   |   | point to the data to be stroed           */       
/*--------------------+---+---+---+------------------------------------------*/
/*****************************************************************************/
extern s32 ebdat3_03FlashWriteData(u16 ID, u16 len, u8* data);
extern s32 (*const eat_FlashWriteData)(u16 ID, u16 len, u8* data);


/*****************************************************************************/
/*  Function   : ebdat3_04FlashReadData                                      */
/*---------------------------------------------------------------------------*/
/*  Object     : read data identified by ID from Flash ROM                   */                  
/*                                                                           */              
/*  Return     : FL_OK on successful reading                                 */
/*               FL_RET_ERR_PARAM if one parameter has an incorrect value    */
/*               FL_RET_ERR_FATAL if a fatal error has occured               */
/*               FL_ERROR  if data area is corrupt                           */
/*                                                                           */
/*---------------------------------------------------------------------------*/
/*  Variable Name     |IN |OUT|GLB|  Utilisation                             */
/*--------------------+---+---+---+------------------------------------------*/
/*  ID                | X |   |   |Identifier assigned to the stored data.   */
/*                                 Note that it must be less than 60000.     */
/*--------------------+---+---+---+------------------------------------------*/
/*  len               | X |   |   | exact length of the flash data           */
/*                                  and it must be <=8k bytes                */
/*--------------------+---+---+---+------------------------------------------*/
/*  data              |   | X |   | buffer used to store retrieved data      */
/*--------------------+---+---+---+------------------------------------------*/
/*****************************************************************************/
extern s32 ebdat3_04FlashReadData(u16 ID, u16 len, u8* data);
extern s32 (*const eat_FlashReadData)(u16 ID, u16 len, u8* data);


/*****************************************************************************/
/*  Function   : ebdat3_05FlashGetLen                                        */
/*---------------------------------------------------------------------------*/
/*  Object     : get the length of the data  identified by ID                */
/*                                                                           */
/*  Return     : FL_OK on successful reading                                 */
/*               FL_RET_ERR_PARAM if one parameter has an incorrect value    */
/*               FL_RET_ERR_FATAL if a fatal error has occured               */
/*                                                                           */
/*---------------------------------------------------------------------------*/
/*  Variable Name     |IN |OUT|GLB|  Utilisation                             */
/*--------------------+---+---+---+------------------------------------------*/
/*  ID                | X |   |   | Identifier assigned to the stored data.  */
/*                                  Note that it must be less than 60000.    */
/*--------------------+---+---+---+------------------------------------------*/
/*  length            |   | X |   | length of the data identified by ID      */
/*--------------------+---+---+---+------------------------------------------*/
/*****************************************************************************/
extern s32 ebdat3_05FlashGetLen(u16 ID,u16*  length);
extern s32 (*const eat_FlashGetLen)(u16 ID,u16*  length);

/*****************************************************************************/
/*  Function   : ebdat3_06FlashDelete                                        */
/*---------------------------------------------------------------------------*/
/*  Object     : delete the data identified by ID from Flash ROM             */
/*                                                                           */
/*  Return     : FL_OK on successful reading                                 */
/*               FL_RET_ERR_PARAM if one parameter has an incorrect value    */
/*               FL_RET_ERR_FATAL if a fatal error has occured               */
/*               FL_ERROR  if data area is corrupt                           */
/*---------------------------------------------------------------------------*/
/*  Variable Name     |IN |OUT|GLB|  Utilisation                             */
/*--------------------+---+---+---+------------------------------------------*/
/*  ID                | X |   |   |Identifier assigned to the stored data.   */
/*                                 Note that it must be less than 60000.     */
/*--------------------+---+---+---+------------------------------------------*/
/*****************************************************************************/
extern s32 ebdat3_06FlashDelete(u16 ID);
extern s32 (*const eat_FlashDelete)(u16 ID);

/*****************************************************************************/
/*  Function   : ebdat3_07FlashGetFreeSize                                   */
/*---------------------------------------------------------------------------*/
/*  Object     : get the free space available on Flash ROM                   */
/*                                                                           */
/*  Return     : FL_OK on successful reading                                 */
/*               FL_RET_ERR_FATAL if a fatal error has occured               */
/*                                                                           */
/*---------------------------------------------------------------------------*/
/*  Variable Name     |IN |OUT|GLB|  Utilisation                             */
/*--------------------+---+---+---+------------------------------------------*/
/*  freeSize          |   | X |   | the free size of the flash.              */
/*****************************************************************************/
extern s32 ebdat3_07FlashGetFreeSize(u32 *freeSize);
extern s32 (*const eat_FlashGetFreeSize)(u32 *freeSize);


/*****************************************************************************/
/*  Function   : ebdat3_08FlashFileRead                                      */
/*---------------------------------------------------------------------------*/
/*  Object     : read file from Flash ROM                                    */
/*                                                                           */
/*  Return     : FL_OK on successful reading                                 */
/*               FL_RET_ERR_PARAM if one parameter has an incorrect value    */
/*               FL_RET_ERR_FATAL if a fatal error has occured               */
/*               FL_ERROR  if data area is corrupt                           */
/*                                                                           */
/*---------------------------------------------------------------------------*/
/*  Variable Name     |IN |OUT|GLB|  Utilisation                             */
/*--------------------+---+---+---+------------------------------------------*/
/*--------------------+---+---+---+------------------------------------------*/
/*  len               | X |   |   | length of the file to read(in bytes)     */
/*--------------------+---+---+---+------------------------------------------*/
/*  data              | X |   |   | buffer used to store retrieved data      */
/*--------------------+---+---+---+------------------------------------------*/
/*  filename          | X |   |   | file name associated with the data       */
/*--------------------+---+---+---+------------------------------------------*/
/*  position          | X |   |   | position of the file to read from        */
/*--------------------+---+---+---+------------------------------------------*/
/*****************************************************************************/
extern s32 ebdat3_08FlashFileRead (u16 len, u8* data,u8* fileName, u16 position);
extern s32 (*const eat_FlashFileRead) (u16 len, u8* data,u8* fileName, u16 position);

/*****************************************************************************/
/*  Function   : ebdat3_09FlashFileWrite                                     */
/*---------------------------------------------------------------------------*/
/*  Object     : Write file into Flash ROM                                   */
/*                                                                           */
/*  Return     : FL_OK on successful writing                                 */
/*              FL_RET_ERR_PARAM if one parameter has an incorrect value     */
/*              FL_RET_ERR_FATAL if a fatal error has occured                */
/*              FL_ERROR  if data area is corrupt                            */
/*                                                                           */
/*---------------------------------------------------------------------------*/
/*  Variable Name     |IN |OUT|GLB|  Utilisation                             */
/*--------------------+---+---+---+------------------------------------------*/
/*--------------------+---+---+---+------------------------------------------*/
/*  Len               | X |   |   |length of the data to write(in bytes)     */
/*--------------------+---+---+---+------------------------------------------*/
/*  data              | X |   |   | point to the data to be stroed           */
/*--------------------+---+---+---+------------------------------------------*/
/*  filename          | X |   |   | file name assigned to the data           */
/*--------------------+---+---+---+------------------------------------------*/
/*  mode              | X |   |   | the writing mode of the file             */
/*--------------------+---+---+---+------------------------------------------*/
/*****************************************************************************/
extern s32 ebdat3_09FlashFileWrite(u16 len, u8* data, u8* fileName, FlFileOperationMode mode);
extern s32 (*const eat_FlashFileWrite)(u16 len, u8* data, u8* fileName, FlFileOperationMode mode);

/*****************************************************************************/
/*  Function   : ebdat3_10FlashFileDelete                                    */
/*---------------------------------------------------------------------------*/
/*  Object     : Delete Flash ROM File                                       */
/*                                                                           */
/*  Return     : FL_OK on successful reading                                 */
/*               FL_RET_ERR_PARAM if one parameter has an incorrect value    */
/*               FL_RET_ERR_FATAL if a fatal error has occured               */
/*               FL_ERROR  if data area is corrupt                           */
/*---------------------------------------------------------------------------*/
/*  Variable Name     |IN |OUT|GLB|  Utilisation                             */
/*--------------------+---+---+---+------------------------------------------*/
/*--------------------+---+---+---+------------------------------------------*/
/*  filename          | X |   |   | the file name to be deleted              */
/*--------------------+---+---+---+------------------------------------------*/
/*****************************************************************************/
extern s32 ebdat3_10FlashFileDelete(u8* fileName);
extern s32 (*const eat_FlashFileDelete)(u8* fileName);

/*****************************************************************************/
/*  Function   : ebdat3_11FlashFileGetLen                                    */
/*---------------------------------------------------------------------------*/
/*  Object     : Get Flash ROM File content length information               */
/*                                                                           */
/*  Return     : FL_OK on successful reading                                 */
/*               FL_RET_ERR_PARAM if one parameter has an incorrect value    */
/*               FL_RET_ERR_FATAL if a fatal error has occured               */
/*               FL_ERROR  if data area is corrupt                           */
/*                                                                           */
/*---------------------------------------------------------------------------*/
/*  Variable Name     |IN |OUT|GLB|  Utilisation                             */
/*--------------------+---+---+---+------------------------------------------*/
/*--------------------+---+---+---+------------------------------------------*/
/*  filename          | X |   |   | file name associated with flash data     */
/*  length            |   | X |   | retrieved file length                    */
/*--------------------+---+---+---+------------------------------------------*/
/*****************************************************************************/
extern s32 ebdat3_11FlashFileGetLen(u8* fileName,u16*  length);
extern s32 (*const eat_FlashFileGetLen)(u8* fileName,u16*  length);


/*FOR EMBEDDEDAT AUDIO PLAY -e */
#endif /* FL_FLASH_H */

/* END OF FILE */
