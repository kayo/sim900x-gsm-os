/***************************************************************************
 *
 *            SIMCOM Application Foreground Layer
 *
 *              Copyright (c) 2011 SIMCOM Ltd.
 *
 ***************************************************************************
 *
 *   $Id: fl_stdlib.h#2 $
 *   $Revision: #2 $
 *   $DateTime: 2011/12/30 09:00:00 $
 *
 ***************************************************************************
 *
 *  Designed by    :   MXN
 *  Coded by       :   MXN
 *  Tested by      :
 *
 ***************************************************************************
 *
 * File Description
 * ----------------
 *   functions and definitions for ANSI C and string processing
 *
 *
 ***************************************************************************
 *
 ***************************************************************************/

#if !defined (FL_STDLIB_H)
#define       FL_STDLIB_H


/****************************************************************************
 * Nested Include Files
 ***************************************************************************/
#include <string.h>
#include <stdlib.h>

#include <stdio.h>
#include <fl_typ.h>

/****************************************************************************
 * Constants
 ***************************************************************************/


/****************************************************************************
 * Type Definitions
 ***************************************************************************/


/****************************************************************************
 *  Macros
 ***************************************************************************/
#define  fl_strcpy        strcpy
#define  fl_strncpy       strncpy
#define  fl_strcat        strcat
#define  fl_strncat       strncat
#define  fl_strlen        strlen
#define  fl_strcmp        strcmp
#define  fl_strncmp       strncmp
#define  fl_strnicmp      ebdat4_09strnicmp
#define  fl_memset        memset
#define  fl_memcpy        memcpy
#define  fl_memcmp        memcmp
#define  fl_itoa          ebdat4_07itoa
#define  fl_atoi          atoi
#define  fl_sprintf       sprintf
#define  fl_memmove       memmove

/****************************************************************************
 *  Function Prototypes
 ***************************************************************************/

/****************************************************************************/
/*  Function   : ebdat4_10strRemoveCRLF                                     */
/*--------------------------------------------------------------------------*/
/*  Object     : Remove CR(0x0D) LF(0x0A) in string                         */
/*                                                                          */
/*  Return     : String without CR/LF                                       */
/*                                                                          */
/*--------------------------------------------------------------------------*/
/*  Variable Name     |IN |OUT|GLB|  Utilisation                            */
/*--------------------+---+---+---+-----------------------------------------*/
/*  dst               |   | X |   |  destination string without CR&LF       */
/*--------------------+---+---+---+-----------------------------------------*/
/*  src               | X |   |   |  source string with CR&LF characters    */
/*--------------------+---+---+---+-----------------------------------------*/
/*  size              | X |   |   |  the size of source string              */
/*--------------------+---+---+---+-----------------------------------------*/
/****************************************************************************/
extern ascii * ebdat4_10strRemoveCRLF ( ascii * dst, ascii * src, u16 size );
extern ascii * (*const eat_strRemoveCRLF) ( ascii * dst, ascii * src, u16 size );

/****************************************************************************/
/*Function   : ebdat4_11strGetParameterString                               */
/*--------------------------------------------------------------------------*/
/*Object     : Return the parameter string at a given position              */
/*                                                                          */
/*Return     : parameter string or NULL if not found                        */
/*                                                                          */
/*--------------------------------------------------------------------------*/
/*Variable Name     |IN |OUT|GLB|  Utilisation                              */
/*------------------+---+---+---+-------------------------------------------*/
/*dst               |   | X |   |  parameter string at given position       */
/*------------------+---+---+---+-------------------------------------------*/
/*src               | X |   |   |  source string to parse                   */
/*------------------+---+---+---+-------------------------------------------*/
/*Position          | X |   |   |  Parameter position                       */
/*------------------+---+---+---+-------------------------------------------*/
/****************************************************************************/
extern ascii * ebdat4_11strGetParameterString ( ascii * dst, const ascii * src, u8 Position );
extern ascii * (*const eat_strGetParameterString) ( ascii * dst, const ascii * src, u8 Position );

#endif /* Fl_STDIO_H */

/* END OF FILE */

