/***************************************************************************
 *
 *            SIMCOM Application Foreground Layer
 *
 *              Copyright (c) 2011 SIMCOM Ltd.
 *
 ***************************************************************************
 *
 *   $Id: Fl_pinforsim900.h#2 $
 *   $Revision: #2 $
 *   $DateTime: 2011/12/30 09:00:00 $
 *
 ***************************************************************************
 *
 *  Designed by    :   MXN
 *  Coded by       :   MXN
 *  Tested by      :
 *
 ***************************************************************************
 *
 * File Description
 * ----------------
 *   Peripheral GPIO definitions
 *
 *
 ***************************************************************************
 *
 ***************************************************************************/

#if !defined (FL_PINFORSIM900_H)
#define FL_PINFORSIM900_H
typedef enum FlPinNameTag
{            /* default,  multi,    GPIO,  I2C*/
	
/*	EVB PIN 		 default function			 MAP				used as*/
#ifdef HAS_KBD_PERIPHERY
	FL_PIN_3,/*		UART_DTR			 GPIO20 				I/O*/
#endif
	FL_PIN_4,/*		UART_RI				 GPIO40 				I/O*/
	FL_PIN_5,/*		UART_DCD			 GPIO51 				I/O*/
	FL_PIN_6,/*		UART_DSR			 GPIO53 				I/O*/
	FL_PIN_11,/*	SPI_CLK				 GPIO48 				I/O*/
	FL_PIN_12,/*	SPI_DATA			 GPIO50 				I/O*/
	FL_PIN_13,/*	SPI_DC				 GPIO49 				I/O*/
	FL_PIN_14,/*	SPI_CS				 GPIO52 				I/O*/
	FL_PIN_34,/*	SIM_PRES			 GPIO8				I/O*/
	FL_PIN_37,/*	I2C_SDA				 GPIO39 				I/O*/
	FL_PIN_38,/*	I2C_SCL				 GPIO38 				I/O*/
#ifdef HAS_KBD_PERIPHERY
	FL_PIN_40,/*	KBR4/GPIO1 			 GPIO13 				I/O*/
	FL_PIN_41,/*	KBR3/GPIO2 			 GPIO35 				I/O*/
	FL_PIN_42,/*	KBR2/GPIO3 			 GPIO34 				I/O*/
	FL_PIN_43,/*	KBR1/GPIO4 			 GPIO33 				I/O*/
	FL_PIN_44,/*	KBR0/GPIO5 			 GPIO32 				I/O*/
	FL_PIN_47,/*	KBC4/GPIO6 			 GPIO11 				I/O*/
	FL_PIN_48,/*	KBC3/GPIO7 			 GPIO12 				I/O*/
	FL_PIN_49,/*	KBC2/GPIO8 			 GPIO16 				I/O*/
	FL_PIN_50,/*	KBC1/GPIO9 			 GPIO15 				I/O*/
	FL_PIN_51,/*	KBC0/GPIO9 			 GPIO14 				I/O*/
#else
	FL_PIN_44,/*	SIM2_PRES 			 GPIO13 				I/O*/
	FL_PIN_49,/*	GPIO4 				 GPIO11 				I/O*/
	FL_PIN_50,/*	GPIO3 				 GPIO35 				I/O*/
	FL_PIN_51,/*	GPIO2 				 GPIO33 				I/O*/
#endif
	FL_PIN_52,/*	NETLIGHT			 GPIO10 				I/O*/
	FL_PIN_66,/*	STATUS 				 GPIO57 				I/O*/
#ifdef HAS_KBD_PERIPHERY
	FL_PIN_67,/*	GPIO11 				 GPIO41 				I/O*/
	FL_PIN_68,/*	GPIO12 				 GPIO1				I/O*/
#else
	FL_PIN_67,/*	GPIO1 				 GPIO12 				I/O*/
	FL_PIN_68,/*	GPO1 				 GPIO32				I/O*/
#endif

  FL_PIN_MAX
} FlPinName;

#ifdef HAS_KBD_PERIPHERY
#define FL_NUM_OF_KEY_PINS		10
#endif
#define	FL_NUM_OF_INT_PINS		4
#endif
