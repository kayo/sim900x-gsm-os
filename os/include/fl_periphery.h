/***************************************************************************
 *
 *            SIMCOM Application Foreground Layer
 *
 *              Copyright (c) 2011 SIMCOM Ltd.
 *
 ***************************************************************************
 *
 *   $Id: fl_periphery.h#2 $
 *   $Revision: #2 $
 *   $DateTime: 2011/12/30 09:00:00 $
 *
 ***************************************************************************
 *
 *  Designed by    :   MXN
 *  Coded by       :   MXN
 *  Tested by      :
 *
 ***************************************************************************
 *
 * File Description
 * ----------------
 *   definitions and functions about peripheral interface operation
 *
 *
 ***************************************************************************
 *
 ***************************************************************************/

#if !defined (FL_PERIPHERY_H)
#define       FL_PERIPHERY_H


/****************************************************************************
 * Nested Include Files
 ***************************************************************************/
#include <fl_typ.h>

/****************************************************************************
 * Constants
 ***************************************************************************/


/****************************************************************************
 * Type Definitions
 ***************************************************************************/
/* pin interface's name */

/* the work mode of pins */
typedef enum FlPinModeTag
{
  FL_PIN_MODE_UNUSED,
    
  FL_PIN_MODE_DEFAULT,
  FL_PIN_MODE_MULTI,
  FL_PIN_MODE_GPIO,
  FL_PIN_MODE_I2C
} FlPinMode;


/* SPI clock speed */
typedef enum FlSpiClkSpeedTag
{
  FL_SPI_SCL_SPEED_6_5Mhz,
  FL_SPI_SCL_SPEED_3_25Mhz,
  FL_SPI_SCL_SPEED_2_167Mhz,
  FL_SPI_SCL_SPEED_1_625Mhz,
  FL_SPI_SCL_SPEED_1_3Mhz,  
  FL_SPI_SCL_SPEED_1_083Mhz,
  FL_SPI_SCL_SPEED_928Khz,
  FL_SPI_SCL_SPEED_812Khz,
  FL_SPI_SCL_SPEED_101Khz
} FlSpiClkSpeed;

/* SPI clock modes */
typedef enum FlSpiClkModeTag
{
  FL_SPI_CLK_MODE_0,
  FL_SPI_CLK_MODE_1,
  FL_SPI_CLK_MODE_2,
  FL_SPI_CLK_MODE_3
} FlSpiClkMode;

/* SPI Access Structure */
typedef struct FlSpiAccessTag
{
  u32   address;
  u32   opcode; 
  u8    opcodeLength;
  u8    addressLength;
} FlSpiAccess;

typedef enum FlTriggerTypeTag
{
    FL_TRIGGER_RISING_EDGE,
    Fl_TRIGGER_FALLING_EDGE,
    Fl_TRIGGER_HIGH_LEVEL,
    Fl_TRIGGER_LOW_LEVEL
}FlTriggerType;

typedef enum FlGpioDirectionTag
{
   FL_GPIO_UNUSED = 0,
   FL_GPIO_INPUT = 1,
   FL_GPIO_OUTPUT
}
FlGpioDirection;

typedef enum
{
  FL_GPIO_TRIG_ON_HIGH_LEVEL,
  FL_GPIO_TRIG_ON_LOW_LEVEL,
  FL_GPIO_TRIG_ON_RISING_EDGE,
  FL_GPIO_TRIG_ON_FALLING_EDGE
} FLGpioTriggerType;

#ifdef HAS_PWM_PERIPHERY
typedef enum FlPWMTag
{
  FL_PWM_0,
  FL_PWM_1,
  FL_PWM_MAX
}FlPWM;
#endif

/* Status */
typedef enum
{
   SSI_OK   = 0,       /* SSI transfer has been initiated */
   SSI_BUSY = 1        /* SSI already in use              */
} SsiStatusType;

/* 3 or 4 Wire Interface */
typedef enum
{ 
   SSI_3WIRE = 0,
   SSI_4WIRE = 1<<31
} SsiModeType;

/* SSI Enable Polarity */
typedef enum
{
   SSI_ACTIVE_LOW  = 0,
   SSI_ACTIVE_HIGH = 1
} SsiEnablePolarityType;

/* Clock configuration */
typedef enum
{
   SSI_SYSTEM_DIV_2      = 0,
   SSI_SYSTEM_DIV_4      = 1<<7,
   SSI_SYSTEM_DIV_8      = 2<<7,
   SSI_SYSTEM_DIV_16     = 3<<7,
   SSI_SYSTEM_DIV_32     = 4<<7,
   SSI_SYSTEM_DIV_64     = 5<<7,
   SSI_SYSTEM_DIV_128    = 6<<7,
   SSI_SYSTEM_DIV_256    = 7<<7,
   SSI_SYSTEM_DIV_512    = 8<<7
}SsiClockType;

/* SSI Data Polarity */
typedef enum
{
   SSI_FALLING_EDGE = 0,
   SSI_RISING_EDGE  = 1<<1
} SsiDataPolarityType;

/* SSI Transfer Format */
typedef enum
{
   SSI_LSBFIRST = 0,
   SSI_MSBFIRST = 1<<5
} SsiTrfFormatType;

/*+Simcom jianying.hu for Embedded AT SPI CS0~CS1~CS2 @2012-10-16*/
/* SSI Slave Device Id */
typedef enum
{
   SSI_SLAVE0 = 0, /* SIM900 PIN:DCD */
   SSI_SLAVE1 = 1, /* SIM900 PIN:DISP_CS */
   SSI_SLAVE2 = 2, /* SIM900 PIN:DSR */
   SSI_NB_OF_DEVICES
} SsiDevNbType;
/*-Simcom jianying.hu for Embedded AT SPI CS0~CS1~CS2 @2012-10-16*/


/****************************************************************************
 *  Macros
 ***************************************************************************/


/****************************************************************************
 *  Function Prototypes
 ***************************************************************************/

/***************************************************************************/
/*  Function : ebdat6_08pinConfigureToUnused                               */
/*-------------------------------------------------------------------------*/
/*  Object   : configure the specified pin to unused mode                  */
/*                                                                         */
/*  Return   : FL_OK                   on success                          */
/*             FL_RET_ERR_PARAM     if the parameter is incorrect          */
/*             FL_RET_ERR_BAD_STATE if the pin's status is unexpected      */
/*                                                                         */
/*-------------------------------------------------------------------------*/
/*  Variable Name     |IN |OUT| Utilisation                                */
/*--------------------+---+---+--------------------------------------------*/
/*  pinName           | x |   | the pin to be configured to UNUSED         */
/*                              status. Note that the FL_PIN_3 cannot be   */
/*                              set as GPIO, as it is reserved.            */
/*--------------------+---+---+--------------------------------------------*/
/***************************************************************************/
extern s32 ebdat6_08pinConfigureToUnused(FlPinName pinName);
extern s32 (*const eat_pinConfigureToUnused)(FlPinName pinName);



/***************************************************************************/
/*  Function : ebdat6_06QueryPinMode                                       */
/*-------------------------------------------------------------------------*/
/*  Object   : query the current pin mode                                  */
/*                                                                         */
/*  Return   : OK                   on success                             */
/*             FL_RET_ERR_PARAM     if the parameter is incorrect          */
/*             FL_RET_ERR_BAD_STATE if the pin's status are unexpected     */
/*                                                                         */
/*-------------------------------------------------------------------------*/
/*  Variable Name     |IN |OUT| Utilisation                                */
/*--------------------+---+---+--------------------------------------------*/
/*  pinName           | X |   | the specified pin                          */
/*  pinMode_p         |   | X | where to return the pin's mode             */
/*  isOutputDir_p     |   | X | where to return the gpio direction         */
/*                              if it's subscribed gpio                    */
/*--------------------+---+---+--------------------------------------------*/
extern s32 ebdat6_06QueryPinMode(FlPinName pinName, FlPinMode *pinMode_p, FlGpioDirection *isOutputDir_p);
extern s32 (*const eat_QueryPinMode)(FlPinName pinName, FlPinMode *pinMode_p, FlGpioDirection *isOutputDir_p);




/*<<<<<<<<<<<<<<<<<<<<<<<<<<<<< Display >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>*/

/***************************************************************************/
/*  Function   : ebdat05_09delay                                           */
/*-------------------------------------------------------------------------*/
/*  Object     : Delay microseconds                                        */
/*               just kick the watchdog                                    */
/*                                                                         */
/*  Return     : void                                                      */
/*-------------------------------------------------------------------------*/
/*  Variable Name     |IN |OUT|GLB|  Utilisation                           */
/*--------------------+---+---+---+----------------------------------------*/
/*  microseconds      | X |   |   |  microseconds (approximately)          */
/*--------------------+---+---+---+----------------------------------------*/
/***************************************************************************/
extern void ebdat05_09delay(u32 microSeconds);
extern void (*const eat_delay)(u32 microSeconds);


/***************************************************************************/
/*  Function   : ebdat05_10DispReset                                       */
/*-------------------------------------------------------------------------*/
/*  Object     : Display device reset(reset DISP_RST PIN).                 */
/*               after Display device reseted,users can call               */
/*               ebdat05_11DispConfig() to config Display device.          */
/*                                                                         */
/*  Return     : void                                                      */
/*-------------------------------------------------------------------------*/
/*  Variable Name     |IN |OUT|GLB|  Utilisation                           */
/*--------------------+---+---+---+----------------------------------------*/
/*  reset_gpio_num    | X |   |   |the gpio used as Display reset          */
/*  microseconds      | X |   |   |reset pulse width (approximately)       */
/*--------------------+---+---+---+----------------------------------------*/
/***************************************************************************/
extern void ebdat05_10DispReset(u8 reset_gpio_num,u8 microseconds);
extern void (*const eat_DispReset)(u8 reset_gpio_num,u8 microseconds);

/***************************************************************************/
/*  Function   : ebdat05_11DispConfig                                      */
/*-------------------------------------------------------------------------*/
/*  Object     : config the SPI interface as 3 line SPI Display interface: */
/*                   (1)DISP_CS  (3 line SPI:Chip Select)                  */
/*                   (2)DISP_CLK (3 line SPI:SPI clock)                    */
/*                   (3)DISP_DATA(3 line SPI:SPI data out/data in)         */
/*               after Display interface is configured,users can call      */
/*               reading or writing functions.                             */
/*                                                                         */
/*  Return     : FL_OK: Display configuration succeed                      */
/* 				 FL_ERROR: Display configuration failed                    */
/*-------------------------------------------------------------------------*/
/*  Variable Name     |IN |OUT|GLB|  Utilisation                           */
/*--------------------+---+---+---+----------------------------------------*/
/*  rst_gpio_num       | X |   |   |the gpio used as Display Reset         */
/*  clk               | X |   |   |the Frequency of SPI CLK                */
/*--------------------+---+---+---+----------------------------------------*/
/***************************************************************************/
extern s32 ebdat05_11DispConfig(FlPinName rst_gpio_num, SsiClockType clk);
extern s32 (*const eat_DispConfig)(FlPinName rst_gpio_num, SsiClockType clk);


/***************************************************************************/
/*  Function   : ebdat05_12DispWriteCommand                                */
/*-------------------------------------------------------------------------*/
/*  Object     : write one command byte to the Display interface           */
/*               this operation will also clear DISP_DC pin (low).         */
/*                                                                         */
/*  Return     : FL_OK: write command cuccessfully                         */
/*               FL_ERROR:write command failed                             */
/*-------------------------------------------------------------------------*/
/*  Variable Name     |IN |OUT|GLB|  Utilisation                           */
/*--------------------+---+---+---+----------------------------------------*/
/*  command           | X |   |   |  command byte to transfer              */
/*--------------------+---+---+---+----------------------------------------*/
/***************************************************************************/
extern s32 ebdat05_12DispWriteCommand(u8 command) ;
extern s32 (*const eat_DispWriteCommand)(u8 command) ;

/***************************************************************************/
/*  Function   : ebdat05_13DispWriteData                                   */
/*-------------------------------------------------------------------------*/
/*  Object     : write one data byte to the Display interface              */
/*               this operation will also set DISP_DC pin (high).          */
/*                                                                         */
/*  Return     : FL_OK: write data cuccessfully                            */
/*               FL_ERROR:write data failed                                */
/*-------------------------------------------------------------------------*/
/*  Variable Name     |IN |OUT|GLB|  Utilisation                           */
/*--------------------+---+---+---+----------------------------------------*/
/*  data              | X |   |   |  data byte to transfer                 */
/*--------------------+---+---+---+----------------------------------------*/
/***************************************************************************/
extern s32 ebdat05_13DispWriteData(u8 data);
extern s32 (*const eat_DispWriteData)(u8 data);



/*<<<<<<<<<<<<<<<<<<<<<<<<<<<<< SPI >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>*/

/***************************************************************************/
/*  Function   : ebdat5_01SpiConfigure                                     */
/*-------------------------------------------------------------------------*/
/*  Object     : configure SPI interface (SSI used as SPI ).               */
/*               after SPI configured,user can call read or write          */
/*               functions                                                 */
/*               call example:see function ebdat05_11DispConfig            */
/*                                                                         */
/*  Return     : FL_OK: SPI configuration successful                       */
/*               FL_ERROR:SPI configuration failed                         */
/*-------------------------------------------------------------------------*/
/*  Variable Name     |IN |OUT|GLB|  Utilisation                           */
/*--------------------+---+---+---+----------------------------------------*/
/*  wireMode          | X |   |   |  SPI wire mode Configuration           */
/*--------------------+---+---+---+----------------------------------------*/
/*  csPolHigh         | X |   |   |  CS polarity Configuration             */
/*--------------------+---+---+---+----------------------------------------*/
/*  cs_gpio_num       | X |   |   |  CS GPIO number Configuration          */
/*--------------------+---+---+---+----------------------------------------*/
/*  clkSpeed          | X |   |   |  SPI Clock speed Configuration         */
/*--------------------+---+---+---+----------------------------------------*/
/*  clkMode           | X |   |   |  write clock polarity configuration    */
/*--------------------+---+---+---+----------------------------------------*/
/*  msbFirst          | X |   |   |  Shift MSB or LSB first configuration  */
/*--------------------+---+---+---+----------------------------------------*/
/***************************************************************************/
extern s32 ebdat5_01SpiConfigure(SsiModeType           wireMode,
                                           SsiEnablePolarityType csPolHigh,
                                           FlPinName             cs_gpio_num,
                                           SsiClockType          clkSpeed, 
                                           SsiDataPolarityType   clkMode, 
                                           SsiTrfFormatType      msbFirst);
extern s32 (*const eat_SpiConfigure)(SsiModeType           wireMode,
                                           SsiEnablePolarityType csPolHigh,
                                           FlPinName             cs_gpio_num,
                                           SsiClockType          clkSpeed, 
                                           SsiDataPolarityType   clkMode, 
                                           SsiTrfFormatType      msbFirst);

/***************************************************************************/
/*  Function   : ebdat5_02SpiWriteByte                                     */
/*-------------------------------------------------------------------------*/
/*  Object     : write one byte to the SPI interface                       */
/*               (this function will set CS Pin low and high)              */
/*                                                                         */
/*  Return     : FL_OK: write one byte successfully                        */
/*               FL_ERROR: write one byte failed                           */
/*-------------------------------------------------------------------------*/
/*  Variable Name     |IN |OUT|GLB|  Utilisation                           */
/*--------------------+---+---+---+----------------------------------------*/
/*  data              | X |   |   |  byte to transfer                      */
/*--------------------+---+---+---+----------------------------------------*/
/***************************************************************************/
extern s32 ebdat5_02SpiWriteByte(u8 data);
extern s32 (*const eat_SpiWriteByte)(u8 data);

/***************************************************************************/
/*  Function   : ebdat5_03SpiReadByte                                      */
/*-------------------------------------------------------------------------*/
/*  Object     : read one byte from the SPI interface                      */
/*               (this function will set CS Pin low and high)              */
/*                                                                         */
/*  Return     : one byte data read from SPI interface                     */
/*-------------------------------------------------------------------------*/
/*  Variable Name     |IN |OUT|GLB|  Utilisation                           */
/*--------------------+---+---+---+----------------------------------------*/
/*                    |   |   |   |                                        */
/*--------------------+---+---+---+----------------------------------------*/
/***************************************************************************/
extern u8 ebdat5_03SpiReadByte (void);
extern u8 (*const eat_SpiReadByte) (void);

/***************************************************************************/
/*  Function   : ebdat5_04SpiWriteBytes                                    */
/*-------------------------------------------------------------------------*/
/*  Object     : write bytes to the SPI interface                          */
/*               (this function will set CS Pin low and high)              */
/*                                                                         */
/*  Return     : FL_OK: write bytes of data successfully                   */
/*               FL_ERROR: write bytes of data failed                      */
/*-------------------------------------------------------------------------*/
/*  Variable Name     |IN |OUT|GLB|  Utilisation                           */
/*--------------------+---+---+---+----------------------------------------*/
/*  p_data            | X |   |   |  data buffer to be sent                */
/*  dataSize          | X |   |   |  size of data to be sent               */
/*--------------------+---+---+---+----------------------------------------*/
/***************************************************************************/
extern s32 ebdat5_04SpiWriteBytes(u8 *p_data, u32 dataSize);
extern s32 (*const eat_SpiWriteBytes)(u8 *p_data, u32 dataSize);

/*+Simcom jianying.hu for Embedded AT SPI CS0~CS1~CS2 @2012-10-18*/
/*<<<<<<<<<<<<<<<<<<<<<<<<<<<<< SPI Enhance >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>*/

/***************************************************************************/
/*  Function   : ebdat5_21EnhanceSpiConfigure                                     */
/*-------------------------------------------------------------------------*/
/*  Object     : SPI interface configure (SSI used as SPI ).               */
/*               after SPI configured,user can call read  or write         */
/*               functions                                                 */
/*               call example:see function ebdat05_11DispConfig            */
/*  Return     : s32                                                       */
/*                  Spi Configure result                                   */
/*-------------------------------------------------------------------------*/
/*  Variable Name     |IN |OUT|GLB|  Utilisation                           */
/*--------------------+---+---+---+----------------------------------------*/
/*  ssiDevNb          | x |   |   |  SPI CS selection                      */
/*--------------------+---+---+---+----------------------------------------*/
/*  wireMode          | x |   |   |  SPI wire mode Configuration           */
/*--------------------+---+---+---+----------------------------------------*/
/*  clkSpeed          | x |   |   |  SPI Clock speed Configuration         */
/*--------------------+---+---+---+----------------------------------------*/
/*  clkMode           | x |   |   |  write clock polarity configuration    */
/*--------------------+---+---+---+----------------------------------------*/
/*  msbFirst          | x |   |   |  Shift MSB or LSB first configuration  */
/*--------------------+---+---+---+----------------------------------------*/
/***************************************************************************/
extern s32 ebdat5_21EnhanceSpiConfigure(SsiDevNbType          ssiDevNb,
                                        SsiModeType           wireMode,
                                        SsiClockType          clkSpeed, 
                                        SsiDataPolarityType   clkMode , 
                                        SsiTrfFormatType      msbFirst);
extern s32 (*const eat_EnhanceSpiConfigure)(SsiDevNbType          ssiDevNb,
                                        SsiModeType           wireMode,
                                        SsiClockType          clkSpeed, 
                                        SsiDataPolarityType   clkMode , 
                                        SsiTrfFormatType      msbFirst);

/***************************************************************************/
/*  Function   : ebdat5_22EnhanceSpiWriteByte                                     */
/*-------------------------------------------------------------------------*/
/*  Object     : write one byte to the SPI interface                       */
/*               (this function will set CS Pin low and high)              */
/*  Return     : s32                                                       */
/*                   return SPI write Status                               */
/*-------------------------------------------------------------------------*/
/*  Variable Name     |IN |OUT|GLB|  Utilisation                           */
/*--------------------+---+---+---+----------------------------------------*/
/*  ssiDevNb          | x |   |   |  SPI CS selection                      */
/*--------------------+---+---+---+----------------------------------------*/
/*  data              | x |   |   |  byte to transfer                      */
/*--------------------+---+---+---+----------------------------------------*/
/***************************************************************************/
extern s32  ebdat5_22EnhanceSpiWriteByte (SsiDevNbType  ssiDevNb,u8 data);
extern s32  (*const eat_EnhanceSpiWriteByte) (SsiDevNbType  ssiDevNb,u8 data);


/***************************************************************************/
/*  Function   : ebdat5_23EnhanceSpiReadByte                                      */
/*-------------------------------------------------------------------------*/
/*  Object     : read one byte from the SPI interface                      */
/*               (this function will set CS Pin low and high)              */
/*  Return     : u8                                                        */
/*                   return 1 byte read from spi                           */
/*-------------------------------------------------------------------------*/
/*  Variable Name     |IN |OUT|GLB|  Utilisation                           */
/*--------------------+---+---+---+----------------------------------------*/
/*  ssiDevNb          | x |   |   |  SPI CS selection                      */
/*--------------------+---+---+---+----------------------------------------*/
/***************************************************************************/
extern u8 ebdat5_23EnhanceSpiReadByte (SsiDevNbType  ssiDevNb);
extern u8 (*const eat_EnhanceSpiReadByte) (SsiDevNbType  ssiDevNb);


/***************************************************************************/
/*  Function   : ebdat5_24EnhanceSpiWriteBytes                                    */
/*-------------------------------------------------------------------------*/
/*  Object     : write bytes to the SPI interface                          */
/*               (this function will set CS Pin low and high)              */
/*  Return     : s32                                                       */
/*                   return SPI write Status                               */
/*-------------------------------------------------------------------------*/
/*  Variable Name     |IN |OUT|GLB|  Utilisation                           */
/*--------------------+---+---+---+----------------------------------------*/
/*  ssiDevNb          | x |   |   |  SPI CS selection                      */
/*--------------------+---+---+---+----------------------------------------*/
/*  p_data            | x |   |   |  data pointer to be sent               */
/*--------------------+---+---+---+----------------------------------------*/
/*  dataSize          | x |   |   |  size of data to be sent               */
/*--------------------+---+---+---+----------------------------------------*/
/***************************************************************************/
extern s32 ebdat5_24EnhanceSpiWriteBytes(SsiDevNbType  ssiDevNb, u8 *p_data, u32 dataSize);
extern s32 (*const eat_EnhanceSpiWriteBytes)(SsiDevNbType  ssiDevNb, u8 *p_data, u32 dataSize);

/***************************************************************************/
/*  Function   : ebdat5_25EnhanceSpiReadBytes                                     */
/*-------------------------------------------------------------------------*/
/*  Object     : read one byte from the SPI interface                      */
/*               (this function will set CS Pin low and high)              */
/*  Return     : u8                                                        */
/*                   return 1 byte read from spi                           */
/*-------------------------------------------------------------------------*/
/*  Variable Name     |IN |OUT|GLB|  Utilisation                           */
/*--------------------+---+---+---+----------------------------------------*/
/*  ssiDevNb          | x |   |   |  SPI CS selection                      */
/*--------------------+---+---+---+----------------------------------------*/
/*  p_data            |   | x |   |  data to be read                       */
/*--------------------+---+---+---+----------------------------------------*/
/*  nums              | x |   |   |  nums of data to be read               */
/*--------------------+---+---+---+----------------------------------------*/
/***************************************************************************/
extern void ebdat5_25EnhanceSpiReadBytes (SsiDevNbType  ssiDevNb, u8 *p_data, int nums);
extern void (*const eat_EnhanceSpiReadBytes) (SsiDevNbType  ssiDevNb, u8 *p_data, int nums);
/*-Simcom jianying.hu for Embedded AT SPI CS0~CS1~CS2 @2012-10-18*/


/*<<<<<<<<<<<<<<<<<<<<<<<<<<< Interrupt >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>*/

/***************************************************************************/
/*  Function : ebdat6_13IntSubscribe                                       */
/*-------------------------------------------------------------------------*/
/*  Object   : Subscribe interrupt pin                                     */
/*                                                                         */
/*  Return   : OK                   on success                             */
/*             FL_RET_ERR_PARAM     if the parameter is incorrect          */
/*             FL_RET_ERR_BAD_STATE if the pin's status are unexpected     */
/*                                                                         */
/*-------------------------------------------------------------------------*/
/*  Variable Name   |IN |OUT| Utilisation                                  */
/*------------------+---+---+----------------------------------------------*/
/*  pinName         | X |   | the specified pin                            */
/*  triggerType     | X |   | trigger type:                                */
/*                            either level or edge triggered               */
/*  deBouncePeriod  | X |   | debounce time for the interrupt              */
/*------------------+---+---+----------------------------------------------*/
extern s32 ebdat6_13IntSubscribe(FlPinName pinName,  
                   FLGpioTriggerType triggerType,  
                   u16       deBouncePeriodMs);
extern s32 (*const eat_IntSubscribe)(FlPinName pinName,  
                   FLGpioTriggerType triggerType,  
                   u16       deBouncePeriodMs);

#ifdef HAS_PWM_PERIPHERY
/*<<<<<<<<<<<<<<<<<<<<<<<<<<< Square Wave >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>*/

/***************************************************************************/
/*  Function : ebdat6_19SqWaveSubscribe                                    */
/*-------------------------------------------------------------------------*/
/*  Object   : generate PWM signal                                         */
/*                                                                         */
/*  Return   : FL_OK                   on success                          */
/*             FL_RET_ERR_PARAM if the parameter is not correct            */
/*                                                                         */
/*PWMPRD:it is the frequnce period
PWMPLS:it is the PWM pulse high time, it is equal to high time / period.
 
eg:
ebdat6_19SqWaveSubscribe(FL_PWM_0, 100,50);
PWMPRD:100--->101 pwmclk
PWMPLS:50---->51 pwmclk
 
pwmclk=sysclk(26Mhz)/8=3.25Mhz
 
PWM out:3.25Mhz/101 = 32.178Khz
high time:51*pwmclk
 
in our reference code there is limitation of input level.
if (level*period/100) = 0
then PWMPLS=127
if PWMPLS>PWMPRD then pwm out low level
 
 
if you want to set pwmclk=3.25Mhz/3=1.08Mhz
ebdat6_19SqWaveSubscribe(FL_PWM_0, 2,50);
PWMPRD:2--->3 pwmclk
50*2/100 = 1
PWMPLS:1---->2 pwmclk */
/*-------------------------------------------------------------------------*/
/*  Variable Name     |IN |OUT| Utilisation                                */
/*--------------------+---+---+--------------------------------------------*/
/*  pwm		          | x |   |pwm pin used to generate PWM signal         */
/*  pwmhalfPeriod:    | x |   |the period of PWM and must be less than 126.*/
/*  pwmlevel:         | x |   |the duty of PWM and must be less than 100.  */
/*--------------------+---+---+--------------------------------------------*/
extern s32 ebdat6_19SqWaveSubscribe(FlPWM pwm, u8 pwmhalfPeriod, u8 pwmlevel);
extern s32 (*const eat_SqWaveSubscribe)(FlPWM pwm, u8 pwmhalfPeriod, u8 pwmlevel);

/***************************************************************************/
/*  Function : ebdat6_20SqWaveUnsubscribe                                  */
/*-------------------------------------------------------------------------*/
/*  Object   : Unsubscribe a PWM pin and pull down the pin                 */
/*                                                                         */
/*  Return   : FL_OK                   on success                          */
/*             FL_RET_ERR_PARAM if the parameter is not correct            */
/*                                                                         */
/*-------------------------------------------------------------------------*/
/*  Variable Name     |IN |OUT| Utilisation                                */
/*--------------------+---+---+--------------------------------------------*/
/*  PWM               | x |   | the PWM channel                            */
/*--------------------+---+---+--------------------------------------------*/
extern s32 ebdat6_20SqWaveUnsubscribe(FlPWM pwm);
extern s32 (*const eat_SqWaveUnsubscribe)(FlPWM pwm);
#endif

/*<<<<<<<<<<<<<<<<<<<<<<<<<<<<<< GPIO >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>*/

/***************************************************************************/
/*  Function : ebdat6_02GpioSubscribe                                      */
/*-------------------------------------------------------------------------*/
/*  Object   : Subscribe the GPIO service                                  */
/*                                                                         */
/*  Return   : OK                   on success                             */
/*             FL_RET_ERR_PARAM     if the parameter is incorrect          */
/*             FL_RET_ERR_BAD_STATE if the pin's status is unexpected      */
/*                                                                         */
/*-------------------------------------------------------------------------*/
/*  Variable Name     |IN |OUT| Utilisation                                */
/*--------------------+---+---+--------------------------------------------*/
/*  pinName           | X |   | the specified pin                          */
/*  gpioDir           | X |   | gpio direction                             */
/*  defValue          | X |   | gpio default value                         */
/*--------------------+---+---+--------------------------------------------*/
extern s32 ebdat6_02GpioSubscribe(FlPinName pinName, FlGpioDirection gpioDir, bool defValue);
extern s32 (*const eat_GpioSubscribe)(FlPinName pinName, FlGpioDirection gpioDir, bool defValue);

/***************************************************************************/
/*  Function : ebdat6_05ReadGpio                                           */
/*-------------------------------------------------------------------------*/
/*  Object   : Read the value on a subscribed input pin                    */
/*                                                                         */
/*  Return   : OK                   on success                             */
/*             FL_RET_ERR_PARAM     if the parameter is incorrect          */
/*             FL_RET_ERR_BAD_STATE if the pin's status is unexpected      */
/*                                                                         */
/*  Note that the pin should be configured as GPI first                    */
/*-------------------------------------------------------------------------*/
/*  Variable Name     |IN |OUT| Utilisation                                */
/*--------------------+---+---+--------------------------------------------*/
/*  pinName           | X |   | the pin to read                            */
/*  inputValue_p      |   | X | the value read form input pin              */
/*--------------------+---+---+--------------------------------------------*/
extern s32 ebdat6_05ReadGpio(FlPinName pinName, bool *inputValue_p);
extern s32 (*const eat_ReadGpio)(FlPinName pinName, bool *inputValue_p);



/***************************************************************************/
/*  Function : ebdat6_04WriteGpio                                          */
/*-------------------------------------------------------------------------*/
/*  Object   : Write gpio value on a subscribed output pin                 */
/*                                                                         */
/*  Return   : OK                   on success                             */
/*             FL_RET_ERR_PARAM     if the parameter is incorrect          */
/*             FL_RET_ERR_BAD_STATE if the pin's status is unexpected      */
/*                                                                         */
/*  Note that the pin should be configured as GPO first                    */
/*-------------------------------------------------------------------------*/
/*  Variable Name     |IN |OUT| Utilisation                                */
/*--------------------+---+---+--------------------------------------------*/
/*  pinName           | X |   | the pin to write                           */
/*  outputValue       | X |   | the value to write to output pin           */
/*--------------------+---+---+--------------------------------------------*/
extern s32 ebdat6_04WriteGpio(FlPinName pinName, bool outputValue);
extern s32 (*const eat_WriteGpio)(FlPinName pinName, bool outputValue);

#ifdef HAS_KBD_PERIPHERY
/***************************************************************************/
/*  Function : ebdat6_15KeySubscribe                                       */
/*-------------------------------------------------------------------------*/
/*  Object   : Subscribe keypad pins                                       */
/*                                                                         */
/*  Return   : FL_OK                   on success                          */
/*                                                                         */
/*-------------------------------------------------------------------------*/
/*  Variable Name     |IN |OUT| Utilisation                                */
/*--------------------+---+---+--------------------------------------------*/
/*--------------------+---+---+--------------------------------------------*/
extern s32 ebdat6_15KeySubscribe(void);
extern s32 (*const eat_KeySubscribe)(void);
#endif

/***************************************************************************/
/*  Function : ebdat6_17DisablePowerOffKey                                 */
/*-------------------------------------------------------------------------*/
/*  Object  : Disable power key and set it as a normal key                 */
/*                                                                         */
/*  Return   : None                                                        */
/*                                                                         */
/*-------------------------------------------------------------------------*/
/*  Variable Name     |IN |OUT| Utilisation                                */
/*--------------------+---+---+--------------------------------------------*/
/*--------------------+---+---+--------------------------------------------*/
extern void ebdat6_17DisablePowerOffKey(void);
extern void (*const eat_DisablePowerOffKey)(void);

/***************************************************************************/
/*  Function : ebdat6_18EnablePowerOffKey                                  */
/*-------------------------------------------------------------------------*/
/*  Object   : enable power key. By default it is enabled                  */
/*                                                                         */
/*  Return   : None                                                        */
/*                                                                         */
/*-------------------------------------------------------------------------*/
/*  Variable Name     |IN |OUT| Utilisation                                */
/*--------------------+---+---+--------------------------------------------*/
/*--------------------+---+---+--------------------------------------------*/
extern void ebdat6_18EnablePowerOffKey(void);
extern void (*const eat_EnablePowerOffKey)(void);

/***************************************************************************/
/*  Function : ebdat6_23GetRTSPinLevel                                     */
/*-------------------------------------------------------------------------*/
/*  Object   : get RTS pin level                                           */
/*                                                                         */
/*  Return   : RTS pin level                                               */
/*            1 for high level, 0 for low level                            */
/*-------------------------------------------------------------------------*/
/*  Variable Name     |IN |OUT| Utilisation                                */
/*--------------------+---+---+--------------------------------------------*/
/*--------------------+---+---+--------------------------------------------*/
extern u8 ebdat6_23GetRTSPinLevel(void);
extern u8 (*const eat_GetRTSPinLevel)(void);

/***************************************************************************/
/*  Function : ebdat6_27SetWatchDogGpio                                    */
/*-------------------------------------------------------------------------*/
/*  Object   : set the pin which kicks the watch dog, when call eat1_09UpdateEmbeddedAp*/
/*                and the module is in updating app firmware mode.         */
/*                                                                         */
/*  Return   : FL_OK                   set success                         */
/*             FL_RET_ERR_PARAM     if the parameter is incorrect          */
/*-------------------------------------------------------------------------*/
/*  Variable Name     |IN |OUT| Utilisation                                */
/*--------------------+---+---+--------------------------------------------*/
/*  gpio                   | X |   | the specified pin                     */
/*--------------------+---+---+--------------------------------------------*/
extern s32 ebdat6_27SetWatchDogGpio(FlPinName gpio);
extern s32 (*const eat_SetWatchDogGpio)(FlPinName gpio);

/************************/
/*  periphery:I2C       */
/************************/


typedef enum
{
    I2C_SPEED_STD_RATE_100K    =0, /* 100 kHz */
    I2C_SPEED_FAST_RATE_400K   =1, /* 400 kHz */
    I2C_SPEED_HIGH_SPEED_3400K =2, /* 3.4 MHz (not support for SIM900 series)*/
}I2C_SPEED_E;


typedef enum
{
    I2C_STATUS_IDLE    =0, /* idle */
    I2C_STATUS_BUSY    =1, /* busy */
}I2C_STATUS_E;


/* I2C Access Type: */
typedef enum
{
   I2C_PUT_ACCESS = 0,
   I2C_GET_ACCESS = 1
} I2C_AccessType;


/* Result of a GPSR Transfer: */
typedef enum
{
   GPSR_TRANSFER_DONE   = 0,
   GPSR_TRANSFER_FAILED = 1
} GpsrTransferStatusType;


/* GPSR Access Type: */
typedef enum
{
   GPSR_PUT_ACCESS = 0,  /* put  data to I2C   */
   GPSR_GET_ACCESS = 1   /* read data from I2C */
} GpsrAccessType;



/* GPSR Status: */
typedef enum
{
   GPSR_OK   = 0,           /* GPSR transfer has been initiated */
   GPSR_BUSY = 1            /* GPSR already in use              */
} GpsrStatusType;

                         
                         
/* GPSR Callback function type: */
typedef void (GpsrCallBackFunctionType)(GpsrTransferStatusType, Uint32);


/* GPSR transfer structure: */
typedef struct GpsrTransfer
{
   void   *pCmd;       /* Pointer on command string                     */
   Uint32 cmdSize;     /* Size of the command string                    */
   void   *pTxData;    /* Pointer on data to be sent                    */
   Uint32 txDataSize;  /* Size of data to be sent                       */
   void   *pRxData;    /* Pointer on data to be received                */
   Uint32 rxDataSize;  /* Size of data to be received                   */
   Uint32                   userData;     /* General purpose user data  */
   GpsrCallBackFunctionType *pIsrCallbackFct; /* ISR callback function  */
   struct GpsrTransfer      *pNext; /* Reserved for the GPSR manager    */
   GpsrAccessType           access; /* Reserved for the GPSR manager    */
}GpsrTransferType;








/***************************************************************************/
/*  Function   : ebdat15_01I2C_SpeedConfig                                 */
/*-------------------------------------------------------------------------*/
/*  Object     : I2C speed config                                          */
/*                                                                         */
/*  Return     : void                                                      */
/*                                                                         */
/*-------------------------------------------------------------------------*/
/*  Variable Name     |IN |OUT|GLB|  Utilisation                           */
/*--------------------+---+---+---+----------------------------------------*/
/*  i2c_speed         | x |   |   |  I2C speed config                      */
/*--------------------+---+---+---+----------------------------------------*/
/***************************************************************************/
extern void ebdat15_01I2C_SpeedConfig(I2C_SPEED_E i2c_speed);
extern void (*const eat_I2C_SpeedConfig)(I2C_SPEED_E i2c_speed);



/***************************************************************************/
/*  Function   : ebdat15_02I2C_ReadWriteDone                               */
/*-------------------------------------------------------------------------*/
/*  Object     : set tag indicated read or write have  done                */
/*                                                                         */
/*  Return     : void                                                      */
/*                                                                         */
/*-------------------------------------------------------------------------*/
/*  Variable Name     |IN |OUT|GLB|  Utilisation                           */
/*--------------------+---+---+---+----------------------------------------*/
/*  status            + x +   +   +                                        */
/*  userData          + x +   +   +                                        */
/*  iic_interface_busy+   +   + x +                                        */
/*--------------------+---+---+---+----------------------------------------*/
/***************************************************************************/
extern void ebdat15_02I2C_ReadWriteDone(GpsrTransferStatusType status, u32 userData);
extern void (*const eat_I2C_ReadWriteDone)(GpsrTransferStatusType status, u32 userData);


/***************************************************************************/
/*  Function   : ebdat15_03I2C_GetStatus                                   */
/*-------------------------------------------------------------------------*/
/*  Object     : get the status of i2c read or write have done             */
/*                                                                         */
/*  Return     : void                                                      */
/*                                                                         */
/*-------------------------------------------------------------------------*/
/*  Variable Name     |IN |OUT|GLB|  Utilisation                           */
/*--------------------+---+---+---+----------------------------------------*/
/*  iic_interface_busy+   +   + x +                                        */
/*--------------------+---+---+---+----------------------------------------*/
/***************************************************************************/
extern I2C_STATUS_E ebdat15_03I2C_GetStatus(void);
extern I2C_STATUS_E (*const eat_I2C_GetStatus)(void);


/***************************************************************************/
/*  Function   : ebdat15_04I2C_SetStatus                                   */
/*-------------------------------------------------------------------------*/
/*  Object     : set i2c status before read or write                       */
/*                                                                         */
/*  Return     : void                                                      */
/*                                                                         */
/*-------------------------------------------------------------------------*/
/*  Variable Name     |IN |OUT|GLB|  Utilisation                           */
/*--------------------+---+---+---+----------------------------------------*/
/*  iic_interface_busy+   +   + x +                                        */
/*--------------------+---+---+---+----------------------------------------*/
/***************************************************************************/
extern void ebdat15_04I2C_SetStatus(I2C_STATUS_E status);
extern void (*const eat_I2C_SetStatus)(I2C_STATUS_E status);


/***************************************************************************/
/*  Function   : ebdat15_05I2C_INITIALIZE_TRANSFER                         */
/*-------------------------------------------------------------------------*/
/*  Object     : i2c initialize transfer                                   */
/*                                                                         */
/*  Return     : GpsrTransferType*                                         */
/*                                                                         */
/*-------------------------------------------------------------------------*/
/*  Variable Name     |IN |OUT|GLB|  Utilisation                           */
/*--------------------+---+---+---+----------------------------------------*/
/*                    +   +   + x +                                        */
/*--------------------+---+---+---+----------------------------------------*/
/***************************************************************************/
extern GpsrTransferType*  ebdat15_05I2C_INITIALIZE_TRANSFER(void);
extern GpsrTransferType*  (*const eat_I2C_INITIALIZE_TRANSFER)(void);


/***************************************************************************/
/*  Function   : ebdat15_06I2C_PUT_DATA                                    */
/*-------------------------------------------------------------------------*/
/*  Object     : i2c put data                                              */
/*                                                                         */
/*  Return     : void                                                      */
/*                                                                         */
/*-------------------------------------------------------------------------*/
/*  Variable Name     |IN |OUT|GLB|  Utilisation                           */
/*--------------------+---+---+---+----------------------------------------*/
/*   PtRSFR           +x  +   +  +                                         */
/*--------------------+---+---+---+----------------------------------------*/
/***************************************************************************/
extern void               ebdat15_06I2C_PUT_DATA(GpsrTransferType* PtRSFR);
extern void (*const eat_I2C_PUT_DATA)(GpsrTransferType* PtRSFR);


/***************************************************************************/
/*  Function   : ebdat15_07I2C_GET_DATA                                    */
/*-------------------------------------------------------------------------*/
/*  Object     : i2c get data                                              */
/*                                                                         */
/*  Return     : void                                                      */
/*                                                                         */
/*-------------------------------------------------------------------------*/
/*  Variable Name     |IN |OUT|GLB|  Utilisation                           */
/*--------------------+---+---+---+----------------------------------------*/
/*   PtRSFR           +x  +   +  +                                         */
/*--------------------+---+---+---+----------------------------------------*/
/***************************************************************************/
extern void               ebdat15_07I2C_GET_DATA(GpsrTransferType* PtRSFR);
extern void               (*const eat_I2C_GET_DATA)(GpsrTransferType* PtRSFR);

#endif /* FL_PERIPHERY_H */

/* END OF FILE */
