/***************************************************************************
 *
 *            SIMCOM Application Foreground Layer
 *
 *              Copyright (c) 2011 SIMCOM Ltd.
 *
 ***************************************************************************
 *
 *   $Id: fl_simcard.h#1 $
 *   $Revision: #1 $
 *   $DateTime: 2012/01/08 09:00:00 $
 *
 ***************************************************************************
 *
 *  Designed by    :   MXN
 *  Coded by       :   MXN
 *  Tested by      :
 *
 ***************************************************************************
 *
 * File Description
 * ----------------
 *   the application's data struct  and  functions
 *
 *
 ***************************************************************************
 *
 ***************************************************************************/
#if !defined(FL_SIMCARD_H)
#define FL_SIMCARD_H

/*****************************************************************************/
/*Function   : ebdat13_00SetModemAPDUToFL                                    */
/*---------------------------------------------------------------------------*/
/*Object     : Controls the direction of APDU data from core                 */
/*                                                                           */
/*Return     : none                                                          */
/*                                                                           */
/*---------------------------------------------------------------------------*/
/*Variable Name     |IN |OUT|GLB|  Utilisation                               */
/*------------------+---+---+---+--------------------------------------------*/
/*destination       | X |   |   |  TRUE(1) sends the APDU data from core     */
/*                                         to embeddedat application         */
/*                                 FALSE(0) sends the APDU data              */
/*                                          from core to SIM card.           */
/*------------------+---+---+---+--------------------------------------------*/
/*****************************************************************************/
extern void ebdat13_00SetModemAPDUToFL(bool destination);
extern void (*const eat_SetModemAPDUToFL)(bool destination);

/*****************************************************************************/
/*Function   : ebdat13_01SetSIMCardAPDUToFL                                  */
/*---------------------------------------------------------------------------*/
/*Object     : Controls the input APDU data's direction from SIM card.       */
/*             if it is set to TRUE,the input APDU data from SIM card is sent*/
/*             to embedded application.                                      */
/*                                                                           */
/*Return     : none                                                          */
/*                                                                           */
/*---------------------------------------------------------------------------*/
/*Variable Name     |IN |OUT|GLB|  Utilisation                               */
/*------------------+---+---+---+--------------------------------------------*/
/*destination       | X |   |   |  TRUE(1) the input data from SIM card      */
/*                                         is sent to embedded application.  */
/*                                 FALSE(0) for sending to core buffer       */
/*------------------+---+---+---+--------------------------------------------*/
/*****************************************************************************/
extern void ebdat13_01SetSIMCardAPDUToFL(bool destination);
extern void (*const eat_SetSIMCardAPDUToFL)(bool destination);

/*****************************************************************************/
/*Function   : ebdat13_03SendResetReqToSIMCard                               */
/*---------------------------------------------------------------------------*/
/*Object     :  Send reset request to the SIM card.                          */
/*                                                                           */
/*Return     : none                                                          */
/*                                                                           */
/*---------------------------------------------------------------------------*/
/*Variable Name     |IN |OUT|GLB|  Utilisation                               */
/*------------------+---+---+---+--------------------------------------------*/
/*type              | X |   |   |  When the module wants to reset the SIM    */
/*                                 card, the modem will receive the reset    */
/*                                 event. The type of the reset must be      */
/*                                 filled in this function.                  */
/* Note:    If you use soft SIM, this function is not used.                  */
/*------------------+---+---+---+--------------------------------------------*/
/*****************************************************************************/
extern void ebdat13_03SendResetReqToSIMCard(u8 type);
extern void (*const eat_SendResetReqToSIMCard)(u8 type);

/*****************************************************************************/
/*Function   : ebdat13_05SendSIMCardResetCnfToModem                          */
/*---------------------------------------------------------------------------*/
/*Object     : When the EAT receive MODEM_APDU event and its apduType is     */
/*             FL_MOD_APDU_RESET a reset confirm must be sent back to the    */
/*             core.                                                         */
/*                                                                           */
/*resetCnf     :           reset parameter confirm                           */
/*                                                                           */
/*---------------------------------------------------------------------------*/
/*Variable Name      |IN |OUT|GLB|  Utilisation                              */
/*--------------------+---+---+---+------------------------------------------*/
/*resetCnf           | X |   |   |  If you use the real SIM card, the        */
/*                                  "resetCnf" must be filled according to   */
/*                                  the EVENT_SIMCARD_APDU.                  */
/*                   If you use the Soft SIM card, resetCnf.v_VoltageValue   */
/*                   should be 1. and the a_AnswerToReset can be all zero.   */
/*------------------+---+---+---+--------------------------------------------*/
/*Note:  If you use the Soft SIM, when a EVENT_MODEM_APDU is received and    */
/*         its type is  FL_MOD_APDU_RESET, this function must be called.     */
/*****************************************************************************/
extern void ebdat13_05SendSIMCardResetCnfToModem(SIMCARDRESET_CNF   resetCnf);
extern void (*const eat_SendSIMCardResetCnfToModem)(SIMCARDRESET_CNF   resetCnf);

/*****************************************************************************/
/*Function   : ebdat13_08SendAPDUReqToSIMCard                                */
/*---------------------------------------------------------------------------*/
/*Object     :  Send the APDU data to SIM card.                              */
/*                                                                           */
/*apduData     :           APDU data                                         */
/*type     :           APDU data type                                        */
/*                                                                           */
/*return :             FL_OK: send data successfully.                        */
/*                       FL_ERROR: send data failed.                         */
/*---------------------------------------------------------------------------*/
/*Variable Name      |IN |OUT|GLB|  Utilisation                              */
/*-------------------+---+---+---+-------------------------------------------*/
/*apduData           | X |   |   |                                           */
/*                          The APDU data will be sent to the SIM card.      */
/*type               | X |   |   |                                           */
/*                         If the APDU is a read request, it should be 1.    */
/*                         If the APDU is a send data request,it should be 0.*/
/*------------------+---+---+---+--------------------------------------------*/
/*Note:  If you use the Soft SIM, this function is not used.                 */
/******************************************************************************/
extern s32 ebdat13_08SendAPDUReqToSIMCard(MODEMAPDU_DATA apduData, u8 type);
extern s32 (*const eat_SendAPDUReqToSIMCard)(MODEMAPDU_DATA apduData, u8 type);

/*****************************************************************************/
/*Function   : ebdat13_10SendAPDUCnfToModem                                  */
/*---------------------------------------------------------------------------*/
/*Object     :  Send the APDU data to SIM card.                              */
/*                                                                           */
/*apduData     :           APDU data                                         */
/*                                                                           */
/*---------------------------------------------------------------------------*/
/*Variable Name      |IN |OUT|GLB|  Utilisation                              */
/*-------------------+---+---+---+-------------------------------------------*/
/*apduData.a_RData| X |      |     |                                         */
/*                          The APDU data will be sent to the modem.         */
/*apduData.v_len     | X |      |     |                                      */
/*                           The lengh of the APDU data                      */
/*------------------+---+---+---+--------------------------------------------*/
/*Note:  If you use the Soft SIM, this function is not used. This is         */
/*       only used in real SIM card.                                         */
/*****************************************************************************/
extern void ebdat13_10SendAPDUCnfToModem(SIMCARDAPDU_DATA apduData);
extern void (*const eat_SendAPDUCnfToModem)(SIMCARDAPDU_DATA apduData);

/*****************************************************************************/
/*Function   : ebdat13_10SendAPDUCnfToModem                                  */
/*---------------------------------------------------------------------------*/
/*Object     :  Send the APDU data to SIM card.                              */
/*                                                                           */
/*apduData     :           APDU data                                         */
/*                                                                           */
/*---------------------------------------------------------------------------*/
/*Variable Name      |IN |OUT|GLB|  Utilisation                              */
/*-------------------+---+---+---+-------------------------------------------*/
/*apduData.a_RData| X |      |     |                                         */
/*                          The APDU data will be sent to the modem.         */
/*apduData.v_len     | X |      |     |                                      */
/*                           The lengh of the APDU data                      */
/*------------------+---+---+---+--------------------------------------------*/
/*Note:  If you use the Soft SIM, when a EVENT_MODEM_APDU is received and    */
/*       its type is FL_MOD_APDU_REQ_DATA or FL_MOD_APDU_SEND_DATA, this     */
/*       function must be called.                                            */
/*****************************************************************************/
extern void ebdat13_11SoftSendAPDUCnfToModem(SIMCARDAPDU_DATA apduData);
extern void (*const eat_SoftSendAPDUCnfToModem)(SIMCARDAPDU_DATA apduData);

#endif

