/*
**
*/
#include <fl_typ.h>
#include <fl_appinit.h>
#include <fl_interface.h>
#if defined COMP_GCC
#include <sys/types.h>
#endif
#if !defined COMP_GCC && defined USE_C_STANDARD_LIBS
#include <rt_misc.h>
#endif
void fl_main(unsigned char id);
#if defined COMP_GCC
extern void _fl_preinit(void);

const flEntry FL_ENTRY __attribute__ ((section (".fl_main_ptr"))) = {
	fl_main
};

// FROM: newlib/libc/misc/init.c
extern void (*__preinit_array_start []) (void) __attribute__((weak));
extern void (*__preinit_array_end []) (void) __attribute__((weak));
extern void (*__init_array_start []) (void) __attribute__((weak));
extern void (*__init_array_end []) (void) __attribute__((weak));
#else
#pragma arm section rodata="GABADISP"
const flEntry GADLENTRY = 
{
 fl_main,
};
#pragma arm section rodata

extern void funcRegionInit(void);
#endif
static void _app_init(void);
static void _wait_init(void);
static void _ready(void);
static void _wait_event(void);

/** Custom init function (this function MUST NOT return until the system is fully initialized) */
extern fl_task_entry fl_init;

/** Task entry functions */
extern fl_task_entry fl_entries[];


/**
 * Entry function to user code. Called for every task.
 * @param id Task ID
 */
 
#if !defined COMP_GCC && defined USE_C_STANDARD_LIBS
__value_in_regs struct __initial_stackheap __user_initial_stackheap(unsigned R0, unsigned SP, unsigned R2, unsigned SL)
{
   struct __initial_stackheap config;
 
   config.heap_base = R0;
   config.heap_limit =  R2;
   config.stack_base =  SP; 
   config.stack_limit =   SL;
   return config;
}
 
void $Sub$$_fp_init(void)
{
}
void $Sub$$_initio(void)
{
}
__value_in_regs struct __argc_argv $Sub$$__ARM_get_argv(void *a )
{
    /*
    int argc;
    char **argv;
    int r2, r3;
    */
    struct __argc_argv arg;
    arg.argc = 0;
    arg.r2 = 0;
    arg.r3 = 0;
    return arg;
}

void StackInitFun(void)
{
  	__rt_lib_init ( 0xa0300000, 0xa0300FFF );
}
#endif
void fl_main(unsigned char id) {
	unsigned char i;


	// Init
	if(id == EMBEDDED_AT_TASK_ID) {
		// Main task, do memory init (this has to happen first!)
		_app_init();
#if !defined COMP_GCC && defined USE_C_STANDARD_LIBS
		StackInitFun();
#endif
		// Call custom init function if defined, otherwise use standard one
		if(fl_init != NULL) (*fl_init)();
		else _wait_init();		

		// Indicate other tasks that we're ready to go
		_ready();

		// Call main task entry
		fl_entry();
	} else {
		// Secondary task, wait for main task to indicate completion of init
		_wait_event();

		// Call task entry function (if set)
		if(fl_entries != NULL) {
			for(i = 0; i < id; i++) {
				if(fl_entries[i] == NULL) break;
			}
			if(i == id) (*fl_entries[id - 1])();
		}
	}

	// Make sure we never return
//	while(1) _wait_event();
}


/**
 * Initializes the user application's memory.
 */
static void _app_init() {
#if defined COMP_GCC
	size_t count, i;
	
	
	count = __preinit_array_end - __preinit_array_start;
	for (i = 0; i < count; i++) {
		__preinit_array_start[i] ();
	}
	
	// Init user memory
	_fl_preinit();
	
	count = __init_array_end - __init_array_start;
	for (i = 0; i < count; i++) {
		__init_array_start[i] ();
	}	
#else
	funcRegionInit();
#endif
}



/**
 * Blocks until all of the system is initialized. (Called from the main task.)
 */
static void _wait_init() {
	FlEventBuffer e;
	unsigned char flash = 0, uart = 0;


	while(!flash || !uart) {
#if defined USE_C_STANDARD_LIBS
		eat_GetEvent(&e);
#else
		eat1_02GetEvent(&e);
#endif
		switch(e.eventTyp) {
			case EVENT_FLASH_READY: flash = 1; break;
			case EVENT_UART_READY: uart = 1; break;
			default: break;
		}
	}
}


/**
 * Indicates secondary tasks that the system is ready. (Called from the main task.)
 */
static void _ready() {
	MSG_EVT m;

	
	m.source = (u8)EMBEDDED_AT_TASK_ID;
	m.destination = (u8)MULTI_TASK_PRIO1_ID;
#if defined USE_C_STANDARD_LIBS
	eat_SendEventMsg(m);
#else
	ebdat4_21SendEventMsg(m);
#endif
	m.destination = (u8)MULTI_TASK_PRIO2_ID;
#if defined USE_C_STANDARD_LIBS
	eat_SendEventMsg(m);
#else
	ebdat4_21SendEventMsg(m);
#endif
	m.destination = (u8)MULTI_TASK_PRIO3_ID;
#if defined USE_C_STANDARD_LIBS
	eat_SendEventMsg(m);
#else
	ebdat4_21SendEventMsg(m);
#endif
	m.destination = (u8)MULTI_TASK_PRIO4_ID;
#if defined USE_C_STANDARD_LIBS
	eat_SendEventMsg(m);
#else
	ebdat4_21SendEventMsg(m);
#endif
	m.destination = (u8)MULTI_TASK_PRIO5_ID;
#if defined USE_C_STANDARD_LIBS
	eat_SendEventMsg(m);
#else
	ebdat4_21SendEventMsg(m);
#endif
}


/**
 * Blocks until an event is received. Used to "wait efficiently".
 */
static void _wait_event() {
	FlEventBuffer e;


#if defined USE_C_STANDARD_LIBS
	eat_GetEvent(&e);	
#else
	eat1_02GetEvent(&e);	
#endif
}
