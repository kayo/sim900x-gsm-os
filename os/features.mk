hws := sim900 sim900r sim900ds

ifeq ($(hw),sim900)
def += HAS_EVENT_APDU=1
def += HAS_KBD_PERIPHERY=1
def += HAS_PWM_PERIPHERY=1
rams := 1M
ram := 1M
blob := 1137B02V01SIM900M64_ST_DTMF_JD_EAT
endif

ifeq ($(hw),sim900r)
def += HAS_EVENT_APDU=1
def += UPDATE_EMBEDDED_AP_HAS_CHECKSUM=1
def += HAS_DEBUG_PORT_CONTROL=1
def += HAS_KBD_PERIPHERY=1
def += HAS_PWM_PERIPHERY=1
rams := 1M
ram := 1M
blob := 1137B01V02SIM900R64_ST_ENHANCE_EAT
endif

ifeq ($(hw),sim900ds)
def += HAS_MULTIPLE_SIM_CARD=1
def += HAS_KEEPALIVE_CONTROL=1
def += UPDATE_EMBEDDED_AP_HAS_CHECKSUM=1
def += HAS_DEBUG_PORT_CONTROL=1
rams := 512K 1M

ifeq ($(ram),512K)
blob := 1158B01V02SIM900-DS128_SPANSION_EAT
endif

ifeq ($(ram),1M)
blob := 1158B01V02SIM900-DS128_SPANSION_EAT_20150121
endif

endif

ld := $(hw)_$(ram)

ifeq (yes,$(nox))
ld += _no_x
endif

ifeq (,$(findstring $(hw),$(hws)))
$(error Please select hardware: $(addprefix hw=,$(hws)))
endif

ifeq (,$(findstring $(ram),$(rams)))
$(error Please select RAM size: $(addprefix ram=,$(rams)))
endif
