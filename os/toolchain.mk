toolchain ?= arm-none-eabi-

CC := $(toolchain)gcc
LD := $(toolchain)ld
OBJCOPY := $(toolchain)objcopy
RM := rm -rf
